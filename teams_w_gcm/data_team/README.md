<!-- markdownlint-disable MD010 MD024 MD033 MD038 -->
# Data crunchers SLOs spec by critical user journeys

1 users 1 goals 2 tasks 3 SLOs

_This doc is auto generated from file_ data_cujs.yml on 2025-03-06 11:14:41

1. As a data analyst
	1. I want to provide insights
		1. NSJ synchronizes usage data
			1. [coverage __96%__](#1111)
		2. I query data
			1. [availability __99.9%__](#1121)
			2. [latency 131ms __95%__](#1122)

## [data  - NSJ synchronizes usage data coverage 96%](https://tbd?query=user%3Aas_a_data_analyst+goal%3Ai_want_to_provide_insights+task%3Ansj_synchronizes_usage_data+%22coverage%22)<a name="1111"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="custom.googleapis.com/dataproducts/interval/count" resource.type="global" metric.label."data_product_name"="appA" metric.label."interval_duration_sec"="86400" metric.label."table_name"="tableA" metric.label."good_points"=monitoring.regex.full_match("5|8|9")
```

Count good+bad

``` promql
metric.type="custom.googleapis.com/dataproducts/interval/count" resource.type="global" metric.label."data_product_name"="appA" metric.label."interval_duration_sec"="86400" metric.label."table_name"="tableA"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-12-18 07:07:09

## [data  - I query data availability 99.9%](https://tbd?query=user%3Aas_a_data_analyst+goal%3Ai_want_to_provide_insights+task%3Ai_query_data+%22availability%22)<a name="1121"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="serviceruntime.googleapis.com/api/request_count" resource.type="consumed_api" resource.label."service"="bigquery.googleapis.com" metric.label."response_code_class"="2xx"
```

Count good+bad

``` promql
metric.type="serviceruntime.googleapis.com/api/request_count" resource.type="consumed_api" resource.label."service"="bigquery.googleapis.com" metric.label."response_code_class"=monitoring.regex.full_match("2xx|5xx")
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-

## [data  - I query data latency 131ms 95%](https://tbd?query=user%3Aas_a_data_analyst+goal%3Ai_want_to_provide_insights+task%3Ai_query_data+%22latency+131ms%22)<a name="1122"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-
