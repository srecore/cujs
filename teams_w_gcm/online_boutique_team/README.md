<!-- markdownlint-disable MD010 MD024 MD033 MD038 -->
# Online Boutique SLOs spec by critical user journeys

1 users 2 goals 15 tasks 30 SLOs

_This doc is auto generated from file_ online_boutique_cujs.yml on 2025-03-06 11:14:41

1. As an online consumer
	1. I want to find items that suit me
		1. I get the homepage
			1. [availability __99.9%__](#1111)
			2. [latency 256ms __95%__](#1112)
		2. I list products
			1. [availability __99.95%__](#1121)
			2. [latency 4ms __95%__](#1122)
		3. I get a product
			1. [availability __99.95%__](#1131)
			2. [latency 4ms __95%__](#1132)
		4. I list currencies
			1. [availability __99.95%__](#1141)
			2. [latency 4ms __95%__](#1142)
		5. I convert an amount to a currency
			1. [availability __99.95%__](#1151)
			2. [latency 4ms __95%__](#1152)
		6. I get a product promotions and ads
			1. [availability __99.95%__](#1161)
			2. [latency 4ms __95%__](#1162)
		7. I list substitution or complementary products
			1. [availability __99.95%__](#1171)
			2. [latency 64ms __95%__](#1172)
	2. I want to order items
		1. I add a quantity of an item to my cart
			1. [availability __99.95%__](#1211)
			2. [latency 4ms __95%__](#1212)
		2. I get my cart
			1. [availability __99.95%__](#1221)
			2. [latency 4ms __95%__](#1222)
		3. I empty my cart
			1. [availability __99.95%__](#1231)
			2. [latency 4ms __95%__](#1232)
		4. I get a delivery quote
			1. [availability __99.95%__](#1241)
			2. [latency 4ms __95%__](#1242)
		5. I order
			1. [availability __99.9%__](#1251)
			2. [latency 256ms __95%__](#1252)
		6. I pay
			1. [availability __99.95%__](#1261)
			2. [latency 8ms __95%__](#1262)
		7. OnlineBoutique trigger shipping
			1. [availability __99.95%__](#1271)
			2. [latency 4ms __95%__](#1272)
		8. OnlineBoutique send me a confirmation email
			1. [availability __99.95%__](#1281)
			2. [latency 64ms __95%__](#1282)

## [online-boutique  - I get the homepage availability 99.9%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_get_the_homepage+%22availability%22)<a name="1111"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="frontend" (metric.label."response_code"="404" OR metric.label."response_code"<="302")
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="frontend" metric.label."response_code"!="426"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| Investigate error 0, 400, 405, 500, and 426 too | Front Teach Lead | EOW |

- 2024-06-12
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - 6.6m qpm enabling high and low burnrate alerts
  - Categorization
    - Good: 200, 301, 302, 404
    - Bad:
      - 0 this is not a regular HTTP status code x60
      - 400 Bad request: Our own front should respect our backs API contract x11247
      - 405 Method not allowed: Our own front should respect our backs API contract x861
      - 500 Internal server error: to be investigated x565
    - Invalid:
      - 426 Upgrade required: to be investigated x126817
  - SLI 99.8% set SLO to 99.9 as aspirational target, leading to a freeze as Front is our main reliability issue.

## [online-boutique  - I get the homepage latency 256ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_get_the_homepage+%22latency+256ms%22)<a name="1112"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="frontend", response_code="200"}[28d]))+sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="frontend", response_code="404"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 and 404 response code
  - optimizing denominator with _count metric
  - Setting the threshold to 256ms get an SLI of 98.5%
    - Meaning if SLO 95%, then Error budget consumed = 30% -> Ok

## [online-boutique  - I list products availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_list_products+%22availability%22)<a name="1121"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="productcatalogservice" metric.label."request_operation"="/hipstershop.ProductCatalogService/ListProducts" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="productcatalogservice" metric.label."request_operation"="/hipstershop.ProductCatalogService/ListProducts"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-10
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 5.2M qpm enabling high and low burnrate alerts

## [online-boutique  - I list products latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_list_products+%22latency+4ms%22)<a name="1122"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-10
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="productcatalogservice", request_operation="/hipstershop.ProductCatalogService/ListProducts", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
    - P95 in GCP metric explorer gives 1.988 ms
    - Setting the threshold to 4ms get an SLI of 98.44%
    - Setting the SLO to 95%, means Error budget consumed at 31% which validates this target as an achievable SLO.
  - Valid events means 200 response code
  - optimizing denominator with _count metric

## [online-boutique  - I get a product availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_get_a_product+%22availability%22)<a name="1131"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="productcatalogservice" metric.label."request_operation"="/hipstershop.ProductCatalogService/GetProduct" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="productcatalogservice" metric.label."request_operation"="/hipstershop.ProductCatalogService/GetProduct"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-10
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 29.6M qpm enabling high and low burnrate alerts

## [online-boutique  - I get a product latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_get_a_product+%22latency+4ms%22)<a name="1132"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-10
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="productcatalogservice", request_operation="/hipstershop.ProductCatalogService/GetProduct", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
    - Setting the threshold to 4ms get an SLI of 98.38%
    - Setting the SLO to 95%, means Error budget consumed at 31% which validates this target as an achievable SLO.
  - Valid events means 200 response code
  - optimizing denominator with _count metric

## [online-boutique  - I list currencies availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_list_currencies+%22availability%22)<a name="1141"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="currencyservice" metric.label."request_operation"="/hipstershop.CurrencyService/GetSupportedCurrencies" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="currencyservice" metric.label."request_operation"="/hipstershop.CurrencyService/GetSupportedCurrencies"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-10
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200 and 500
  - SLO set to 99.95 as achievable
  - 5.2M qpm enabling high and low burnrate alerts

## [online-boutique  - I list currencies latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_list_currencies+%22latency+4ms%22)<a name="1142"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-10
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="currencyservice", request_operation="/hipstershop.CurrencyService/GetSupportedCurrencies", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
    - P95 in GCP metric explorer gives 3.869 ms
    - Setting the threshold to 4ms get an SLI of 97.1%
    - Setting the SLO to 95%, means Error budget consumed at 57% which validates this target as an achievable SLO.
  - Valid events means 200 response code
  - Using metric `istio_io:service_server_request_count`at denominator to optimize performances provide slightly different results than using metric `istio_io:service_server_response_latencies_bucket` with le set to `+Inf`
  - Using metric `istio_io:service_server_response_latencies_count at denominator provides exactly the same results than  using metric `istio_io:service_server_response_latencies_bucket` with le set to `+Inf`: validated

## [online-boutique  - I convert an amount to a currency availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_convert_an_amount_to_a_currency+%22availability%22)<a name="1151"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="currencyservice" metric.label."request_operation"="/hipstershop.CurrencyService/Convert" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="currencyservice" metric.label."request_operation"="/hipstershop.CurrencyService/Convert"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-10
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 14M qpm enabling high and low burnrate alerts

## [online-boutique  - I convert an amount to a currency latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_convert_an_amount_to_a_currency+%22latency+4ms%22)<a name="1152"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-10
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="currencyservice", request_operation="/hipstershop.CurrencyService/Convert", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
    - P95 in GCP metric explorer gives 3.791 ms
    - Setting the threshold to 4ms get an SLI of 98.3%
    - Setting the SLO to 95%, means Error budget consumed at 35% which validates this target as an achievable SLO.
  - Valid events means 200 response code
  - optimizing denominator with _count metric

## [online-boutique  - I get a product promotions and ads availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_get_a_product_promotions_and_ads+%22availability%22)<a name="1161"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="adservice" metric.label."request_operation"="/hipstershop.AdService/GetAds" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="adservice" metric.label."request_operation"="/hipstershop.AdService/GetAds"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-11
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200, 500, 504
  - SLO set to 99.95 as achievable
  - 3.6M qpm enabling high and low burnrate alerts

## [online-boutique  - I get a product promotions and ads latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_get_a_product_promotions_and_ads+%22latency+4ms%22)<a name="1162"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-11
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="adservice", request_operation="/hipstershop.AdService/GetAds", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
    - P95 in GCP metric explorer gives 2.75 ms
    - Setting the threshold to 4ms get an SLI of 98.3%
    - Setting the SLO to 95%, means Error budget consumed at 35% which validates this target as an achievable SLO.
  - Valid events means 200 response code
  - optimizing denominator with _count metric

## [online-boutique  - I list substitution or complementary products availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_list_substitution_or_complementary_products+%22availability%22)<a name="1171"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="recommendationservice" metric.label."request_operation"="/hipstershop.RecommendationService/ListRecommendations" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="recommendationservice" metric.label."request_operation"="/hipstershop.RecommendationService/ListRecommendations"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 4.57M qpm enabling high and low burnrate alerts

## [online-boutique  - I list substitution or complementary products latency 64ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_find_items_that_suit_me+task%3Ai_list_substitution_or_complementary_products+%22latency+64ms%22)<a name="1172"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="recommendationservice", request_operation="/hipstershop.RecommendationService/ListRecommendations", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 40.34 ms
  - Setting the threshold to 64ms get an SLI of 96,5%
    - Meaning if SLO 95%, then Error budget consumed = 70%
  - Setting the threshold to the next le 128ms get an SLI of 99.2%
    - Meaning if SLO 95%, then Error budget consumed = 16%
  - Validate targets: 95% - 64ms

## [online-boutique  - I add a quantity of an item to my cart availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_add_a_quantity_of_an_item_to_my_cart+%22availability%22)<a name="1211"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="cartservice" metric.label."request_operation"="/hipstershop.CartService/AddItem" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="cartservice" metric.label."request_operation"="/hipstershop.CartService/AddItem"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 686k qpm enabling high and low burnrate alerts

## [online-boutique  - I add a quantity of an item to my cart latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_add_a_quantity_of_an_item_to_my_cart+%22latency+4ms%22)<a name="1212"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="cartservice", request_operation="/hipstershop.CartService/AddItem", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 2.66 ms
  - Setting the threshold to 4ms get an SLI of 97,6%
    - Meaning if SLO 95%, then Error budget consumed = 48% -> Ok

## [online-boutique  - I get my cart availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_get_my_cart+%22availability%22)<a name="1221"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="cartservice" metric.label."request_operation"="/hipstershop.CartService/GetCart" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="cartservice" metric.label."request_operation"="/hipstershop.CartService/GetCart"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 5.2m qpm enabling high and low burnrate alerts

## [online-boutique  - I get my cart latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_get_my_cart+%22latency+4ms%22)<a name="1222"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", canonical_service_name="cartservice", request_operation="/hipstershop.CartService/GetCart", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 2.66 ms
  - Setting the threshold to 4ms get an SLI of 97,6%
    - Meaning if SLO 95%, then Error budget consumed = 48% -> Ok

## [online-boutique  - I empty my cart availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_empty_my_cart+%22availability%22)<a name="1231"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="cartservice" metric.label."request_operation"="/hipstershop.CartService/EmptyCart" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="cartservice" metric.label."request_operation"="/hipstershop.CartService/EmptyCart"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 228k qpm enabling high and low burnrate alerts

## [online-boutique  - I empty my cart latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_empty_my_cart+%22latency+4ms%22)<a name="1232"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-12
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service", resource.label.canonical_service_name="cartservice", request_operation="/hipstershop.CartService/EmptyCart", response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 1.99 ms
  - Setting the threshold to 4ms get an SLI of 98,4%
    - Meaning if SLO 95%, then Error budget consumed = 32% -> Ok .

## [online-boutique  - I get a delivery quote availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_get_a_delivery_quote+%22availability%22)<a name="1241"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="shippingservice" metric.label."request_operation"="/hipstershop.ShippingService/GetQuote" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="shippingservice" metric.label."request_operation"="/hipstershop.ShippingService/GetQuote"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 1.6m qpm enabling high and low burnrate alerts

## [online-boutique  - I get a delivery quote latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_get_a_delivery_quote+%22latency+4ms%22)<a name="1242"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service",canonical_service_name="shippingservice", request_operation="/hipstershop.ShippingService/GetQuote",response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 1.92 ms
  - Setting the threshold to 4ms get an SLI of 98,8%
    - Meaning if SLO 95%, then Error budget consumed = 24% -> Ok

## [online-boutique  - I order availability 99.9%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_order+%22availability%22)<a name="1251"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="checkoutservice" metric.label."request_operation"="/hipstershop.CheckoutService/PlaceOrder" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="checkoutservice" metric.label."request_operation"="/hipstershop.CheckoutService/PlaceOrder"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found 200 and 500
  - SLO set to 99.9 as achievable
  - 228k qpm enabling high and low burnrate alerts

## [online-boutique  - I order latency 256ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_order+%22latency+256ms%22)<a name="1252"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service",canonical_service_name="checkoutservice", request_operation="/hipstershop.CheckoutService/PlaceOrder",response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 185.6 ms
  - Setting the threshold to 256ms get an SLI of 99,4%
    - Meaning if SLO 95%, then Error budget consumed = 12% -> Ok

## [online-boutique  - I pay availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_pay+%22availability%22)<a name="1261"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="paymentservice" metric.label."request_operation"="/hipstershop.PaymentService/Charge" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="paymentservice" metric.label."request_operation"="/hipstershop.PaymentService/Charge"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 228k qpm enabling high and low burnrate alerts

## [online-boutique  - I pay latency 8ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Ai_pay+%22latency+8ms%22)<a name="1262"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service",canonical_service_name="paymentservice", request_operation="/hipstershop.PaymentService/Charge",response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 4 ms
  - Setting the threshold to 8ms get an SLI of 98,6%
    - Meaning if SLO 95%, then Error budget consumed = 28% -> Ok

## [online-boutique  - OnlineBoutique trigger shipping availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Aonlineboutique_trigger_shipping+%22availability%22)<a name="1271"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="shippingservice" metric.label."request_operation"="/hipstershop.ShippingService/ShipOrder" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="shippingservice" metric.label."request_operation"="/hipstershop.ShippingService/ShipOrder"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 228k qpm enabling high and low burnrate alerts

## [online-boutique  - OnlineBoutique trigger shipping latency 4ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Aonlineboutique_trigger_shipping+%22latency+4ms%22)<a name="1272"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request `sum by(le) (increase(istio_io:service_server_response_latencies_bucket{monitored_resource="istio_canonical_service",canonical_service_name="shippingservice", request_operation="/hipstershop.ShippingService/ShipOrder",response_code="200"}[28d]))`
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 1.95 ms
  - Setting the threshold to 4ms get an SLI of 99,4%
    - Meaning if SLO 95%, then Error budget consumed = 12% -> Ok

## [online-boutique  - OnlineBoutique send me a confirmation email availability 99.95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Aonlineboutique_send_me_a_confirmation_email+%22availability%22)<a name="1281"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="emailservice" metric.label."request_operation"="/hipstershop.EmailService/SendOrderConfirmation" metric.label."response_code"="200"
```

Count good+bad

``` promql
metric.type="istio.io/service/server/request_count" resource.type="istio_canonical_service" resource.label."canonical_service_name"="emailservice" metric.label."request_operation"="/hipstershop.EmailService/SendOrderConfirmation"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - initiate SLO
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_request_count`
  - Found only 200
  - SLO set to 99.95 as achievable
  - 228k qpm enabling high and low burnrate alerts

## [online-boutique  - OnlineBoutique send me a confirmation email latency 64ms 95%](https://tbd?query=user%3Aas_an_online_consumer+goal%3Ai_want_to_order_items+task%3Aonlineboutique_send_me_a_confirmation_email+%22latency+64ms%22)<a name="1282"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-06-13
  - Instrumented by Google service mesh based on Istio. Metric name: `istio_io:service_server_response_latencies_bucket`. [Documentation indicates the unit is millisecond](https://cloud.google.com/monitoring/api/metrics_istio#istio-istio)
  - Exploring `le` label with request ``
    - Provides the list: `0,1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65356,131072,262144,524288,+Inf`
    - Meaning: Exponential progression power of 2 from zero ms to 8.7 min.
  - Valid events means 200 response code
  - optimizing denominator with _count metric
  - P95 in GCP metric explorer gives 4 ms
  - Setting the threshold to 64ms get an SLI of 98,6%
    - Meaning if SLO 95%, then Error budget consumed = 28% -> Ok
