<!-- markdownlint-disable MD010 MD024 MD033 MD038 -->
# Security Analysis SLOs spec by critical user journeys

1 users 1 goals 7 tasks 9 SLOs

_This doc is auto generated from file_ security_cujs.yml on 2025-03-06 11:14:41

1. As a security analyst
	1. I want to identify non compliant configurations
		1. RAM analyzes assets on a schedule basis
			1. [coverage __99.95%__](#1111)
			2. [freshness all but k8s pod 10.92m __95%__](#1112)
			3. [freshness k8s pod 5.46m __95%__](#1113)
			4. Work in progress [freshness k8s pod 5.46m window 5m __95%__](#1114)
		2. RAM analyzes assets in real-time
			1. [coverage __99.9%__](#1121)
			2. Work in progress [freshness 5.4m __95%__](#1122)
		3. I get my user profile
		4. I list cloud assets
		5. I get an asset compliance timeline
		6. RAM ingest asset continuously
			1. Work in progress [coverage window 1hour sum __99%__](#1161)
			2. Work in progress [coverage window 1h mean __99%__](#1162)
		7. SQL DB running=true
			1. Work in progress [coverage window 1m __99.9%__](#1171)

## [security  - RAM analyzes assets on a schedule basis coverage 99.95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_on_a_schedule_basis+%22coverage%22)<a name="1111"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="logging.googleapis.com/user/ram_execution_count" resource.type="cloud_run_revision" metric.label."origin"="scheduled" metric.label."status"=monitoring.regex.full_match("finish.*")
```

Count good+bad

``` promql
metric.type="logging.googleapis.com/user/ram_execution_count" resource.type="cloud_run_revision" metric.label."origin"="scheduled" metric.label."status"!="retry"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-03-29
  - finish.* means good
  - retry means invalid
  - noretry means bad.

## [security  - RAM analyzes assets on a schedule basis freshness all but k8s pod 10.92m 95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_on_a_schedule_basis+%22freshness+all+but+k8s+pod+10.92m%22)<a name="1112"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-01-07 fix filter reset target 95% 10.02m
- 2024-03-29 set target

## [security  - RAM analyzes assets on a schedule basis freshness k8s pod 5.46m 95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_on_a_schedule_basis+%22freshness+k8s+pod+5.46m%22)<a name="1113"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-01-07
  - set target 95% 5.46m
  - 38k event per 28d, enabling low burn rate alert. high burn rate not activated as scheduled period > 1h

## [security  - RAM analyzes assets on a schedule basis freshness k8s pod 5.46m window 5m 95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_on_a_schedule_basis+%22freshness+k8s+pod+5.46m+window+5m%22)<a name="1114"></a>

Status: work in progress, no consequences when the error budget is exhausted

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-02-05

## [security  - RAM analyzes assets in real-time coverage 99.9%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_in_real-time+%22coverage%22)<a name="1121"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
metric.type="logging.googleapis.com/user/ram_execution_count" resource.type="cloud_run_revision" metric.label."origin"="real-time" metric.label."status"=monitoring.regex.full_match("finish.*")
```

Count good+bad

``` promql
metric.type="logging.googleapis.com/user/ram_execution_count" resource.type="cloud_run_revision" metric.label."origin"="real-time" metric.label."status"!="retry"
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-01-07
  - 24 event per sec, reset target 99.9%, only LOW burnrate alert
- 2024-03-29
  - finish.* means good
  - retry means invalid
  - noretry means bad

## [security  - RAM analyzes assets in real-time freshness 5.4m 95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_in_real-time+%22freshness+5.4m%22)<a name="1122"></a>

Status: work in progress, no consequences when the error budget is exhausted

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-03-29
  - low traffic in in this env, keep WIP no burnrate alerts

## [security  - RAM ingest asset continuously coverage window 1hour sum 99%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_ingest_asset_continuously+%22coverage+window+1hour+sum%22)<a name="1161"></a>

Status: work in progress, no consequences when the error budget is exhausted

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-02-25
  - 28 x 24h = 672h per aggregation window
  - target 99% means 1% Error budget target 6.72h

## [security  - RAM ingest asset continuously coverage window 1h mean 99%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_ingest_asset_continuously+%22coverage+window+1h+mean%22)<a name="1162"></a>

Status: work in progress, no consequences when the error budget is exhausted

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-02-25
  - 28 x 24h = 672h per aggregation window
  - target 99% means 1% Error budget target 6.72h

## [security  - SQL DB running=true coverage window 1m 99.9%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Asql_db_running%3Dtrue+%22coverage+window+1m%22)<a name="1171"></a>

Status: work in progress, no consequences when the error budget is exhausted

Count good

``` promql

```

Count good+bad

``` promql

```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-02-25
