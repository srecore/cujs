<!-- markdownlint-disable MD010 MD024 MD033 MD038 -->
# security analysis SLOs spec by critical user journeys

1 users 1 goals 2 tasks 5 SLOs

_This doc is auto generated from file_ security_cujs.yml on 2025-03-06 08:15:16

1. As a security analyst
	1. I want to identify non compliant configurations
		1. RAM analyzes assets on a schedule basis
			1. [coverage __99.95%__](#1111)
			2. [freshness all but k8s pod 10.92m __95%__](#1112)
			3. [freshness k8s pod 5.46m __95%__](#1113)
		2. RAM analyzes assets in real-time
			1. Work in progress [coverage __99.9%__](#1121)
			2. Work in progress [freshness 5.4m __95%__](#1122)

## [security  - RAM analyzes assets on a schedule basis coverage 99.95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_on_a_schedule_basis+%22coverage%22)<a name="1111"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_count{monitored_resource="cloud_run_revision",origin="scheduled",status=~"finish.*"}[28d]))
```

Count good+bad

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_count{monitored_resource="cloud_run_revision",origin="scheduled",status!="retry"}[28d]))
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-03-29
  - finish.* means good
  - retry means invalid
  - noretry means bad.

## [security  - RAM analyzes assets on a schedule basis freshness all but k8s pod 10.92m 95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_on_a_schedule_basis+%22freshness+all+but+k8s+pod+10.92m%22)<a name="1112"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_latency_e2e_bucket{monitored_resource="cloud_run_revision",microservice_name="stream2bq",origin="scheduled",asset_type!="k8s.io/Pod",status=~"finish.*",le="655.3600000000745"}[28d]))
```

Count good+bad

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_latency_e2e_count{monitored_resource="cloud_run_revision",microservice_name="stream2bq",origin="scheduled",asset_type!="k8s.io/Pod",status=~"finish.*"}[28d]))
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-01-07 fix filter reset target 95% 10.02m
- 2024-03-29 set target

## [security  - RAM analyzes assets on a schedule basis freshness k8s pod 5.46m 95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_on_a_schedule_basis+%22freshness+k8s+pod+5.46m%22)<a name="1113"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_latency_e2e_bucket{monitored_resource="cloud_run_revision",microservice_name="stream2bq",origin="scheduled",asset_type="k8s.io/Pod",status=~"finish.*",le="327.6800000000349"}[28d]))
```

Count good+bad

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_latency_e2e_count{monitored_resource="cloud_run_revision",microservice_name="stream2bq",origin="scheduled",asset_type="k8s.io/Pod",status=~"finish.*"}[28d]))
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-01-07
  - set target 95% 5.46m
  - 38k event per 28d, enabling low burn rate alert. high burn rate not activated as scheduled period > 1h

## [security  - RAM analyzes assets in real-time coverage 99.9%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_in_real-time+%22coverage%22)<a name="1121"></a>

Status: work in progress, no consequences when the error budget is exhausted

Count good

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_count{monitored_resource="cloud_run_revision",origin="real-time",status=~"finish.*"}[28d]))
```

Count good+bad

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_count{monitored_resource="cloud_run_revision",origin="real-time",status!="retry"}[28d]))
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2025-01-07
  - 24 event per sec, reset target 99.9%, only LOW burnrate alert
- 2024-03-29
  - finish.* means good
  - retry means invalid
  - noretry means bad

## [security  - RAM analyzes assets in real-time freshness 5.4m 95%](https://tbd?query=user%3Aas_a_security_analyst+goal%3Ai_want_to_identify_non_compliant_configurations+task%3Aram_analyzes_assets_in_real-time+%22freshness+5.4m%22)<a name="1122"></a>

Status: work in progress, no consequences when the error budget is exhausted

Count good

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_latency_e2e_bucket{monitored_resource="cloud_run_revision",microservice_name="stream2bq",origin="real-time", le="327.6800000000349"}[28d]))
```

Count good+bad

``` promql
sum(increase(logging_googleapis_com:user_ram_execution_latency_e2e_count{monitored_resource="cloud_run_revision",microservice_name="stream2bq",origin="real-time"}[28d]))
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-03-29
  - low traffic in in this env, keep WIP no burnrate alerts
