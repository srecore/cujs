<!-- markdownlint-disable MD010 MD024 MD033 MD038 -->
# Data crunchers SLOs spec by critical user journeys

1 users 1 goals 2 tasks 3 SLOs

_This doc is auto generated from file_ data_cujs.yml on 2025-03-06 08:15:16

1. As a data analyst
	1. I want to provide insights
		1. NSJ synchronizes usage data
			1. [coverage __96%__](#1111)
		2. I query data
			1. [availability __99.95%__](#1121)
			2. [latency 131ms __95%__](#1122)

## [data  - NSJ synchronizes usage data coverage 96%](https://tbd?query=user%3Aas_a_data_analyst+goal%3Ai_want_to_provide_insights+task%3Ansj_synchronizes_usage_data+%22coverage%22)<a name="1111"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource="global", data_product_name="appA",interval_duration_sec="86400",table_name="tableA",good_points=~"5|8|9"}[28d]))
```

Count good+bad

``` promql
sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource="global", data_product_name="appA",interval_duration_sec="86400",table_name="tableA"}[28d]))
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 202

## [data  - I query data availability 99.95%](https://tbd?query=user%3Aas_a_data_analyst+goal%3Ai_want_to_provide_insights+task%3Ai_query_data+%22availability%22)<a name="1121"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
sum(increase(serviceruntime_googleapis_com:api_request_count{monitored_resource="consumed_api",service="bigquery.googleapis.com",response_code_class="2xx"}[28d]))
```

Count good+bad

``` promql
sum(increase(serviceruntime_googleapis_com:api_request_count{monitored_resource="consumed_api",service="bigquery.googleapis.com",response_code_class=~"2xx|5xx"}[28d]))
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-03-29
  - Simple 2xx as good, 5xx as bad, meaning not considering potential retrying on 5xx leading to eventual success (2xx)

## [data  - I query data latency 131ms 95%](https://tbd?query=user%3Aas_a_data_analyst+goal%3Ai_want_to_provide_insights+task%3Ai_query_data+%22latency+131ms%22)<a name="1122"></a>

Status: SLO validated, __applying this [error budget policy](../_docs/error_budget_policy.md) when the error budget is exhausted__

Count good

``` promql
sum(increase(serviceruntime_googleapis_com:api_request_latencies_bucket{monitored_resource="consumed_api",service="bigquery.googleapis.com", le="0.131072"}[28d]))
```

Count good+bad

``` promql
sum(increase(serviceruntime_googleapis_com:api_request_latencies_count{monitored_resource="consumed_api",service="bigquery.googleapis.com"}[28d]))
```

### Rationals

| Action | Who | When |
|--------|-----|------|
| | | |

- 2024-03-29
  - No label to filter 2xx only, so this slo covers all responses
