# Grafana FinOps on Cloud Run

The key ServerLess principle from FinOps perspective is : __you paid only when you use it__

## Cloud Run pricing

[Cloud Run is billed](https://cloud.google.com/run/pricing#tier-1) by:

- vCPU-second
- GiB-second
- quantity of million request (only Cloud Run Services, no Jobs)

## Grafana Cloud Run instance(s) are active only when used as expected

The following drawing shows billable instance time (in second per second) for both the cloud run service hosting grafana and the cloud run job hosting the data syncer:

![Cloud run billable instance time](images/grafana_billable_instance_time.png)

As expected:

- The grafana cloud run service is active on day hours when used. The WebSockets form example `/api/live/ws` used by grafana keep the instance active which works as documented in [Cloud Run - Using WebSockets](https://cloud.google.com/run/docs/triggering/websockets)
- The grafana cloud job run fast every 30 minutes (the default cron schedule in the TF module). As it updates the Oauth token to the grafana service data source, stealthy little peak shows up in the grafana service at the same time. Both lead to very limited consumption.

## Optimizing memory allocation

As a starting point `750Mi` have been allocated to the grafana service. The following heatmap graph shows around 25% of memory utilization (when is instance is used):

![grafana memory utilization with 750mi](images/grafana_memory_utilization_with_750mi.png)

Cloud Run second generation execution environment as longer startup times and minimum memory allocation of 512Mi. Let's force execution environment to Gen1 and memory allocation to `750*.25/.8=235` Mi. The following graphs show both the new memory setting from 750Mi to 235Mi and the improved utilization from ~25% to ~80%.

![grafana memory utilization with 235mi](images/grafana_memory_utilization_with_235mi.png)

The data syncer tool is hosted in a Cloud Run Job, meaning Gen2 only, meaning 512Mi minimum allocation, which is also the default. By the way, it runs for ~30sec including startup time from log entry info, so memory allocation down sizing is not a priority.
