# Our error budget policy

## Goals

The goals of this policy are to:

- Protect service users from repeated service disruptions.
- Incentivize Product management / Product development / Product operations teams to thoughtfully balance velocity and reliability.
- Provide a concrete schedule and explicit permission for reliability work.

One important non-goal is punishment. Just as postmortems are not about blaming, freezes are not about punishing.

This policy requires certain teams to focus on reliability, which usually results in conflicts with previously-scheduled transformation plans and  product launches, feature enhancements, and other engineering work as defined is the __roadmaps__. We know these conflicts can generate stress. Through good communication, respect, and flexibility, teams can move through freezes with minimal stress and without a feeling of being punished.

## Building trust by exposing transparent SLO specifications

- The service Levels is the proportion of Good events / Good + Bad event for a period of time, frequently 28 days.So it is always a percentage %.
  - SLI Service Level Indicator: the measurement
  - SLO Service Level Objective: the target

Typical SLOs for request/response based systems (APIs) are:

|Type|Definition|
|----|----------|
|Availability|Proportion of valid request served successfully|
|Latency xx threshold|Proportion of valid request served faster than xx threshold|
|Quality|Proportion of valid request served without degrading quality|

Typical SLOs for data pipelines are:

|Type|Definition|
|----|----------|
|Correctness|Proportion of valid data processed correctly|
|Freshness xx threshold|Proportion of valid data processed more recently than xx threshold|
|Coverage|Proportion of valid data processed without discarding, missing, duplicates|
|Throughput xx threshold|Proportion of valid events where the processing rate is greater of equal to xx threshold|

- The SLOs are are set by the teams under the lead of Product Owner-TechLead and change to be aligned with the seasonality of the business.
- The SLOs specifications are transparently shared from the root of this repository
  - Aligned with our Critical User Journeys CUJs as User / Goal / Task.
  - Each teams specifies his SLOs as code in YAML format file which is the single source of truth.
  - This single source of truth is used to:
    - Automatically provision SLO in the digital monitoring platform (as Prometheus recording rules)
    - Automatically generated easy to read SLO specification (as a markdown file per team)

## Focusing on the Error budget

An error budget is the amount of unavailability the SLO tolerates.

- Error budget objective = 100% - SLO
- Error budget measured = 100% - SLI

Example

- If SLO = 99.9% then Error Budget = 0.1%
- if SLI = 99.8% then Error Budget measured = 0.2%, which is +100% over the agreed budget

So the error budget is the sensitive indicator we are focusing on. That budget is consumed every time a user request isn't serviced acceptably – whether because the user receives an error, a timeout, or cannot reach the service at all. SRE balances reliability and agility through error budgets.

Launches are a major source of instability, representing roughly 70% of all outages, and the development work for launches competes for engineering attention with development work for stability. Accordingly, it is very useful to have a control mechanism which structurally supports engineering attention on stability as needed. Error budgets are that mechanism.

### Error budget report to support data driven decision making

As stated above the main decision is to know when to prioritize reliability work. Two grafana dashboards make this easy:

#### Critical user journeys - error Budget summary

![Error budgets summary example](images/grafana_error_budget_summary.png)

#### Critical user journeys - SLO details

![SLO detail example](images/grafana_slo_details.png)

## Using burnrate alerts to protect the roadmaps

When the error budget is gone, work effort is reallocated to fix what is broken. This consequence may impact the ability to respect the ETA Estimated Time Arrival of features communicated in the App roadmap. To lower the frequency this may happen alerts on error budget burnrate are configured.

Burnrate = error budget measured / error budget objective

Examples:

|Error budget objective|Error budget measured|Burnrate|Computation|
|----------------------|---------------------|--------|-----------|
|0.1%|0.05%|1/2 meaning half speed burn rate, situation normal|0.05/0.1=0.5|
|0.1%|0.2%|2 meaning burning a 2 time more than the agreed speed|0.2/0.1=2|

For alerting the burnrate is computed on shrunk aggregation periods, usually 48h et 1h. Reducing the time interval makes to measurement less stable to small fluctuation over the time line, on purpose, so that trends are visible. To avoid to noisy alerting which may be counter productive with pager fatigue, the burnrate alerts associate the shrunk period of time with a burnrate threshold

Examples:

|Type|Shrunk period|Burnrate threshold|Meaning|
|----|-------------|------------------|-------|
|Low burnrate alert|48 hours|3|The 28 days error budget has been burnt during 2 days at least 3 time faster than the nominal speed, which means the full 28 days budget will be burnt in maximum the coming 8 days if nothing is done to stop the bleeding now|
|High burnrate alert|1 hour|10|The 28 days error budget has been burnt during 1 hour at least 10 time faster than the nominal speed, which means the full 28 days budget will be burnt in maximum the coming ~3 days if nothing is done to stop the bleeding now|

Of course the shrunk period and burnrate threshold may be tuned SLO by SLO. The above setting are good starting point.

### When burnrate alert cannot be set?

When the event rate is to low, alerting on burn rate cannot be set. The spreadsheet [SRE Check min volumes of events](https://docs.google.com/spreadsheets/d/1utFCs5Hh8iYxVZWRbVQuRIK4_J4TijFm5NNmBMzlQD8/edit#gid=0) illustrates these limits.

Setting up additional traffic with a synthetic loader is a valid workaround to fix this issue, specially before and at launch time of new services. It is not a perfect solution as the synthetic loader almost never reproduce the diversity of user behaviors.

## Error budget policy

With all this background in mind, the error budget policy is:

|When|Who|Goal|Actions|
|----|---|----|-------|
|Receiving an error budget burning rate alert|The app's team|Bad news should travel fast to Limit the blast radius|<ul><li>Investigate the root cause</li><li>Confirm or deny the reliability issue impacting users leading soon to exhaust the error budget</li></ul>|
|The burnrate alert has been confirmed|The app's team|Avoid the related error budget exhaustion|<ul><li>Run a crisis call to discuss what can be reprioritized in the running sprint</li><li>Allocates ~20% of resources with the targeted skills (Back/Front/Infra ...) to fix the found reliability issue. This work is done on the "burnup" time of the on going sprint</li></ul>|
|Checking the SLOs every morning|The app's team|Make data driven decision on user happiness measurement|<ul><li>Check if any validated SLOs (with freezeWhenErrorBudgetExhausted set to true) has no more error budget</li><li>Send a slack message to the teams "SLO breached fo App xx"</li></ul>|
|Receiving an "SLO breached" message|The app's team|Recover user happiness|<ul><li>Run a crisis call and double check SLO signal is valid</li><li>Hit a postmortem activity</li><li>Communicate the impact on the roadmap to the department leaders</li><li>The ongoing work is stopped at any time during the sprint</li><li>The ongoing work can restart once the error budget has been recovered AND service may proceed with feature releases per the agreed roll-out policy</li></ul>|

### Exceptions

Exceptions to not apply this policy need to get approval from the department director. Exceptions mean once every at least one year.

## Freeze scope

This error budget policy applies to a team's App for production environment and the App hard dependencies.
