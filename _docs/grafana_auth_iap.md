# Grafana authenticate and authorize role using Identity Aware Proxy

## Cloud Run + IAP configuration summary

The Cloud Run service hosting Grafana is exposed behind a Global External Load Balancer. Identity Aware Proxy IAP is set on the backend that expose Cloud Run, meaning:

- When the user hit Grafana published URL from his browser for the first time the HTTP request has no Authorization header.
  - Then IAP redirect the user to Google Identity login. On successful login the user get a valid IAP token
- When the user hit Grafana published URLs the browser add the Authorization header which contain the IAP token
  - IAP validate the token. If not valid (aka forged) the access is denied
  - IAP integrates with GCP IAM. IAM grant or deny access based on the IAM policy set on the related load balance backend.

## How to automatically manage Grafana User with IAP?

Grafana offers [multiple authentication configurations](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/). To integrate with IAP we are using [JWT authentication](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/jwt/).

An option would be to set the configuration with a `grafana.ini` file hosted either on Cloud Filestore ou Cloud Storage, but that would bring extra complexity and cost. So we stick to [override the `grafana.ini` setting with environment variables](https://grafana.com/docs/grafana/latest/setup-grafana/configure-grafana/#override-configuration-with-environment-variables) and using secret manager to manage the value when needed.

The environment variables to configure Grafana authentication with IAP are set in the [Terraform cloud run resource](../_tools/infra/dashboards/main.tf) as:

```Terraform
      env {
        name = "GF_AUTH_JWT_ENABLED"
        value = "true"
      }
      env {
        name = "GF_AUTH_JWT_HEADER_NAME"
        value = "x-goog-iap-jwt-assertion"
      }
      env {
        name = "GF_AUTH_JWT_USERNAME_CLAIM"
        value = "sub"
      }
      env {
        name = "GF_AUTH_JWT_EMAIL_CLAIM"
        value = "email"
      }
      env {
        name = "GF_AUTH_JWT_AUTO_SIGN_UP"
        value = "true"
      }
      env {
        name = "GF_AUTH_JWT_JWK_SET_URL"
        value = "https://www.gstatic.com/iap/verify/public_key-jwk"
      }
      env {
        name = "GF_AUTH_JWT_CACHE_TTL"
        value = "60m"
      }
      env {
        name = "GF_AUTH_JWT_EXPECT_CLAIMS"
        value = "{\"iss\": \"https://cloud.google.com/iap\",\"aud\": \"${var.audience_dashboards}\"}"
      }
```

With this settings Grafana only authenticate with IAP, performing the following actions:

- Validate the token to Google (already done by IAP, but defense in depth is OK), using the `GF_AUTH_JWT_EXPECT_CLAIMS` settings. Note the audience string has been crafted directly by our Terraform load balancer module.
- For each new user validated by IAP, grafana will create a new grafana user which username is like `accounts.google.com:<>id number>` and Email is email, as `GF_AUTH_JWT_AUTO_SIGN_UP` is set to `true`. The `GF_AUTH_JWT_USERNAME_CLAIM` set to retrieve the `accounts.google.com:<>id number>` string from the token / payload / claim named `sub`, the subject. The `GF_AUTH_JWT_EMAIL_CLAIM` quite obviously to retrieve the email string from token / payload / claim named `email`
- As there is so far no RBAC set, Grafana will provide by default `Viewer` role.

## How to add role management when Grafana authenticate with IAP?

- It is not possible to add claims to the [IAP token payload](https://cloud.google.com/iap/docs/signed-headers-howto#verifying_the_jwt_payload), more specifically a `role` claim.
- It is possible to to so with Google Identity Platform IP, using a cloud function on the flow, but if IAP is set to use external identities (meaning Identity Platform) then it [breaks IAP IAM integration](https://cloud.google.com/iap/docs/customizing#authenticating_with), and will then requires to use Workforce Identity Federation for the authorization part in IAP. This would complexity: Identify Platform + Cloud Function with role claim mechanism + Workload Identity federation.

Let's stick to simple IAP with IAM doing the following:

A JMESPath (aka a query on JSON) can be set in the `GF_AUTH_JWT_ROLE_ATTRIBUTE_PATH` variable, returning one of the Grafana roles (None, Viewer, Editor, or Admin) so that Grafana grant the role to the user from data in the JWT.

Let's leverage the user email string from the JWP and map it in the JMESPath code. Example:

Given the JWT contains at least the following payload:

```JSON
{
  "email": "user3@example.com",
}
```

When the following JMESPath is queried on this JSON:

```JMESPath
contains(['user5@example.com', 'user6@example.com'], email) && 'GrafanaAdmin' || 
contains(['user3@example.com', 'user4@example.com'], email) && 'Admin' || 
contains(['user1@example.com', 'user2@example.com'], email) && 'Editor' || 
'Viewer'
```

Then the following string is returned:

```text
"Admin"
```

  As you notice, when no match is found, this code provides `Viewer` role, which is also Grafana default behavior. You can adapt and test the above code snippets to your environment using [JMESPath playground](https://play.jmespath.org/):

![JMESPath playground](images/jmespath_playground_example.png)

The JMESPath code can be securely stored / lifecycled in a secret in Cloud Secret Manager, adding the following to the terraform code:

```terraform
      env {
        name = "GF_AUTH_JWT_ROLE_ATTRIBUTE_PATH"
        value_source {
          secret_key_ref {
            secret  = "projects/${var.project_id}/secrets/grafana-jwt-role-attribute-path"
            version = "latest"
          }
        }
      }
```

The service account used to deploy the terraform code must have the permission to read this secret.

To update the RBAC model:

- Create a new version of the secret with the JMESPath code updated
- Redeploy Cloud Run with the same settings so it update the related environment variable

Once set, Grafana accounts are automatically created (synced via JWT) with the expected role assignment, up to GrafanaAdmin, example:

![grafana account synced via jwt with grafana admin role](images/grafana_synced_via_jwt_grafana_admin_role.png)]
