# Terraform code to deploy Critical User Journey SLOs, burnrate alerts and dashboards to Cloud Monitoring

The terraform code in this folder deploys the CUJs SLOs, burnrate alerts and custom dashboards to Cloud Monitoring. It is executed in the following flow.

The Terraform code to deploy the Cloud Build trigger of the CD pipeline box of this flow is located in the folder `_tools_gcb`.

![Cloud run billable instance time](images/pipeline_gcb_tf_gcm.png)

## Key points

- When pushing to a feature branch with an open Merge/Pull Request to the trunk branch, that include changes to the team CUJS files, terraform apply the related SLOs and burnrate alerts to Cloud Monitoring resources
  - Why?
    - A successful `terraform plan` does not guarantee a successful `Terraform apply` as some errors can be detected only when consumed the underlying API (here, GCP monitoring API). For example a syntax error in an SLO monitoring filter.
    - It means performing `terraform apply` on merge to trunk branch is too late. If error occurs at this time it requires to open a new issue / branch which is too much toil.
- Detecting which Critical User Journey file(s) has been updated is based on a `git diff` command which means the changes MUST be `git add` and `git commit` to be detected:
  - It looks obvious for the `terraform apply` performed remotely by the cloud build trigger after a `git push`.
  - It is less obvious when performing the local `terraform plan` but needed as detection is git diff based.
- The same tf state / prefix is used for:
  - default workspace: cloud build triggers and related resources
  - per team workspace: SLOs, alerts on burnrate, and dashboards

## How to deploy CUJs SLOs, burnrate alerts and custom dashboards to Cloud Monitoring

### Set environment variables

```shell
# to be run as source ensuring env vars to be available after the script completes  
# source ./set_env_vars.sh

export PROJECT_ID="your-project-id"
export GOOGLE_APPLICATION_CREDENTIALS="your-credentials.json"

export TERRAFORM_BE_BUCKET_NAME="your-tfstate-bucket-name"
export TERRAFORM_BE_PREFIX="sre-gcb"

export REPO_NAME="srecore-cujs" # update if needed
export TEAM_FOLDER_NAME="teams_w_gcm" # update if needed
```

### Deploy

```shell
make tfinit
# update slo config in cuj yaml file(s)
git add
git commit
make tfplan
git push # push is triggering the remote pipeline performing the terraform apply
```
