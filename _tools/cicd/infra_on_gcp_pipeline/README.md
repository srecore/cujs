# Infrastructure on Google Managed Platform pipeline

## Prerequisites

The connection to between Cloud Build and GitLab already exists (2nd gen), named `gitlab` by default.

Set up environment variables as:

```shell
# to be run as source ensuring env vars to be available after the script completes  
# source ./set_env_vars.sh

# Infra
export TERRAFORM_VERSION="1.11.0-*"

# Infra production environment
export INFRA_PRD_PROJECT_ID="your-project-id"
export INFRA_PRD_REGION="europe-west1"

export INFRA_PRD_TERRAFORM_BE_BUCKET_NAME="your-tfstate-bucket-name"
export INFRA_PRD_TERRAFORM_BE_PREFIX="your-bucket-prefix"

export INFRA_PRD_DNS_NAME="sre.your.company.com"
export INFRA_PRD_DATA_SOURCE_UIDS="qwerty12345"
export INFRA_PRD_DIRECTORY_CUSTOMER_ID="A1A1a1A1A1"
export INFRA_PRD_DIRECTORY_DNS_NAME="id.your.company.com"

# Pipeline

export PIPELINE_PROJECT_ID="your-project-id"
export PIPELINE_REGION="europe-west1"

export PIPELINE_TERRAFORM_BE_BUCKET_NAME="brunore-tfstate-orgops"
export PIPELINE_TERRAFORM_BE_PREFIX="infra-on-gcp"

export REPO_NAME="srecore-cujs"
export CONNECTION="gitlab"
export GOOGLE_APPLICATION_CREDENTIALS="/home/brunoreboul/keys/sreiac.json"
