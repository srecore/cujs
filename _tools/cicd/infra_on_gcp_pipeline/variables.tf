/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# Infra

variable "terraform_version" {
  description = "Terraform version use inside the pipeline to deploy the app"
  type = string
}

# Infra Environment PRODUCTION
variable "infra_prd_terraform_be_bucket_name" {
  description = "The deployed App GCS bucket name to store the terraform state in production environment"
  type = string
}

variable "infra_prd_terraform_be_prefix" {
  description = "The deployed App Prefix in the GCS bucket to isolate the terraform states"
  type = string
}

variable "infra_prd_project_id" {
  description = "The deployed App GCP project id in production environment"
  type        = string
}

variable "infra_prd_region" {
  description = "The deployed GCP region in production environment"
  type        = string
  default     = "europe-west1"
}

variable "infra_prd_dns_name" {
  description = "DNS name used by the Global External Load Balancer"
  type = string
}

variable "infra_prd_data_source_uids" {
  description = "Grafana datasource UIDS, to be used by GMP data source syncer to update the token"
  type = string
}

variable "infra_prd_directory_customer_id" {
  description = "The directory customer ID related to the GCP organization"
  type = string
}

variable "infra_prd_directory_dns_name" {
  description = "The directory DNS name related to the GCP organization"
  type = string
}

# Pipeline
variable "pipeline_project_id" {
  description = "The pipeline Cloud Deploy pipeline GCP project id"
  type        = string
}

variable "pipeline_region" {
  description = "The pipeline gcp region"
  type        = string
  default     = "europe-west1"
}

variable "repo_name" {
  description = "Gitlab repo name"
  type = string
}

variable "connection" {
  description = "Cloud Build connection to git repo platform"
  type = string
}