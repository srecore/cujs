/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  pipeline_name = "infra-on-gcp"
}

# Cloud Build
resource "google_service_account" "cbuild_custom_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcb-${local.pipeline_name}"
  display_name = "Cloud Build Infra on GCP"
  description  = "Used to automate the deployment of GCP infrastructure resources"
}

resource "google_storage_bucket" "cloud_build_logs" {
  project  = var.pipeline_project_id
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-cloud-build-logs"
  location = var.pipeline_region
}

resource "google_project_iam_member" "custom_cloud_build_sa_srv_agt" {
  project = var.pipeline_project_id
  role    = "roles/cloudbuild.serviceAgent"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_project_iam_member" "custom_cloud_build_sa_deploy_releaser" {
  project = var.pipeline_project_id
  role    = "roles/clouddeploy.releaser"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_cloudbuild_trigger" "onPR2Trunk-create-release" {
  depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
  project         = var.pipeline_project_id
  location        = var.pipeline_region
  name            = "onPR2Trunk-${local.pipeline_name}"
  description     = "When pushing to a feature branch with an open Merge/Pull Request to the trunk branch, that include changes to the Terraformed infrastructure, create a Cloud Deploy release to Terraform plan, review, apply"
  service_account = google_service_account.cbuild_custom_sa.id
  tags            = [local.pipeline_name]
  repository_event_config {
    pull_request {
      branch          = "^main$"
      invert_regex    = false
      comment_control = "COMMENTS_ENABLED_FOR_EXTERNAL_CONTRIBUTORS_ONLY"
    }
    repository = "projects/${var.pipeline_project_id}/locations/${var.pipeline_region}/connections/${var.connection}/repositories/${var.repo_name}"
  }
  included_files = ["_tools/infra/**/*.tf",
    "_tools/infra/**/*.yaml",
    "_tools/infra/**/*.yml",
    "_tools/infra/**/*.go",
    "_tools/infra/**/go.mod",
    "_tools/infra/**/go.sum",
    "_tools/infra/**/*.json",
    "_tools/infra/**/*.js",
  "_tools/infra/**/*.ico"]
  substitutions = {
    _LOG_BUCKET_URL = google_storage_bucket.cloud_build_logs.url
    _REGION         = var.pipeline_region
  }
  build {
    logs_bucket = google_storage_bucket.cloud_build_logs.url
    options {
      logging = "GCS_ONLY"
    }
    step {
      name       = "google/cloud-sdk:slim"
      id         = "create Cloud Deploy release"
      entrypoint = "bash"
      args = ["-c", <<-EOF
    set -e
    cd _tools/infra
    pwd
    echo PROJECT_ID: $PROJECT_ID

    release_name="g-$SHORT_SHA-infra-on-gcp"
    echo "release_name: $release_name"

    gcloud deploy releases create "$release_name" --project=$PROJECT_ID --region=$_REGION --delivery-pipeline=${local.pipeline_name}-tf 2>&1 | tee /tmp/gcloud_output.txt
    exit_code=$?

    if [[ $exit_code -ne 0 ]]; then
        if grep -q "already exists" /tmp/gcloud_output.txt; then
          echo "Release $release_name already exists. Stop here without error."
        else
          echo "Error creating release $release_name"
          cat /tmp/gcloud_output.txt
          exit $exit_code
        fi
    fi
  EOF
      ]
    }
  }
}

# Cloud Deploy
resource "google_service_account" "cdeploy_prd_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-prd"
  display_name = "Cloud Deploy GCP Infrastructure in Production"
  description  = "Used to run Terraform code deploying GCP infrastructure resources in production"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_prd" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-prd"
}

resource "google_storage_bucket_iam_member" "cdeploy_prd_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_prd.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
}

resource "google_storage_bucket_iam_member" "cdeploy_prd_sa_storage_obj_user_tfstate" {
  bucket = var.infra_prd_terraform_be_bucket_name
  role   = "roles/storage.objectUser"
  member = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
}

resource "google_project_iam_member" "cdeploy_prd_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_project_iam_member" "cdeploy_prd_sa_log_writer" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/logging.logWriter"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_prd_sa" {
  service_account_id = google_service_account.cdeploy_prd_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

# IAM role to deploy the infrastructure GCP resources
resource "google_project_iam_member" "cdeploy_prd_sa_cloud_function_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/cloudfunctions.admin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_cloud_run_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/run.admin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_cloud_scheduler_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/cloudscheduler.admin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_cloud_sql_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/cloudsql.admin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_compute_instance_admin_v1" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/compute.instanceAdmin.v1"
}

resource "google_project_iam_member" "cdeploy_prd_sa_compute_load_balancer_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/compute.loadBalancerAdmin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_compute_network_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/compute.networkAdmin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_compute_security_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/compute.securityAdmin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_iap_policy_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/iap.admin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_identity_platform_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/identityplatform.admin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_oauth_config_editor" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/oauthconfig.editor"
}

resource "google_project_iam_member" "cdeploy_prd_sa_project_iam_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/resourcemanager.projectIamAdmin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_secret_mgr_secret_accessor" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/secretmanager.secretAccessor"
}

resource "google_project_iam_member" "cdeploy_prd_sa_service_account_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/iam.serviceAccountAdmin"
}

resource "google_project_iam_member" "cdeploy_prd_sa_service_account_user" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/iam.serviceAccountUser"
}

resource "google_project_iam_member" "cdeploy_prd_sa_service_usage_consumer" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/serviceusage.serviceUsageConsumer"
}

resource "google_project_iam_member" "cdeploy_prd_sa_storage_admin" {
  project = var.infra_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/storage.admin"
}

# Pipeline
resource "google_clouddeploy_target" "tf_prd" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "prd-tf-infra" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} Terraform infra in production"
  execution_configs {
    usages            = ["RENDER", "DEPLOY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_prd_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_prd.url
  }
  require_approval = true
  custom_target {
    # Already created in cuj_slos_on_gcm_pipeline
    custom_target_type = "projects/${var.pipeline_project_id}/locations/${var.pipeline_region}/customTargetTypes/terraform"
  }
}

resource "google_clouddeploy_delivery_pipeline" "terraform" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "${local.pipeline_name}-tf"
  description = "Provision GCP infrastructure resources using Terraform in a remote deployment pipeline"

  serial_pipeline {
    stages {
      target_id = google_clouddeploy_target.tf_prd.name
      deploy_parameters {
        values = {
          "customTarget/terraformVersion"           = var.terraform_version
          "customTarget/terraformBackEndBucketName" = var.infra_prd_terraform_be_bucket_name
          "customTarget/terraformBackEndPrefix"     = var.infra_prd_terraform_be_prefix
          "customTarget/projectID"                  = var.infra_prd_project_id
          "customTarget/dnsName"                    = var.infra_prd_dns_name
          "customTarget/directoryCustomerId"        = var.infra_prd_directory_customer_id
          "customTarget/directoryDNSName"           = var.infra_prd_directory_dns_name
          "customTarget/dataSourceUids"             = var.infra_prd_data_source_uids
        }
      }
    }
  }
}

