# Critical User Journeys SLOs on Google Cloud Monitoring pipeline

## Prerequisites

The connection to between Cloud Build and GitLab already exists (2nd gen), named `gitlab` by default.

Set up environment variables as:

```shell
# to be run as source ensuring env vars to be available after the script completes  
# source ./set_env_vars.sh

# App
export APP_PRD_PROJECT_ID="your-project-id"
export APP_DEV_BUCKET_SUFFIX="-codelab-srecoregcm-dev"
export APP_DEV_PATH="lab-dev"

export APP_PRD_PROJECT_ID="your-project-id"
export APP_TST_BUCKET_SUFFIX="-codelab-srecoregcm-tst"
export APP_TST_PATH="lab-test"

export APP_PRD_PROJECT_ID="your-project-id"
export APP_PRD_BUCKET_SUFFIX="-codelab-srecoregcm"
export APP_PRD_PATH="lab"

# Pipeline

export APP_PRD_PROJECT_ID="your-project-id"
export PIPELINE_REGION="europe-west1"

export PIPELINE_TERRAFORM_BE_BUCKET_NAME="your-tfstate-bucket-name"
export PIPELINE_TERRAFORM_BE_PREFIX="your-bucket-prefix"

export REPO_NAME="srecore-cujs"
export CONNECTION="gitlab"
export GOOGLE_APPLICATION_CREDENTIALS="/home/brunoreboul/keys/sreiac.json"
