/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# App Environment DEVELOPMENT
variable "app_dev_project_id" {
  description = "The deployed App GCP project id in development environment"
  type        = string
}

variable "app_dev_bucket_suffix" {
  description = "The deployed App GCS bucket name suffix to store the static web site in development environment"
  type = string
}

variable "app_dev_path" {
  description = "The path in the load balancer URL map to the development environment"
  type = string
}

# App Environment TEST
variable "app_tst_project_id" {
  description = "The deployed App GCP project id in test environment"
  type        = string
}

variable "app_tst_bucket_suffix" {
  description = "The deployed App GCS bucket name suffix to store the static web site in test environment"
  type = string
}

variable "app_tst_path" {
  description = "The path in the load balancer URL map to the test environment"
  type = string
}

# App Environment PRODUCTION
variable "app_prd_project_id" {
  description = "The deployed App GCP project id in production environment"
  type        = string
}

variable "app_prd_bucket_suffix" {
  description = "The deployed App GCS bucket name suffix to store the static web site in production environment"
  type = string
}

variable "app_prd_path" {
  description = "The path in the load balancer URL map to the production environment"
  type = string
}

# Pipeline
variable "pipeline_project_id" {
  description = "The pipeline Cloud Deploy pipeline GCP project id"
  type        = string
}

variable "pipeline_region" {
  description = "The pipeline gcp region"
  type        = string
  default     = "europe-west1"
}

variable "repo_name" {
  description = "Gitlab repo name"
  type = string
}

variable "connection" {
  description = "Cloud Build connection to git repo platform"
  type = string
}