/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  pipeline_name = "codelab-on-gcs"
}

# Cloud Build
resource "google_service_account" "cbuild_custom_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcb-${local.pipeline_name}"
  display_name = "Cloud Build  CUJs SLOs on GCM"
  description  = "Used to automate the deployment codelab app to GCS buckets"
}

resource "google_storage_bucket" "cloud_build_logs" {
  project  = var.pipeline_project_id
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-cloud-build-logs"
  location = var.pipeline_region
}

resource "google_project_iam_member" "custom_cloud_build_sa_srv_agt" {
  project = var.pipeline_project_id
  role    = "roles/cloudbuild.serviceAgent"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_project_iam_member" "custom_cloud_build_sa_deploy_releaser" {
  project = var.pipeline_project_id
  role    = "roles/clouddeploy.releaser"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

# A GitLab event triggers  building the docker image
resource "google_cloudbuild_trigger" "on_pr_2_trunk_codelab" {
  depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
  project         = var.pipeline_project_id
  location        = var.pipeline_region
  name            = "onPR2Trunk-${local.pipeline_name}"
  description     = "When push to a feature branch with an open Merge/Pull Request to the trunk branch, includes source files of this microservice, build the docker image with nginx config to host the codelab static web site"
  service_account = google_service_account.cbuild_custom_sa.id
  tags            = [local.pipeline_name, "build"]
  repository_event_config {
    pull_request {
      branch          = "^main$"
      invert_regex    = false
      comment_control = "COMMENTS_ENABLED_FOR_EXTERNAL_CONTRIBUTORS_ONLY"
    }
    repository = "projects/${var.pipeline_project_id}/locations/${var.pipeline_region}/connections/${var.connection}/repositories/${var.repo_name}"
  }
  included_files = ["Implement-SRE-core-practices-using-Google-Cloud-Platform/**/*"]
  substitutions = {
    _REGION   = var.pipeline_region
  }
  build {
    logs_bucket = google_storage_bucket.cloud_build_logs.url
    options {
      logging                 = "GCS_ONLY"
      requested_verify_option = "VERIFIED"
    }
    # Key point: Put the skaffold file in the artifact GCS buck 
    # so that they can be retrieved by the trigger based on PubSub (no git repo) that will launch the gcloud release create command. 
    # use the git short SHA as folder name in the bucket to keep strict consistency between git repo and files in GCS
    step {
      name       = "google/cloud-sdk:slim"
      id         = "create Cloud Deploy release"
      entrypoint = "bash"
      args = ["-c", <<-EOF
    set -e
    cd Implement-SRE-core-practices-using-Google-Cloud-Platform
    pwd
    echo PROJECT_ID: $PROJECT_ID

    release_name="g-$SHORT_SHA-sre-scale-gcp"
    echo "release_name: $release_name"

    gcloud deploy releases create "$release_name" --project=$PROJECT_ID --region=$_REGION --delivery-pipeline=${local.pipeline_name} 2>&1 | tee /tmp/gcloud_output.txt
    exit_code=$?

    if [[ $exit_code -ne 0 ]]; then
        if grep -q "already exists" /tmp/gcloud_output.txt; then
          echo "Release $release_name already exists. Stop here without error."
        else
          echo "Error creating release $release_name"
          cat /tmp/gcloud_output.txt
          exit $exit_code
        fi
    fi
  EOF
      ]
    }
  }
}

# Cloud Deploy in development environment
resource "google_service_account" "cdeploy_dev_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-dev"
  display_name = "Cloud Deploy Codelab on GCS in Production"
  description  = "Used to deploy Codelab on GCS in production"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_dev" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-dev"
}

resource "google_storage_bucket_iam_member" "cdeploy_dev_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_dev.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_dev_sa.email}"
}

resource "google_project_iam_member" "cdeploy_dev_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_dev_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_dev_sa" {
  service_account_id = google_service_account.cdeploy_dev_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

# IAM Role to deploy the development in development environment
resource "google_storage_bucket_iam_member" "cdeploy_dev_sa_storage_obj_user_codelab_dev_bucket" {
  bucket = "${var.app_dev_project_id}${var.app_dev_bucket_suffix}"
  role   = "roles/storage.objectUser"
  member = "serviceAccount:${google_service_account.cdeploy_dev_sa.email}"
}

# Cloud Deploy in test environment
resource "google_service_account" "cdeploy_tst_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-tst"
  display_name = "Cloud Deploy Codelab on GCS in Production"
  description  = "Used to deploy Codelab on GCS in production"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_tst" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-tst"
}

resource "google_storage_bucket_iam_member" "cdeploy_tst_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_tst.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_tst_sa.email}"
}

resource "google_project_iam_member" "cdeploy_tst_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_tst_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_tst_sa" {
  service_account_id = google_service_account.cdeploy_tst_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

# IAM Role to deploy the app in test environment
resource "google_storage_bucket_iam_member" "cdeploy_tst_sa_storage_obj_user_codelab_tst_bucket" {
  bucket = "${var.app_tst_project_id}${var.app_tst_bucket_suffix}"
  role   = "roles/storage.objectUser"
  member = "serviceAccount:${google_service_account.cdeploy_tst_sa.email}"
}

# Cloud Deploy in production environment
resource "google_service_account" "cdeploy_prd_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-prd"
  display_name = "Cloud Deploy Codelab on GCS in Production"
  description  = "Used to deploy Codelab on GCS in production"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_prd" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-prd"
}

resource "google_storage_bucket_iam_member" "cdeploy_prd_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_prd.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
}

resource "google_project_iam_member" "cdeploy_prd_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_prd_sa" {
  service_account_id = google_service_account.cdeploy_prd_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

# IAM Role to deploy the app in production environment
resource "google_storage_bucket_iam_member" "cdeploy_prd_sa_storage_obj_user_codelab_prd_bucket" {
  bucket = "${var.app_prd_project_id}${var.app_prd_bucket_suffix}"
  role   = "roles/storage.objectUser"
  member = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
}
#Pipeline
resource "google_clouddeploy_custom_target_type" "gcs" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "gcs"
  description = "Google Cloud Storage"
  custom_actions {
    render_action = "storage-diff"
    deploy_action = "storage-copy"
  }
}

resource "google_clouddeploy_target" "gcs_dev" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "dev-gcs" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} storage diff and copy in development"
  execution_configs {
    usages            = ["RENDER", "DEPLOY", "VERIFY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_dev_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_dev.url
  }
  require_approval = false
  custom_target {
    custom_target_type = google_clouddeploy_custom_target_type.gcs.id
  }
}

resource "google_clouddeploy_target" "gcs_tst" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "tst-gcs" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} storage diff and copy in test"
  execution_configs {
    usages            = ["RENDER", "DEPLOY", "VERIFY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_tst_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_tst.url
  }
  require_approval = false
  custom_target {
    custom_target_type = google_clouddeploy_custom_target_type.gcs.id
  }
}

resource "google_clouddeploy_target" "gcs_prd" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "prd-gcs" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} storage diff and copy in production"
  execution_configs {
    usages            = ["RENDER", "DEPLOY", "VERIFY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_prd_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_prd.url
  }
  require_approval = true
  custom_target {
    custom_target_type = google_clouddeploy_custom_target_type.gcs.id
  }
}

resource "google_clouddeploy_delivery_pipeline" "gcs" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = local.pipeline_name
  description = "Deploy codelab static web site to GCS buckets in a remote deployment pipeline"

  serial_pipeline {
    stages {
      target_id = google_clouddeploy_target.gcs_dev.name
      deploy_parameters {
        values = {
          "customTarget/bucketName" = "${var.app_dev_project_id}${var.app_dev_bucket_suffix}"
          "customTarget/path" = var.app_dev_path
        }
      }
      strategy {
        standard {
          verify = true
        }
      }
    }
    stages {
      target_id = google_clouddeploy_target.gcs_tst.name
      deploy_parameters {
        values = {
          "customTarget/bucketName" = "${var.app_tst_project_id}${var.app_tst_bucket_suffix}"
          "customTarget/path" = var.app_tst_path
        }
      }
      strategy {
        standard {
          verify = true
        }
      }
    }
    stages {
      target_id = google_clouddeploy_target.gcs_prd.name
      deploy_parameters {
        values = {
          "customTarget/bucketName" = "${var.app_prd_project_id}${var.app_prd_bucket_suffix}"
          "customTarget/path" = var.app_prd_path
        }
      }
      strategy {
        standard {
          verify = true
        }
      }
    }
  }
}

# deployment pipeline automation
resource "google_service_account" "cdeploy_automation_custom_sa" {
  project     = var.pipeline_project_id
  account_id   = "${local.pipeline_name}-automation"
  display_name = "Automation for Cloud Deploy for CodeLab"
  description  = "Used to run cloud deploy pipeline automation for CodeLab related delivery pipelines"
}

resource "google_project_iam_member" "custom_cloud_deploy_automation_sa_cdeploy_operator" {
  project     = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
  role    = "roles/clouddeploy.operator"
}

resource "google_service_account_iam_member" "custom_cloud_deploy_auto_sa_act_as_custom_cdeploy_dev_sa" {
  service_account_id = google_service_account.cdeploy_dev_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_deploy_auto_sa_act_as_custom_cdeploy_tst_sa" {
  service_account_id = google_service_account.cdeploy_tst_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_deploy_auto_sa_act_as_custom_cdeploy_prd_sa" {
  service_account_id = google_service_account.cdeploy_prd_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
}

resource "google_clouddeploy_automation" "on_target_ok_wait_and_promote_next" {
  name              = "on-target-ok-wait-and-promote-next"
  project           = google_clouddeploy_delivery_pipeline.gcs.project
  location          = google_clouddeploy_delivery_pipeline.gcs.location
  delivery_pipeline = google_clouddeploy_delivery_pipeline.gcs.name
  service_account   = google_service_account.cdeploy_automation_custom_sa.email
  selector {
    targets {
      id = google_clouddeploy_target.gcs_dev.target_id
    }
    targets {
      id = google_clouddeploy_target.gcs_tst.target_id
    }
  }
  suspended = false
  rules {
    promote_release_rule {
      id                    = "on-target-ok-wait-and-promote-next"
      wait                  = "60s"
      destination_target_id = "@next"
    }
  }
}
