/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  pipeline_name = "cuj-slos-on-pre"
}

# Cloud Build
resource "google_service_account" "cbuild_custom_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcb-${local.pipeline_name}"
  display_name = "Cloud Build  CUJs SLOs on Prometheus Rule Evaluator"
  description  = "Used to automate the deployment of Prometheus recording rules to implement SLIs, SLOs, Error Budgets and burnrate alerts on Prometheus Rule Evaluator from Critical User Journey YAML files"
}

resource "google_storage_bucket" "cloud_build_logs" {
  project  = var.pipeline_project_id
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-cloud-build-logs"
  location = var.pipeline_region
}

resource "google_project_iam_member" "custom_cloud_build_sa_srv_agt" {
  project = var.pipeline_project_id
  role    = "roles/cloudbuild.serviceAgent"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_project_iam_member" "custom_cloud_build_sa_deploy_releaser" {
  project = var.pipeline_project_id
  role    = "roles/clouddeploy.releaser"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_cloudbuild_trigger" "onPR2Trunk-create-release" {
  depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
  project         = var.pipeline_project_id
  location        = var.pipeline_region
  name            = "onPR2Trunk-${local.pipeline_name}"
  description     = "When pushing to a feature branch with an open Merge/Pull Request to the trunk branch, that include changes to the team CUJS files, create a Cloud Deploy release per team to kubectl apply the related SLIs, SLOs, Error budgets and burnrate alerts to Prometheus Rule Evaluator"
  service_account = google_service_account.cbuild_custom_sa.id
  tags            = [local.pipeline_name]
  repository_event_config {
    pull_request {
      branch          = "^main$"
      invert_regex    = false
      comment_control = "COMMENTS_ENABLED_FOR_EXTERNAL_CONTRIBUTORS_ONLY"
    }
    repository = "projects/${var.pipeline_project_id}/locations/${var.pipeline_region}/connections/${var.connection}/repositories/${var.repo_name}"
  }
  included_files = ["${var.team_folder_name}/**/*_rules_k8s.yml"]
  substitutions = {
    _LOG_BUCKET_URL = google_storage_bucket.cloud_build_logs.url
    _REGION         = var.pipeline_region
  }
  build {
    logs_bucket = google_storage_bucket.cloud_build_logs.url
    options {
      logging = "GCS_ONLY"
    }
    step {
      name       = "alpine/git"
      id         = "find updated critical user journeys"
      entrypoint = "sh"
      args = ["-c", <<-EOF
    set -e
    echo cujs root folder: $_CUJS_ROOT_FOLDER_NAME
    echo repo name: $REPO_NAME
    echo repo full name: $REPO_FULL_NAME
    echo branch name: $BRANCH_NAME
    echo ref name: $REF_NAME
    echo commit SHA: $COMMIT_SHA
    git fetch --depth=100 origin main
     git diff --name-only origin/main HEAD | grep "$_CUJS_ROOT_FOLDER_NAME/.*_rules_k8s.yml" | tr '\n' ' ' | tee /workspace/rules_file_list.txt
  EOF
      ]
    }
    step {
      name       = "google/cloud-sdk:slim"
      id         = "create Cloud Deploy release per team"
      entrypoint = "bash"
      args = ["-c", <<-EOF
    set -e
    cd _tools/cicd/cuj_slos_on_pre_pipeline/skaffold
    pwd
    echo PROJECT_ID: $PROJECT_ID

    for rules_file in $(cat /workspace/rules_file_list.txt); do
      echo Processing rules file $rules_file
      team=$(echo "$rules_file" | sed -E 's|^.*/(.*)_rules_k8s\.yml$|\1|')
      echo "Team: $team"

      cp "/workspace/$rules_file" team_rules.yaml
      ls -al

      team_prefix=$(echo "$team"| tr '[:upper:]' '[:lower:]' | sed 's/[^a-z0-9-]//g'| cut -c 1-15)
      release_name="g-$SHORT_SHA-$team_prefix"
      echo "release_name: $release_name"

      gcloud deploy releases create "$release_name" --project=$PROJECT_ID --region=$_REGION --delivery-pipeline=${local.pipeline_name} 2>&1 | tee /tmp/gcloud_output.txt
      exit_code=$?

      if [[ $exit_code -ne 0 ]]; then
          if grep -q "already exists" /tmp/gcloud_output.txt; then
            echo "Release $release_name already exists. Stop here without error."
          else
            echo "Error creating release $release_name"
            cat /tmp/gcloud_output.txt
            exit $exit_code
          fi
      fi
    done
  EOF
      ]
    }
  }
}

# Cloud Deploy
resource "google_service_account" "cdeploy_prd_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-prd"
  display_name = "Cloud Deploy CUJs SLOs on GCM in Production"
  description  = "Used to run kubectl apply command to  deploy SLOs, SLIs, Error Budgets, burnrate alerts in production. No dev no qa as are observability resources and must consume production data"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_prd" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-prd"
}

resource "google_storage_bucket_iam_member" "cdeploy_prd_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_prd.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
}

resource "google_project_iam_member" "cdeploy_prd_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_project_iam_member" "cdeploy_dev_sa_run_developer" {
  project = var.app_prd_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_sa.email}"
  role    = "roles/container.developer"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_prd_sa" {
  service_account_id = google_service_account.cdeploy_prd_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_clouddeploy_target" "gke_prd" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "prd-gke" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} kubectl apply in production"
  execution_configs {
    usages            = ["RENDER", "DEPLOY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_prd_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_prd.url
  }
  require_approval = true
  gke {
    cluster = "projects/${var.app_prd_project_id}/locations/${var.app_prd_region}/clusters/${var.app_prd_cluster}"
  }
}

resource "google_clouddeploy_delivery_pipeline" "cuj-slos-on-pre" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = local.pipeline_name
  description = "Deploy Prometheus recording rules on Prometheus Rule Evaluator on GKE"

  serial_pipeline {
    stages {
      target_id = google_clouddeploy_target.gke_prd.name
    }
  }
}

