# Critical User Journeys SLOs on Prometheus Rule Evaluator pipeline

This code has been tested on Prometheus Rule Evaluator PRE provided as part of Google Managed Prometheus GMP. It is expected to works equally with vanilla Prometheus or Thanos.

## Prerequisites

The connection to between Cloud Build and GitLab already exists (2nd gen), named `gitlab` by default.

Set up environment variables as:

```shell
# to be run as source ensuring env vars to be available after the script completes  
# source ./set_env_vars.sh

# App
export TEAM_FOLDER_NAME="teams_w_gcm"
export TERRAFORM_VERSION="1.11.0-*"

# App production environment

export APP_PRD_PROJECT_ID="your-project-id"
export APP_PRD_REGION="europe-west1"
export APP_PRD_CLUSTER="your-cluster"

# Pipeline

export PIPELINE_PROJECT_ID="your-project-id"
export PIPELINE_REGION="europe-west1"

export PIPELINE_TERRAFORM_BE_BUCKET_NAME="your-tfstate-bucket-name"
export PIPELINE_TERRAFORM_BE_PREFIX="your-bucket-prefix"

export REPO_NAME="srecore-cujs"
export CONNECTION="gitlab"
export GOOGLE_APPLICATION_CREDENTIALS="your-credentials.json"
```
