/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# Infra
variable "iap_project_id" {
  description = "Identity-Aware Proxy GCP project id"
  type        = string
}

# App

# App development environment
variable "app_dev_project_id" {
  description = "The deployed App GCP project id in development environment"
  type        = string
}

variable "app_dev_region" {
  description = "The deployed GCP region in development environment"
  type        = string
}

variable "app_dev_region_multi" {
  description = "The deployed GCP multi region in development environment"
  type        = string
}

# App production north environment
variable "app_prd_north_project_id" {
  description = "The deployed App GCP project id in production north environment"
  type        = string
}

variable "app_prd_north_region" {
  description = "The deployed GCP region in production north environment"
  type        = string
}

# App production south environment
variable "app_prd_south_project_id" {
  description = "The deployed App GCP project id in production south environment"
  type        = string
}

variable "app_prd_south_region" {
  description = "The deployed GCP region in production south environment"
  type        = string
}

# App production east environment
variable "app_prd_east_project_id" {
  description = "The deployed App GCP project id in production east environment"
  type        = string
}

variable "app_prd_east_region" {
  description = "The deployed GCP region in production east environment"
  type        = string
}

# Pipeline
variable "pipeline_project_id" {
  description = "The pipeline Cloud Deploy pipeline GCP project id"
  type        = string
}

variable "pipeline_region" {
  description = "The pipeline gcp region"
  type        = string
  default     = "europe-west1"
}

variable "repo_name" {
  description = "Gitlab repo name"
  type = string
}

variable "connection" {
  description = "Cloud Build connection to git repo platform"
  type = string
}

variable "artifact_registry" {
  description = "Which google artifact registry: europe, us, asia"
  type = string
}

variable "artifact_repo_name" {
  description = "Docker image repo name"
  type = string
}
