# Critical User Journeys SLOs on Google Cloud Monitoring pipeline

## Prerequisites

The connection to between Cloud Build and GitLab already exists (2nd gen), named `gitlab` by default.

Set up environment variables as:

```shell
# to be run as source ensuring env vars to be available after the script completes  
# source ./set_env_vars.sh

# Infra

export IAP_PROJECT_ID="your-project-id"

# App

export APP_DEV_PROJECT_ID="your-project-id"
export APP_DEV_REGION="europe-west1"
export APP_DEV_REGION_MULTI="europe"

export APP_PRD_NORTH_PROJECT_ID="your-project-id"
export APP_PRD_NORTH_REGION="europe-north1"

export APP_PRD_SOUTH_PROJECT_ID="your-project-id"
export APP_PRD_SOUTH_REGION="europe-southwest1"

export APP_PRD_EAST_PROJECT_ID="your-project-id"
export APP_PRD_EAST_REGION="me-west1"

# Pipeline

export PIPELINE_PROJECT_ID="your-project-id"
export PIPELINE_REGION="europe-west1"

export PIPELINE_TERRAFORM_BE_BUCKET_NAME="your-tfstate-bucket-name"
export PIPELINE_TERRAFORM_BE_PREFIX="codelab-on-gcr"

export REPO_NAME="srecore-implement-sre-core-practices"
export CONNECTION="gitlab"

export ARTIFACT_REGISTRY="europe-docker.pkg.dev"
export ARTIFACT_REPO_NAME="codelabs"

export GOOGLE_APPLICATION_CREDENTIALS="your-credentials.json"
```
