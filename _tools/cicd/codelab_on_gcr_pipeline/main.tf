/**
 * Copyright 2025 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  pipeline_name = "codelab-on-gcr"
}

data "google_project" "iap_project" {
  project_id = var.iap_project_id
}

# Cloud Run
data "google_iam_policy" "binding" {
  binding {
    role = "roles/run.invoker"
    members = [
      "serviceAccount:service-${data.google_project.iap_project.number}@gcp-sa-iap.iam.gserviceaccount.com",
    ]
  }
}

# Cloud Build
resource "google_service_account" "cbuild_custom_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcb-${local.pipeline_name}"
  display_name = "Cloud Build  CUJs SLOs on GCM"
  description  = "Used to automate the deployment of codelab app to Cloud run"
}

resource "google_storage_bucket" "cloud_build_logs" {
  project  = var.pipeline_project_id
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-cloud-build-logs"
  location = var.pipeline_region
}

resource "google_storage_bucket" "cloud_build_artifacts" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-cloud-build-artifacts"
  lifecycle_rule {
    condition {
      age = 90 # days
    }
    action {
      type = "Delete"
    }
  }
}

resource "google_project_iam_member" "custom_cloud_build_sa_srv_agt" {
  project = var.pipeline_project_id
  role    = "roles/cloudbuild.serviceAgent"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_project_iam_member" "custom_cloud_build_sa_deploy_releaser" {
  project = var.pipeline_project_id
  role    = "roles/clouddeploy.releaser"
  member  = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

# A GitLab event triggers  building the docker image
resource "google_cloudbuild_trigger" "on_pr_2_trunk_codelab" {
  depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
  project         = var.pipeline_project_id
  location        = var.pipeline_region
  name            = "onPR2Trunk-${local.pipeline_name}"
  description     = "When push to a feature branch with an open Merge/Pull Request to the trunk branch, includes source files of this microservice, build the docker image with nginx config to host the codelab static web site"
  service_account = google_service_account.cbuild_custom_sa.id
  tags            = [local.pipeline_name, "build"]
  repository_event_config {
    pull_request {
      branch          = "^main$"
      invert_regex    = false
      comment_control = "COMMENTS_ENABLED_FOR_EXTERNAL_CONTRIBUTORS_ONLY"
    }
    repository = "projects/${var.app_dev_project_id}/locations/${var.app_dev_region}/connections/${var.connection}/repositories/${var.repo_name}"
  }
  included_files = ["Implement-SRE-core-practices-using-Google-Cloud-Platform/**/*"]
  build {
    logs_bucket = google_storage_bucket.cloud_build_logs.url
    options {
      logging                 = "GCS_ONLY"
      requested_verify_option = "VERIFIED"
    }
    # Key point: Put the skaffold file in the artifact GCS buck 
    # so that they can be retrieved by the trigger based on PubSub (no git repo) that will launch the gcloud release create command. 
    # use the git short SHA as folder name in the bucket to keep strict consistency between git repo and files in GCS
    step {
      name       = "gcr.io/cloud-builders/docker"
      id         = "build docker image"
      entrypoint = "sh"
      env = [
        "SHORT_SHA=$SHORT_SHA"
      ]
      args = ["-c", <<-EOF
    set -e
    docker build -t ${var.artifact_registry}/${var.app_dev_project_id}/${var.artifact_repo_name}/srecorepracticesgcp:git-$SHORT_SHA .
EOF
      ]
    }
    images = ["${var.artifact_registry}/${var.app_dev_project_id}/${var.artifact_repo_name}/srecorepracticesgcp:git-$SHORT_SHA"]
  }
}

# A Container Analysis event triggers the release
resource "google_cloudbuild_trigger" "on_cao_built_by_cloud_build_export_sbom" {
  depends_on      = [google_project_iam_member.custom_cloud_build_sa_srv_agt]
  project         = var.pipeline_project_id
  location        = var.pipeline_region
  name            = "onCAOBuiltByCloudBuild"
  description     = "On Container Analysis Occurrence ATTESTATION with note indicating built-by-cloud-build PubSub message get occurrence details to export Software Bill Of Materials SBOM then create the cloud deploy release"
  service_account = google_service_account.cbuild_custom_sa.id
  tags            = ["cao", "attestation", "built-by-cloud-build", "pubsub", "SBOM"]
  pubsub_config {
    topic = "projects/${var.app_dev_project_id}/topics/container-analysis-occurrences-v1"
  }
  substitutions = {
    _JSON_DATA = "$(body.message.data)"
    _KIND      = "$(body.message.data.kind)"
    # _OCCURRENCE = "$(body.message.data.name)"
    _TIME     = "$(body.message.data.notificationTime)"
    _REGION   = var.pipeline_region
    _REGISTRY = "${var.artifact_registry}"
    _REPO     = "${var.artifact_repo_name}"
  }
  filter = "_KIND == \"ATTESTATION\""
  build {
    logs_bucket = google_storage_bucket.cloud_build_logs.url
    options {
      logging = "GCS_ONLY"
    }
    step {
      name       = "alpine"
      id         = "Get occurrence reference"
      entrypoint = "sh"
      args = ["-c", <<-EOF
        set -e
        apk add sed
        echo data: $_JSON_DATA
        echo "data is missing double quote for valid JSON format"
        echo $_JSON_DATA | sed -E 's|.*name:(.*),notificationTime:.*|\1|' | tee /workspace/occurrence.txt
EOF
      ]
      # Cannot use a script from disk as there is no git repo when triggering from PubSub 
      # use variable in lower case: when upper case got error message is not a valid built-in substitution
      # using the full uri in jq filter leads the shell to mis interpret the string. just using the sha256 signature is enough
    }
    step {
      name       = "google/cloud-sdk:slim"
      id         = "Export SBOM and create release"
      entrypoint = "bash"
      args = ["-c", <<-EOF
        set -ex
        apt-get update && apt-get install -y jq
        echo kind:             $_KIND
        echo notificationTime: $_TIME
        
        curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $(gcloud auth print-access-token)" https://containeranalysis.googleapis.com/v1/$(cat /workspace/occurrence.txt) | tee /workspace/attestation.json
        
        uri=$(cat /workspace/attestation.json | jq -r '.resourceUri')
        echo "uri: $uri"

        sha256=$(echo "$uri" | sed 's/.*sha256:\(.*\)/\1/')
        echo "SHA256: $sha256"
        
        if cat /workspace/attestation.json | jq -r '.noteName | contains("built-by-cloud-build")'; then
          gcloud artifacts sbom export --uri=$(cat /workspace/attestation.json | jq -r '.resourceUri')
          echo "SBOM exported to GCS"
        else
          echo "Skipping SBOM export as noteName does not contain 'built-by-cloud-build'"
        fi

        echo PROJECT_ID: $PROJECT_ID
        echo _REGISTRY: $_REGISTRY
        echo _REPO: $_REPO
        
        gcloud_cmd="gcloud artifacts docker tags list $_REGISTRY/$PROJECT_ID/$_REPO/srecorepracticesgcp --format=json | jq --arg sha256 \\\"$sha256\\\" -r '.[] | select(.version | endswith(\"$sha256\")) | .tag | split(\"/\") | last | select(startswith(\"git-\")) | ltrimstr(\"git-\")'"
        echo "Executing: $cloud_cmd"

        short_sha=$(eval "$gcloud_cmd")
        echo "short_sha: $short_sha"

        release_name="g-$short_sha-sre-scale-gcp"
        echo "release_name: $release_name"

        gsutil -m cp gs://${google_storage_bucket.cloud_build_artifacts.name}/$short_sha/* .
        gcloud deploy releases create "$release_name"  --project=$PROJECT_ID --region=$_REGION --delivery-pipeline=${local.pipeline_name} --images=codelab-image=$uri 2>&1 | tee /tmp/gcloud_output.txt
        exit_code=$?

        if [[ $exit_code -ne 0 ]]; then
            if grep -q "already exists" /tmp/gcloud_output.txt; then
              echo "Release $release_name already exists. Stop here without error."
            else
              echo "Error creating release $release_name"
              cat /tmp/gcloud_output.txt
              exit $exit_code
            fi
        fi
EOF
      ]
    }
  }
}

#Cloud Deploy

# development execution environment

resource "google_binary_authorization_policy" "policy_dev" {
  project = var.app_dev_project_id
  default_admission_rule {
    evaluation_mode  = "REQUIRE_ATTESTATION"
    enforcement_mode = "ENFORCED_BLOCK_AND_AUDIT_LOG"
    require_attestations_by = [
      "projects/${var.app_dev_project_id}/attestors/built-by-cloud-build",
    ]
  }
}

resource "google_service_account" "cdeploy_dev_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-dev"
  display_name = "Cloud Deploy ${local.pipeline_name} in development environment"
  description  = "Used to deploy the App to Cloud Run in development environment"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_dev" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-dev"
}

resource "google_storage_bucket_iam_member" "cdeploy_dev_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_dev.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_dev_sa.email}"
}

resource "google_project_iam_member" "cdeploy_dev_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_dev_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_project_iam_member" "cdeploy_dev_sa_run_developer" {
  project = var.app_dev_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_dev_sa.email}"
  role    = "roles/run.developer"
}

resource "google_artifact_registry_repository_iam_member" "cdeploy_dev_sa_artifact_registry_reader" {
  project    = var.app_dev_project_id
  location   = var.app_dev_region_multi
  repository = var.artifact_repo_name
  role       = "roles/artifactregistry.reader"
  member     = "serviceAccount:${google_service_account.cdeploy_dev_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_dev_sa" {
  service_account_id = google_service_account.cdeploy_dev_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_clouddeploy_target" "gcr_dev" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "dev-gcr" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} in development"
  execution_configs {
    usages            = ["RENDER", "DEPLOY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_dev_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_dev.url
  }
  deploy_parameters = {
    service_account = google_service_account.codelab_hosting_sa_dev.email
  }
  require_approval = false
  run {
    location = "projects/${var.app_dev_project_id}/locations/${var.app_dev_region}"
  }
}

# Cloud Run development
resource "google_service_account" "codelab_hosting_sa_dev" {
  project      = var.app_dev_project_id
  account_id   = "codelab-hosting-dev"
  display_name = "codelab hosting-dev"
  description  = "Cloud Run service service account to host codelabs in development environment"
}

resource "google_service_account_iam_member" "cdeploy_dev_sa_act_as_dev_sa" {
  service_account_id = google_service_account.codelab_hosting_sa_dev.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_dev_sa.email}"
}

# Basic place-holder to ensure cloud run service exist before setting IAM invoker role to IAP sa
resource "google_cloud_run_v2_service" "codelab_dev" {
  project  = var.app_dev_project_id
  location = var.app_dev_region
  name     = "codelab-dev"
  client   = "terraform"
  template {
    containers {
      image = "us-docker.pkg.dev/cloudrun/container/hello" # just a placeholder
    }
  }
  lifecycle {
    ignore_changes = all # key setting
  }
}

resource "google_cloud_run_service_iam_policy" "codelab-dev" {
  location = google_cloud_run_v2_service.codelab_dev.location
  project  = google_cloud_run_v2_service.codelab_dev.project
  service  = google_cloud_run_v2_service.codelab_dev.name

  policy_data = data.google_iam_policy.binding.policy_data
}

# production north execution environment

# commented as same project for dev an prd
# resource "google_binary_authorization_policy" "policy_prd_north" {
#   project = var.app_prd_north_project_id
#   default_admission_rule {
#     evaluation_mode  = "REQUIRE_ATTESTATION"
#     enforcement_mode = "ENFORCED_BLOCK_AND_AUDIT_LOG"
#     require_attestations_by = [
#       "projects/${var.app_prd_north_project_id}/attestors/built-by-cloud-build",
#     ]
#   }
# }

resource "google_service_account" "cdeploy_prd_north_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-prd-north"
  display_name = "Cloud Deploy ${local.pipeline_name} in production north environment"
  description  = "Used to deploy the App to Cloud Run in production north environment"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_prd_north" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-prd-north"
}

resource "google_storage_bucket_iam_member" "cdeploy_prd_north_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_prd_north.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_prd_north_sa.email}"
}

resource "google_project_iam_member" "cdeploy_prd_north_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_north_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_project_iam_member" "cdeploy_prd_north_sa_run_prd_runner" {
  project = var.app_prd_north_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_north_sa.email}"
  role    = "roles/run.developer"
}

resource "google_artifact_registry_repository_iam_member" "cdeploy_prd_north_sa_artifact_registry_reader" {
  project    = var.app_dev_project_id
  location   = var.app_dev_region_multi
  repository = var.artifact_repo_name
  role       = "roles/artifactregistry.reader"
  member     = "serviceAccount:${google_service_account.cdeploy_prd_north_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_prd_north_sa" {
  service_account_id = google_service_account.cdeploy_prd_north_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_clouddeploy_target" "gcr_prd_north" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "prd-north-gcr" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} in production north"
  execution_configs {
    usages            = ["RENDER", "DEPLOY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_prd_north_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_prd_north.url
  }
  deploy_parameters = {
    service_account = google_service_account.codelab_hosting_sa_prd_north.email
  }
  require_approval = false
  run {
    location = "projects/${var.app_prd_north_project_id}/locations/${var.app_prd_north_region}"
  }
}

# Cloud Run production north
resource "google_service_account" "codelab_hosting_sa_prd_north" {
  project      = var.app_prd_north_project_id
  account_id   = "codelab-hosting-prd-north"
  display_name = "codelab hosting-prd-north"
  description  = "Cloud Run service service account to host codelabs in production north environment"
}

resource "google_service_account_iam_member" "cdeploy_prd_north_sa_act_as_prd_north_sa" {
  service_account_id = google_service_account.codelab_hosting_sa_prd_north.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_prd_north_sa.email}"
}

# Basic place-holder to ensure cloud run service exist before setting IAM invoker role to IAP sa
resource "google_cloud_run_v2_service" "codelab_prd_north" {
  project  = var.app_prd_north_project_id
  location = var.app_prd_north_region
  name     = "codelab"
  client   = "terraform"
  template {
    containers {
      image = "us-docker.pkg.dev/cloudrun/container/hello" # just a placeholder
    }
  }
  lifecycle {
    ignore_changes = all # key setting
  }
}

resource "google_cloud_run_service_iam_policy" "codelab-prd_north" {
  location = google_cloud_run_v2_service.codelab_prd_north.location
  project  = google_cloud_run_v2_service.codelab_prd_north.project
  service  = google_cloud_run_v2_service.codelab_prd_north.name

  policy_data = data.google_iam_policy.binding.policy_data
}

# production south execution environment

# commented as same project for dev an prd
# resource "google_binary_authorization_policy" "policy_prd_south" {
#   project = var.app_prd_south_project_id
#   default_admission_rule {
#     evaluation_mode  = "REQUIRE_ATTESTATION"
#     enforcement_mode = "ENFORCED_BLOCK_AND_AUDIT_LOG"
#     require_attestations_by = [
#       "projects/${var.app_prd_south_project_id}/attestors/built-by-cloud-build",
#     ]
#   }
# }

resource "google_service_account" "cdeploy_prd_south_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-prd-south"
  display_name = "Cloud Deploy ${local.pipeline_name} in production south environment"
  description  = "Used to deploy the App to Cloud Run in production south environment"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_prd_south" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-prd-south"
}

resource "google_storage_bucket_iam_member" "cdeploy_prd_south_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_prd_south.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_prd_south_sa.email}"
}

resource "google_project_iam_member" "cdeploy_prd_south_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_south_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_project_iam_member" "cdeploy_prd_south_sa_run_prd_runner" {
  project = var.app_prd_south_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_south_sa.email}"
  role    = "roles/run.developer"
}

resource "google_artifact_registry_repository_iam_member" "cdeploy_prd_south_sa_artifact_registry_reader" {
  project    = var.app_dev_project_id
  location   = var.app_dev_region_multi
  repository = var.artifact_repo_name
  role       = "roles/artifactregistry.reader"
  member     = "serviceAccount:${google_service_account.cdeploy_prd_south_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_prd_south_sa" {
  service_account_id = google_service_account.cdeploy_prd_south_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_clouddeploy_target" "gcr_prd_south" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "prd-south-gcr" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} in production south"
  execution_configs {
    usages            = ["RENDER", "DEPLOY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_prd_south_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_prd_south.url
  }
  deploy_parameters = {
    service_account = google_service_account.codelab_hosting_sa_prd_south.email
  }
  require_approval = false
  run {
    location = "projects/${var.app_prd_south_project_id}/locations/${var.app_prd_south_region}"
  }
}

# Cloud Run production south
resource "google_service_account" "codelab_hosting_sa_prd_south" {
  project      = var.app_prd_south_project_id
  account_id   = "codelab-hosting-prd-south"
  display_name = "codelab hosting-prd-south"
  description  = "Cloud Run service service account to host codelabs in production south environment"
}

resource "google_service_account_iam_member" "cdeploy_prd_south_sa_act_as_prd_south_sa" {
  service_account_id = google_service_account.codelab_hosting_sa_prd_south.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_prd_south_sa.email}"
}

# Basic place-holder to ensure cloud run service exist before setting IAM invoker role to IAP sa
resource "google_cloud_run_v2_service" "codelab_prd_south" {
  project  = var.app_prd_south_project_id
  location = var.app_prd_south_region
  name     = "codelab"
  client   = "terraform"
  template {
    containers {
      image = "us-docker.pkg.dev/cloudrun/container/hello" # just a placeholder
    }
  }
  lifecycle {
    ignore_changes = all # key setting
  }
}

resource "google_cloud_run_service_iam_policy" "codelab-prd_south" {
  location = google_cloud_run_v2_service.codelab_prd_south.location
  project  = google_cloud_run_v2_service.codelab_prd_south.project
  service  = google_cloud_run_v2_service.codelab_prd_south.name

  policy_data = data.google_iam_policy.binding.policy_data
}

# production east execution environment

# commented as same project for dev an prd
# resource "google_binary_authorization_policy" "policy_prd_east" {
#   project = var.app_prd_east_project_id
#   default_admission_rule {
#     evaluation_mode  = "REQUIRE_ATTESTATION"
#     enforcement_mode = "ENFORCED_BLOCK_AND_AUDIT_LOG"
#     require_attestations_by = [
#       "projects/${var.app_prd_east_project_id}/attestors/built-by-cloud-build",
#     ]
#   }
# }

resource "google_service_account" "cdeploy_prd_east_sa" {
  project      = var.pipeline_project_id
  account_id   = "gcd-${local.pipeline_name}-prd-east"
  display_name = "Cloud Deploy ${local.pipeline_name} in production east environment"
  description  = "Used to deploy the App to Cloud Run in production east environment"
}

resource "google_storage_bucket" "cloud_deploy_artifacts_prd_east" {
  project  = var.pipeline_project_id
  location = var.pipeline_region
  name     = "${var.pipeline_project_id}-${local.pipeline_name}-artifacts-prd-east"
}

resource "google_storage_bucket_iam_member" "cdeploy_prd_east_sa_storage_adm_artifacts" {
  bucket = google_storage_bucket.cloud_deploy_artifacts_prd_east.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.cdeploy_prd_east_sa.email}"
}

resource "google_project_iam_member" "cdeploy_prd_east_sa_deploy_runner" {
  project = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_east_sa.email}"
  role    = "roles/clouddeploy.jobRunner"
}

resource "google_project_iam_member" "cdeploy_prd_east_sa_run_prd_runner" {
  project = var.app_prd_east_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_prd_east_sa.email}"
  role    = "roles/run.developer"
}

resource "google_artifact_registry_repository_iam_member" "cdeploy_prd_east_sa_artifact_registry_reader" {
  project    = var.app_dev_project_id
  location   = var.app_dev_region_multi
  repository = var.artifact_repo_name
  role       = "roles/artifactregistry.reader"
  member     = "serviceAccount:${google_service_account.cdeploy_prd_east_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_build_sa_act_as_cdeploy_prd_east_sa" {
  service_account_id = google_service_account.cdeploy_prd_east_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cbuild_custom_sa.email}"
}

resource "google_clouddeploy_target" "gcr_prd_east" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "prd-east-gcr" # keep short, MUST contain plan to trigger terraform plan
  description = "${local.pipeline_name} in production east"
  execution_configs {
    usages            = ["RENDER", "DEPLOY"]
    execution_timeout = "3600s"
    service_account   = google_service_account.cdeploy_prd_east_sa.email
    artifact_storage  = google_storage_bucket.cloud_deploy_artifacts_prd_east.url
  }
  deploy_parameters = {
    service_account = google_service_account.codelab_hosting_sa_prd_east.email
  }
  require_approval = false
  run {
    location = "projects/${var.app_prd_east_project_id}/locations/${var.app_prd_east_region}"
  }
}

# Cloud Run production east
resource "google_service_account" "codelab_hosting_sa_prd_east" {
  project      = var.app_prd_east_project_id
  account_id   = "codelab-hosting-prd-east"
  display_name = "codelab hosting-prd-east"
  description  = "Cloud Run service service account to host codelabs in production east environment"
}

resource "google_service_account_iam_member" "cdeploy_prd_east_sa_act_as_prd_east_sa" {
  service_account_id = google_service_account.codelab_hosting_sa_prd_east.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_prd_east_sa.email}"
}

# Basic place-holder to ensure cloud run service exist before setting IAM invoker role to IAP sa
resource "google_cloud_run_v2_service" "codelab_prd_east" {
  project  = var.app_prd_east_project_id
  location = var.app_prd_east_region
  name     = "codelab"
  client   = "terraform"
  template {
    containers {
      image = "us-docker.pkg.dev/cloudrun/container/hello" # just a placeholder
    }
  }
  lifecycle {
    ignore_changes = all # key setting
  }
}

resource "google_cloud_run_service_iam_policy" "codelab-prd_east" {
  location = google_cloud_run_v2_service.codelab_prd_east.location
  project  = google_cloud_run_v2_service.codelab_prd_east.project
  service  = google_cloud_run_v2_service.codelab_prd_east.name

  policy_data = data.google_iam_policy.binding.policy_data
}

# Deployment Pipeline
resource "google_clouddeploy_target" "gcr_prd_multi" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = "prod-multi"
  description = "Production multi"
  multi_target {
    target_ids = [ google_clouddeploy_target.gcr_prd_south.target_id, google_clouddeploy_target.gcr_prd_east.target_id ]
  }
}

# Profiles means Skaffold profiles to use when rendering the manifest for this stage
resource "google_clouddeploy_delivery_pipeline" "codelabs-on-cloud-run" {
  project     = var.pipeline_project_id
  location    = var.pipeline_region
  name        = local.pipeline_name
  description = "Deploy codelabs to Cloud Run"

  serial_pipeline {
    stages {
      target_id = google_clouddeploy_target.gcr_dev.name
      profiles  = ["dev"]
    }
    stages {
      target_id = google_clouddeploy_target.gcr_prd_north.name
      profiles  = ["prod"]
      strategy {
        canary {
          runtime_config {
            cloud_run {
              automatic_traffic_control = true
            }
          }
          canary_deployment {
            percentages = [5, 25, 50]
          }
        }
      }
    }
    stages {
      target_id = google_clouddeploy_target.gcr_prd_multi.name
      profiles  = ["prod"]
    }
  }
}

# deployment pipeline automation
resource "google_service_account" "cdeploy_automation_custom_sa" {
  project     = var.pipeline_project_id
  account_id   = "gcd-auto-codelab"
  display_name = "Automation for Cloud Deploy for CodeLab"
  description  = "Used to run cloud deploy pipeline automation for CodeLab related delivery pipelines"
}

resource "google_project_iam_member" "custom_cloud_deploy_automation_sa_cdeploy_operator" {
  project     = var.pipeline_project_id
  member  = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
  role    = "roles/clouddeploy.operator"
}

resource "google_service_account_iam_member" "custom_cloud_deploy_auto_sa_act_as_custom_cdeploy_dev_sa" {
  service_account_id = google_service_account.cdeploy_dev_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_deploy_auto_sa_act_as_cdeploy_prd_north_sa" {
  service_account_id = google_service_account.cdeploy_prd_north_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_deploy_auto_sa_act_as_cdeploy_prd_south_sa" {
  service_account_id = google_service_account.cdeploy_prd_south_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
}

resource "google_service_account_iam_member" "custom_cloud_deploy_auto_sa_act_as_cdeploy_prd_east_sa" {
  service_account_id = google_service_account.cdeploy_prd_east_sa.id
  role               = "roles/iam.serviceAccountUser"
  member             = "serviceAccount:${google_service_account.cdeploy_automation_custom_sa.email}"
}

resource "google_clouddeploy_automation" "on-dev-ok-wait-and-promote-prd-north-canary" {
  name              = "on-dev-ok-wait-and-promote-prd-north-canary"
  project           = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.project
  location          = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.location
  delivery_pipeline = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.name
  service_account   = google_service_account.cdeploy_automation_custom_sa.email
  selector {
    targets {
      id = google_clouddeploy_target.gcr_dev.target_id
    }
  }
  suspended = false
  rules {
    promote_release_rule {
      id                    = "on-dev-ok-wait-and-promote-prd-north-canary"
      wait                  = "60s"
      destination_target_id = "@next"
    }
  }
}

resource "google_clouddeploy_automation" "wait-canary-delay-and-advance" {
  name              = "wait-canary-delay-and-advance"
  project           = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.project
  location          = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.location
  delivery_pipeline = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.name
  service_account   = google_service_account.cdeploy_automation_custom_sa.email
  selector {
    targets {
      id = google_clouddeploy_target.gcr_prd_north.target_id
    }
  }
  suspended = false
  rules {
    advance_rollout_rule {
      id            = "wait-canary-delay-and-advance"
      source_phases = ["canary-5"]
      wait          = "120s"
    }
  }
}

resource "google_clouddeploy_automation" "advance-progressively-with-delay-short" {
  name              = "advance-progressively-with-delay-short"
  project           = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.project
  location          = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.location
  delivery_pipeline = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.name
  service_account   = google_service_account.cdeploy_automation_custom_sa.email
  selector {
    targets {
      id = google_clouddeploy_target.gcr_prd_north.target_id
    }
  }
  suspended = false
  rules {
    advance_rollout_rule {
      id            = "advance-progressively-with-delay-short"
      source_phases = ["canary-25", "canary-50"]
      wait          = "60s"
    }
  }
}

resource "google_clouddeploy_automation" "on-prd-north-ok-wait-and-promote-remaining-" {
  name              = "on-prd-north-ok-wait-and-promote-remaining-prd"
  project           = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.project
  location          = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.location
  delivery_pipeline = google_clouddeploy_delivery_pipeline.codelabs-on-cloud-run.name
  service_account   = google_service_account.cdeploy_automation_custom_sa.email
  selector {
    targets {
      id = google_clouddeploy_target.gcr_prd_north.target_id
    }
  }
  suspended = false
  rules {
    promote_release_rule {
      id                    = "on-prd-north-ok-wait-and-promote-remaining-prd"
      wait                  = "60s"
      destination_target_id = "@next"
    }
  }
}
