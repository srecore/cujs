# Terraform code to deploy Critical User Journey SLOs, burnrate alerts and dashboards to Cloud Monitoring

The terraform code in this folder deploys the CUJs SLOs, burnrate alerts and custom dashboards to Cloud Monitoring.

## Testing commands

To deploy a specific team CUJs SLOs burn rate alerts and custom dashboards WITHOUT using `git diff`

```shell
export team="security"
terraform init -backend-config="bucket=${TERRAFORM_BE_BUCKET_NAME}" -backend-config="prefix=${TERRAFORM_BE_PREFIX}"
terraform workspace new "${team}" || terraform workspace select "${team}"
terraform plan -var "project_id=$PROJECT_ID" -var "cujs_file_path=../../teams_w_gcm/${team}_team/${team}_cujs.yml" -var "notification_channels=[]"
```

```shell
CONNECTION_NAME=$(gcloud builds connections describe gitlab --region=$REGION --project=$PROJECT_ID --format="value(name)")
echo "Connection Name: $CONNECTION_NAME"
gcloud builds repositories list --connection=$CONNECTION_NAME --region=$REGION --project=$PROJECT_ID
gcloud builds repositories describe "$CONNECTION_NAME/repositories/srecore-cujs"
gcloud builds submit "$CONNECTION_NAME/repositories/srecore-cujs" --region=$REGION --revision=$BRANCH --service-account=$CBUILD_SERVICE_ACCOUNT
```
