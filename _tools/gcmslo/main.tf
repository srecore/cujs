/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

data "google_project" "project" {
  project_id = var.project_id
}

locals {
  cujs_config = yamldecode(file(var.cujs_file_path))
  team        = local.cujs_config.team
  team_name   = local.cujs_config.teamName
  slos_flat = flatten([
    for user in local.cujs_config.criticalUserJourneys : [
      for goal in user.goals : [
        for task in goal.tasks : [
          for slo in try(task.SLOs, []) : {
            slo_id                  = slo.id
            display_name            = "${local.cujs_config.team} | ${slo.app} | ${user.user}, ${goal.goal}, ${task.task} | ${slo.type}"
            slo                     = "${task.task} | ${slo.type}"
            user_goal               = "${user.user}, ${goal.goal}"
            target                  = slo.target
            goal                    = slo.target / 100
            warn                    = 1 - ((1 - (slo.target / 100)) * 0.9)
            gauge_range_min         = 1 - ((1 - (slo.target / 100)) * 2)
            rationals               = replace(slo.description, "\"", "\\\"")
            rolling_period_days     = regex("([0-9]+)", try(slo.window, "28d"))[0]
            alignment_period_sec    = regex("([0-9]+)", try(slo.window, "28d"))[0] * 24 * 60 * 60
            sli_category            = try(slo.sli_category, "request_based")
            sli_type                = slo.sli_type
            sli_kind                = try(slo.sli_kind, "")
            filter_good             = try(slo.filter_good, "")
            filter_good_doc         = try(slo.filter_good, "") == "" ? "N/A" : replace(slo.filter_good, "\"", "\\\"")
            filter_valid            = try(slo.filter_valid, "")
            filter_valid_doc        = try(slo.filter_valid, "") == "" ? "N/A" : replace(slo.filter_valid, "\"", "\\\"")
            filter_distribution     = try(slo.filter_distribution, "")
            filter_distribution_doc = try(slo.filter_distribution, "") == "" ? "N/A" : replace(slo.filter_distribution, "\"", "\\\"")
            filter_time_series      = try(slo.filter_time_series, "")
            filter_time_series_doc  = try(slo.filter_time_series, "") == "" ? "N/A" : replace(slo.filter_time_series, "\"", "\\\"")
            window_period           = try(slo.window_period, "")
            threshold               = try(slo.threshold, 0)
            range_min               = try(slo.range_min, -9007199254740991)
            range_min_doc           = try(slo.range_min, "") == "" ? "N/A" : slo.range_min
            range_max               = try(slo.range_max, 9007199254740991)
            range_max_doc           = try(slo.range_max, "") == "" ? "N/A" : slo.range_max
          }
        ]
      ]
    ]
  ])
  alerts_flat = flatten([
    for user in local.cujs_config.criticalUserJourneys : [
      for goal in user.goals : [
        for task in goal.tasks : [
          for slo in try(task.SLOs, []) : [
            for alert in(slo.burnrate_alerts == null ? [] : slo.burnrate_alerts) : {
              display_name          = "${local.cujs_config.team} | ${slo.app} | ${user.user}, ${goal.goal}, ${task.task} | ${slo.type} | ${alert.type} burnrate ${alert.window} > ${alert.threshold}"
              slo_name              = "projects/${data.google_project.project.number}/services/${local.cujs_config.team}/serviceLevelObjectives/${slo.id}"
              loopback_period       = alert.window
              threshold             = alert.threshold
              enabled               = alert.active
              notification_channels = try(local.cujs_config.notificationChannels, var.notification_channels)
            }
          ]
        ]
      ]
    ]
  ])
}

resource "google_monitoring_custom_service" "team_service" {
  project      = var.project_id
  service_id   = local.team
  display_name = "Team ${local.team_name}"
}

resource "google_monitoring_slo" "slo" {
  # explicit dependency as using implicit dependency with service = google_monitoring_custom_service.team_service.id lead to error 404
  depends_on          = [google_monitoring_custom_service.team_service]
  for_each            = { for idx, slo in local.slos_flat : idx => slo }
  project             = var.project_id
  service             = local.team
  slo_id              = each.value.slo_id
  display_name        = each.value.display_name
  goal                = each.value.goal
  rolling_period_days = each.value.rolling_period_days

  dynamic "request_based_sli" {
    for_each = each.value.sli_category == "request_based" ? [1] : []
    content {
      dynamic "distribution_cut" {
        for_each = each.value.sli_type == "distribution_cut" ? [1] : []
        content {
          distribution_filter = each.value.filter_distribution
          range {
            min = each.value.range_min
            max = each.value.range_max
          }
        }
      }
      dynamic "good_total_ratio" {
        for_each = each.value.sli_type == "good_total_ratio" ? [1] : []
        content {
          good_service_filter  = each.value.filter_good
          total_service_filter = each.value.filter_valid
        }
      }
    }
  }

  dynamic "windows_based_sli" {
    for_each = each.value.sli_category == "windows_based" ? [1] : []
    content {
      window_period          = each.value.window_period
      good_bad_metric_filter = each.value.sli_type == "good_bad_metric_filter" ? each.value.filter_time_series : null

      dynamic "metric_sum_in_range" {
        for_each = each.value.sli_type == "metric_sum_in_range" ? [1] : []
        content {
          time_series = each.value.filter_time_series
          range {
            min = each.value.range_min
            max = each.value.range_max
          }
        }
      }
      dynamic "metric_mean_in_range" {
        for_each = each.value.sli_type == "metric_mean_in_range" ? [1] : []
        content {
          time_series = each.value.filter_time_series
          range {
            min = each.value.range_min
            max = each.value.range_max
          }
        }
      }

      dynamic "good_total_ratio_threshold" {
        for_each = each.value.sli_type == "good_total_ratio_threshold" ? [1] : []
        content {
          threshold = each.value.threshold
          performance {
            dynamic "distribution_cut" {
              for_each = each.value.sli_kind == "distribution_cut" ? [1] : []
              content {
                distribution_filter = each.value.filter_distribution
                range {
                  min = each.value.range_min
                  max = each.value.range_max
                }
              }
            }
            dynamic "good_total_ratio" {
              for_each = each.value.sli_kind == "good_total_ratio" ? [1] : []
              content {
                good_service_filter  = each.value.filter_good
                total_service_filter = each.value.filter_valid
              }
            }
          }
        }
      }
    }
  }

}

resource "google_monitoring_alert_policy" "burnrate_alert" {
  depends_on   = [google_monitoring_slo.slo]
  for_each     = { for idx, alert in local.alerts_flat : idx => alert }
  project      = var.project_id
  display_name = each.value.display_name
  combiner     = "OR"
  conditions {
    display_name = each.value.display_name
    condition_threshold {
      filter          = "select_slo_burn_rate(\"${each.value.slo_name}\", \"${each.value.loopback_period}\")"
      duration        = "0s"
      comparison      = "COMPARISON_GT"
      threshold_value = each.value.threshold
      trigger {
        count = 1
      }
    }
  }
  enabled               = each.value.enabled
  notification_channels = each.value.notification_channels
}

resource "google_monitoring_dashboard" "slo_dashboard" {
  depends_on = [google_monitoring_slo.slo]
  for_each   = { for idx, slo in local.slos_flat : idx => slo }
  project    = var.project_id
  dashboard_json = <<EOF
{
  "displayName": "${each.value.display_name}",
  "labels": {
    "${local.team}": ""
  },
  "mosaicLayout": {
    "columns": 48,
    "tiles": [
      {
        "width": 48,
        "height": 4,
        "widget": {
          "title": "${each.value.slo} ${each.value.target}%",
          "sectionHeader": {
            "subtitle": "${each.value.user_goal}",
            "dividerBelow": true
          }
        }
      },
      {
        "yPos": 4,
        "width": 12,
        "height": 12,
        "widget": {
          "title": "Error budget left fraction",
          "scorecard": {
            "gaugeView": {
              "lowerBound": -1,
              "upperBound": 1
            },
            "thresholds": [
              {
                "color": "YELLOW",
                "direction": "BELOW",
                "value": 0.1
              },
              {
                "color": "RED",
                "direction": "BELOW",
                "value": 0.00000000000000000000000000000000000000001
              }
            ],
            "timeSeriesQuery": {
              "timeSeriesFilter": {
                "aggregation": {
                  "alignmentPeriod": "60s",
                  "perSeriesAligner": "ALIGN_MEAN"
                },
                "filter": "select_slo_budget_fraction(\"${google_monitoring_slo.slo[each.key].id}\")"
              }
            }
          }
        }
      },
      {
        "xPos": 24,
        "yPos": 4,
        "width": 18,
        "height": 12,
        "widget": {
          "title": "Count good bad events",
          "timeSeriesTable": {
            "columnSettings": [
              {
                "alignment": "RIGHT",
                "column": "event_type",
                "visible": true
              },
              {
                "column": "project_id"
              },
              {
                "column": "value",
                "visible": true
              }
            ],
            "dataSets": [
              {
                "minAlignmentPeriod": "${each.value.alignment_period_sec}s",
                "timeSeriesQuery": {
                  "timeSeriesFilter": {
                    "aggregation": {
                      "alignmentPeriod": "${each.value.alignment_period_sec}s",
                      "perSeriesAligner": "ALIGN_SUM"
                    },
                    "filter": "select_slo_counts(\"${google_monitoring_slo.slo[each.key].id}\")",
                    "pickTimeSeriesFilter": {
                      "direction": "TOP",
                      "numTimeSeries": 30,
                      "rankingMethod": "METHOD_MEAN"
                    }
                  }
                }
              }
            ],
            "metricVisualization": "BAR"
          }
        }
      },
      {
        "yPos": 16,
        "width": 48,
        "height": 12,
        "widget": {
          "title": "SLO documentation",
          "collapsibleGroup": {
            "collapsed": true
          }
        }
      },
      {
        "yPos": 16,
        "width": 48,
        "height": 12,
        "widget": {
          "text": {
            "content": "### Filter good:\n```shell\n${each.value.filter_good_doc}\n```\n### Filter valid:\n```shell\n${each.value.filter_valid_doc}\n```\n### Filter distribution:\n```shell\n${each.value.filter_distribution_doc}\n```\nRange minimum ${each.value.range_min_doc} and maximum ${each.value.range_max_doc}\n---\n${each.value.range_max_doc}",
            "format": "MARKDOWN",
            "style": {
              "backgroundColor": "#FFFFFF",
              "fontSize": "FS_LARGE",
              "horizontalAlignment": "H_LEFT",
              "padding": "P_EXTRA_SMALL",
              "textColor": "#212121",
              "verticalAlignment": "V_TOP"
            }
          }
        }
      },
      {
        "yPos": 28,
        "height": 16,
        "width": 48,
        "widget": {
          "title": "Error budget left",
          "xyChart": {
            "chartOptions": {
              "mode": "COLOR"
            },
            "dataSets": [
              {
                "plotType": "LINE",
                "targetAxis": "Y1",
                "timeSeriesQuery": {
                  "timeSeriesFilter": {
                    "aggregation": {
                      "perSeriesAligner": "ALIGN_NEXT_OLDER"
                    },
                    "filter": "select_slo_budget_fraction(\"${google_monitoring_slo.slo[each.key].id}\")"
                  },
                  "unitOverride": "10^2.%"
                }
              }
            ],
            "thresholds": [
              {
                "targetAxis": "Y1",
                "value": 1,
                "label": "100% means ErrBdg not used: Innovation at risk"
              },
              {
                "targetAxis": "Y1",
                "label": "0% means ErrBdg gone: Reliability at risk"
              }
            ]
          }
        }
      },
      {
        "yPos": 44,
        "width": 48,
        "height": 16,
        "widget": {
          "xyChart": {
            "dataSets": [
              {
                "timeSeriesQuery": {
                  "timeSeriesFilter": {
                    "filter": "select_slo_counts(\"${google_monitoring_slo.slo[each.key].id}\")",
                    "aggregation": {
                      "alignmentPeriod": "${each.value.alignment_period_sec}s",
                      "perSeriesAligner": "ALIGN_SUM"
                    }
                  }
                },
                "plotType": "STACKED_BAR",
                "minAlignmentPeriod": "${each.value.alignment_period_sec}s",
                "targetAxis": "Y1"
              }
            ],
            "timeshiftDuration": "0s",
            "yAxis": {
              "label": "y1Axis",
              "scale": "LINEAR"
            },
            "chartOptions": {
              "mode": "COLOR"
            }
          },
          "title": "Count of good and bad events over the last ${each.value.rolling_period_days} days"
        }
      },
      {
        "xPos": 12,
        "yPos": 4,
        "width": 12,
        "height": 12,
        "widget": {
          "title": "SLI vs SLO fraction",
          "scorecard": {
            "gaugeView": {
              "lowerBound": ${each.value.gauge_range_min
  },
              "upperBound": 1
            },
            "thresholds": [
              {
                "color": "YELLOW",
                "direction": "BELOW",
                "value": ${each.value.warn
  }
              },
              {
                "color": "RED",
                "direction": "BELOW",
                "value": ${each.value.goal
}
              }
            ],
            "timeSeriesQuery": {
              "timeSeriesFilter": {
                "aggregation": {
                  "alignmentPeriod": "60s",
                  "perSeriesAligner": "ALIGN_MEAN"
                },
                "filter": "select_slo_compliance(\"${google_monitoring_slo.slo[each.key].id}\")"
              }
            }
          }
        }
      }
    ]
  }
}
EOF
}
