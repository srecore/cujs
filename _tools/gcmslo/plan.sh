#!/usr/bin/env bash

# Copyright 2024 Google LLC

# Licensed under the Apache License, Version 2.0 (the 'License');
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an 'AS IS' BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License

# to be run as source ensuring env vars to be available after the script completes  

git fetch origin main
git diff --name-only origin/main HEAD | grep "${TEAM_FOLDER_NAME}/.*_cujs.yml" | tr '\n' ' ' | tee cujs_file_list.txt
for cujs_file in $(cat cujs_file_list.txt); do
  echo Processing CUJS file $cujs_file
  team=$(echo "$cujs_file" | sed -E 's|^.*/(.*)_cujs\.yml$|\1|')
  echo "Team: $team"
  terraform workspace new $team || terraform workspace select $team
  terraform plan -var "project_id=$PROJECT_ID" -var "cujs_file_path=../../$cujs_file" -var "notification_channels=[]"
done