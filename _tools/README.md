# Tools

To provision SLOs, automatically @ scale using PromQL queries and Prometheus recording rules see the following folders:

- `cicd` folder contains Cloud Build + Cloud Deploy pipelines
  - `codelab_on_gcr_pipeline` containerized app to Cloud Run
  - `cuj_slos_on_gcm_pipeline` Critical User Journey SLOs, burnrate alerts, dashboards to Cloud Monitoring
  - `cuj_slos_on_pre_pipeline` Critical User Journey SLOs, burnrate alerts as Prometheus recording rules to Prometheus Rule Evaluator PRE on a GKE cluster
- `cuj2slo` folder contains the Golang tool to create the README.md documentation file and the Prometheus recording and alerting rule YAML file from the CUJs YAML files.
- `gcmslo` Terraform module to deploy SLOs, burnrate alerts, custom dashboards from CUJs YAML file to Cloud Monitoring
- `grafana` folder contains JSON exports of the two Grafana dashboards, CUJ summary and SLO detail.
- `infra` folder contains
  - `dashboards` folder contains Terraform code to deploy Grafana on Cloud Run with role management, DB, data syncer ... with IAP + IAM (only Google identities)
  - `identityplatform` folder contains Terraform code to deploy Grafana on Cloud Run with role management, DB, data syncer ... with IAP + IDP using various identity providers.
    - `authtest` folder contains Golang code for a simple app to display the Token when using IAP + IDP, deployed from source to Cloud Run.
    - `blockingfunc` folder contains node.js code for the `beforecreate` and `beforesignin` blocking function use with IDP.
  - `loadbalancer` folder contains Terraform code to provision the load balancer exposing Grafana with IAP+IAM, IAP+IDP, static public sites, `authtest` app etc ...
  - `report` folder is similar to `dashboard` folder to deploy Grafana on Cloud Run with a setup adapted to IDP and custom role claim in the token
