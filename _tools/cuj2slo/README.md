# From CUJs to SLOs

```shell
gcloud auth list
gcloud auth login
gcloud config set project $PROJECT_ID
gcloud container clusters get-credentials $CLUSTER --region $REGION --project $PROJECT_ID
kubectl config get-clusters
kubectl config get-contexts # adapt set_env_vars,sh if needed
kubectl config set-context $CONTEXT
```

## Build Prometheus recording and alerting rules for SLOs

- Change directory to a given team directory
- Execute `cuj2slo -exttags -promrr -promar`, located in the `_tools/cuj2slo/main`, pickup the compiled executable related to your Operating System (Linux, Mac AMd, Mac ARM, Windows)
  - `-exttags` add extended metric tags like team user goal task app type
  - `-promrr` generate Prometheus recording rules configuration files
  - `-promar` generate Prometheus alerting rules configuration files
  - Check the two generates files `README.md` and the one suffixed `_rules_k8s.yml`
- Tip: Tune intervals

| Context | Regular | When tuning SLOs |
|---------|---------|------------------|
| SLO | interval: 8h | interval: 10m |
| Low burnrate | interval: 1h | interval: 5m |
| High burnrate | interval: 2m | interval: 2m |

## Deploying Prometheus recording and alerting rules for SLOs

Manually:

- Dry run mode: `kubectl apply -n <namespaceName> -f <teamName>_rules_k8s.yml --dry-run`
- Applying mode: - `kubectl apply -n <namespaceName> -f <teamName>_rules_k8s.yml`
- Tps:
  - When facing error `is invalid: metadata.annotations: Too long: must have at most 262144 bytes`
  - Then first `kubectl delete` second `kubectl create`
- Visualize 8ks object in GKE console: Console / Kubernetes Engine / Object Browser
  - Filter on cluster name, Namespace, API Groups `monitoring.googleapis.com`, all kind of objects
