// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"strconv"
)

func computeShortWindowDuration(window string) (string, error) {
	d, err := parseDurationCustom(window)
	if err != nil {
		return "", err
	}

	// Google recommendation for short window to be 1 twelve of the base window
	dShort := d / 12
	dShortMinRounded := int(dShort.Minutes())
	return fmt.Sprintf("%sm", strconv.Itoa(dShortMinRounded)), nil
}
