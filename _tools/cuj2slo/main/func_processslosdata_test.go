// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProcessSLOsData(t *testing.T) {
	tests := []struct {
		name                string
		slos                []SLO
		sloBaseURL          string
		team                string
		errorBudgetPolicy   string
		user                string
		goal                string
		task                string
		tocContent          string
		bodyContent         string
		userNumber          int
		goalNumber          int
		taskNumber          int
		sloCount            int
		wantErr             bool
		expectedTOCContent  []string
		expectedBodyContent []string
	}{
		{
			name: "process SLOs data",
			slos: []SLO{
				{
					ID:                             "1015b5c8-9c4d-4ad0-8c99-788a4d7c3768",
					App:                            "App1",
					Topic:                          "Topic1",
					Type:                           "Type1",
					Target:                         99.99,
					FreezeWhenErrorBudgetExhausted: true,
					FilterGood:                     "FilterGood1",
					FilterValid:                    "FilterValid1",
					Description:                    "Description1",
				},
				{
					ID:                             "0b1244c9-e0cc-4efe-adbc-f07bf5e6a2b0",
					App:                            "App2",
					Topic:                          "Topic2",
					Type:                           "Type2",
					Target:                         99.98,
					FreezeWhenErrorBudgetExhausted: false,
					FilterGood:                     "FilterGood2",
					FilterValid:                    "FilterValid2",
					Description:                    "Description2",
				},
			},
			sloBaseURL:          sloBaseURL,
			team:                "team1",
			errorBudgetPolicy:   "ErrorBudgetPolicy1",
			user:                "User1",
			goal:                "Goal1",
			task:                "Task1",
			tocContent:          "",
			bodyContent:         "",
			userNumber:          1,
			goalNumber:          1,
			taskNumber:          1,
			sloCount:            0,
			wantErr:             false,
			expectedTOCContent:  []string{"			1. [Type1 __99.99%__](#1111)", "			2. Work in progress [Type2 __99.98%__](#1112)"},
			expectedBodyContent: []string{"## [team1 Topic1 - Task1 Type1 99.99%](https://tbd"},
		},
		{
			name: "broken error bdg url",
			slos: []SLO{
				{
					ID:                             "3d1474ad-e214-496e-90e5-afa217ff81a3",
					App:                            "App1",
					Topic:                          "Topic1",
					Type:                           "Type1",
					Target:                         99.99,
					FreezeWhenErrorBudgetExhausted: true,
					FilterGood:                     "FilterGood1",
					FilterValid:                    "FilterValid1",
					Description:                    "Description1",
				},
				{
					ID:                             "0629650b-5864-4081-b36c-70470a821feb",
					App:                            "App2",
					Topic:                          "Topic2",
					Type:                           "Type2",
					Target:                         99.98,
					FreezeWhenErrorBudgetExhausted: false,
					FilterGood:                     "FilterGood2",
					FilterValid:                    "FilterValid2",
					Description:                    "Description2",
				},
			},
			sloBaseURL:        "::\\\\?@*",
			team:              "team1",
			errorBudgetPolicy: "ErrorBudgetPolicy1",
			user:              "User1",
			goal:              "Goal1",
			task:              "Task1",
			tocContent:        "",
			bodyContent:       "",
			userNumber:        1,
			goalNumber:        1,
			taskNumber:        1,
			sloCount:          0,
			wantErr:           true,
		},
		{
			name: "duplicate slo uuid",
			slos: []SLO{
				{
					ID:                             "1015b5c8-9c4d-4ad0-8c99-788a4d7c3768",
					App:                            "App1",
					Topic:                          "Topic1",
					Type:                           "Type1",
					Target:                         99.99,
					FreezeWhenErrorBudgetExhausted: true,
					FilterGood:                     "FilterGood1",
					FilterValid:                    "FilterValid1",
					Description:                    "Description1",
				},
				{
					ID:                             "1015b5c8-9c4d-4ad0-8c99-788a4d7c3768",
					App:                            "App2",
					Topic:                          "Topic2",
					Type:                           "Type2",
					Target:                         99.98,
					FreezeWhenErrorBudgetExhausted: false,
					FilterGood:                     "FilterGood2",
					FilterValid:                    "FilterValid2",
					Description:                    "Description2",
				},
			},
			sloBaseURL:        sloBaseURL,
			team:              "team1",
			errorBudgetPolicy: "ErrorBudgetPolicy1",
			user:              "User1",
			goal:              "Goal1",
			task:              "Task1",
			tocContent:        "",
			bodyContent:       "",
			userNumber:        1,
			goalNumber:        1,
			taskNumber:        1,
			sloCount:          0,
			wantErr:           true,
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			var rulesConfig RulesConfig
			var IDs string
			err := processSLOsData(test.sloBaseURL,
				test.slos,
				&test.team,
				&test.errorBudgetPolicy,
				&test.user,
				&test.goal,
				&test.task,
				&test.tocContent,
				&test.bodyContent,
				&test.userNumber,
				&test.goalNumber,
				&test.taskNumber,
				&test.sloCount,
				&rulesConfig,
				false,
				false,
				&IDs)
			if test.wantErr {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)

				for _, expectedContent := range test.expectedTOCContent {
					if !strings.Contains(test.tocContent, expectedContent) {
						t.Errorf("missing expected TOC content:\n%s\ngot:\n%s\n", expectedContent, test.tocContent)
					}
				}
				for _, expectedContent := range test.expectedBodyContent {
					if !strings.Contains(test.bodyContent, expectedContent) {
						t.Errorf("missing expected body content:\n%s\ngot:\n%s\n", expectedContent, test.bodyContent)
					}
				}
			}
		})
	}
}
