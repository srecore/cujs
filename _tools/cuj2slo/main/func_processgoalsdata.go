// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import "fmt"

func processGoalsData(sloURL string, goals []Goal,
	team, user, errorBudgetPolicy, tocContent, bodyContent *string,
	userNumber, goalCount, taskCount, sloCount *int, rulesConfig *RulesConfig, addExtTags bool, makePromAR bool, IDs *string) {
	var goalNumber int
	for _, goal := range goals {
		goalNumber += 1
		*goalCount += 1
		*tocContent += fmt.Sprintf("\t%d. %s\n", goalNumber, goal.Goal)
		processTasksData(sloURL,
			goal.Tasks,
			team,
			user,
			&goal.Goal,
			errorBudgetPolicy,
			tocContent,
			bodyContent,
			userNumber,
			&goalNumber,
			taskCount,
			sloCount,
			rulesConfig,
			addExtTags,
			makePromAR,
			IDs)
	}
}
