// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

func generateArtifacts(yamlFilePath string, makePromRR bool, makePromAR bool, addExtTags bool, IDs *string) (message string, err error) {
	if strings.Contains(yamlFilePath, cujFileSuffix) {
		p := filepath.Clean(yamlFilePath) // fix G304
		b, err := os.ReadFile(p)
		if err != nil {
			return "", fmt.Errorf("os.ReadFile %v", err)
		} else {
			var CriticalUserJourneys CriticalUserJourneys
			err = yaml.Unmarshal(b, &CriticalUserJourneys)
			if err != nil {
				// fmt.Println(err)
				return "", fmt.Errorf("cannot unmarshal %s: %v", yamlFilePath, err)
			} else {
				fmt.Println(p)
				// fmt.Println(CriticalUserJourneys)
				var tocContent, bodyContent string
				var userNumber, goalCount, taskCount, SLOCount int
				var rulesConfig RulesConfig
				rulesConfig.APIVersion = CriticalUserJourneys.APIVersion
				rulesConfig.Kind = CriticalUserJourneys.Kind
				rulesConfig.Metadata.Name = fmt.Sprintf("%s-sre-rules", CriticalUserJourneys.Team)
				rulesConfig.Metadata.Namespace = CriticalUserJourneys.Metadata.Namespace
				rulesConfig.Metadata.Labels = CriticalUserJourneys.Metadata.Labels
				processCUJsData(sloBaseURL, CriticalUserJourneys.CriticalUserJourneys,
					&CriticalUserJourneys.Team,
					&CriticalUserJourneys.ErrorBudgetPolicy,
					&tocContent,
					&bodyContent,
					&userNumber,
					&goalCount,
					&taskCount,
					&SLOCount,
					&rulesConfig,
					addExtTags,
					makePromAR,
					IDs)

				title := "<!-- markdownlint-disable MD010 MD024 MD033 MD038 -->\n"
				title = fmt.Sprintf("%s# %s SLOs spec by critical user journeys\n\n", title, CriticalUserJourneys.TeamName)
				title = fmt.Sprintf("%s%d users %d goals %d tasks %d SLOs\n\n", title, userNumber, goalCount, taskCount, SLOCount)
				title = fmt.Sprintf("%s_This doc is auto generated from file_ %s on %s\n\n", title, yamlFilePath, time.Now().Format("2006-01-02 15:04:05"))

				markdownContent := fmt.Sprintf("%s%s\n%s", title, tocContent, bodyContent)

				if strings.HasSuffix(markdownContent, "\n\n") {
					markdownContent = markdownContent[:len(markdownContent)-1]
				}

				// documentation artifact
				outFileName, err := writeArtifact(yamlFilePath, []byte(markdownContent), "readme")
				if err != nil {
					return "", err
				}
				message = fmt.Sprintf("\n\t%s done", outFileName)

				// rule artifact
				if makePromRR {
					content, err := yaml.Marshal(&rulesConfig)
					if err != nil {
						return "", err
					}
					outFileName, err = writeArtifact(yamlFilePath, content, "promrr")
					if err != nil {
						return "", err
					}
					message = fmt.Sprintf("%s\n\t%s done", message, outFileName)
				}
			}
		}
	} else {
		message = fmt.Sprintf("file %s as not the CUJ suffix %s", yamlFilePath, cujFileSuffix)
	}
	return message, err
}
