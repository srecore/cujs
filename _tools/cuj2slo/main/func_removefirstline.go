package main

import "strings"

func removeFirstLine(s string) string {
	lines := strings.Split(s, "\n")
	if len(lines) > 1 {
		return strings.Join(lines[1:], "\n")
	}
	return ""
}
