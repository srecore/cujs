// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"net/url"
	"strings"
)

func processSLOsData(sloURL string, slos []SLO,
	team, errorBudgetPolicy, user, goal, task, tocContent, bodyContent *string,
	userNumber, goalNumber, taskNumber, sloCount *int, rulesConfig *RulesConfig, addExtTags bool, makePromAR bool, IDs *string) (err error) {
	var SLONumber int
	for _, slo := range slos {
		if strings.Contains(*IDs, slo.ID) {
			return fmt.Errorf("duplicate slo id %s", slo.ID)
		}
		*IDs = *IDs + slo.ID

		SLONumber += 1
		*sloCount += 1
		name := fmt.Sprintf("%d%d%d%d", *userNumber, *goalNumber, *taskNumber, SLONumber)
		if slo.FreezeWhenErrorBudgetExhausted {
			*tocContent += fmt.Sprintf("\t\t\t%d. [%s __%v%%__](#%s)\n", SLONumber, slo.Type, slo.Target, name)
		} else {
			*tocContent += fmt.Sprintf("\t\t\t%d. Work in progress [%s __%v%%__](#%s)\n", SLONumber, slo.Type, slo.Target, name)
		}
		subTitle := fmt.Sprintf("%s %s - %s %s %v%%", *team, slo.Topic, *task, slo.Type, slo.Target)
		u, err := url.Parse(sloURL)
		if err != nil {
			return err
		}
		parameters := url.Values{}
		// parameters.Add("query", fmt.Sprintf("%s \"%s %s\"", *team, *task, slo.Type))
		sloStatementTagValue := strings.ToLower(strings.ReplaceAll(*user, " ", "_"))
		userJourneyMapTagValue := strings.ToLower(strings.ReplaceAll(*goal, " ", "_"))
		userJourneyMapStepTagValue := strings.ToLower(strings.ReplaceAll(*task, " ", "_"))
		parameters.Add("query", fmt.Sprintf("user:%s goal:%s task:%s \"%s\"",
			sloStatementTagValue,
			userJourneyMapTagValue,
			userJourneyMapStepTagValue,
			slo.Type))

		u.RawQuery = parameters.Encode()
		subTitle = fmt.Sprintf("[%s](%s)", subTitle, u.String())
		*bodyContent += fmt.Sprintf("## %s<a name=\"%s\"></a>\n\n", subTitle, name)
		if slo.FreezeWhenErrorBudgetExhausted {
			*bodyContent += fmt.Sprintf("Status: SLO validated, __applying this [error budget policy](%s) when the error budget is exhausted__\n\n", *errorBudgetPolicy)
		} else {
			*bodyContent += "Status: work in progress, no consequences when the error budget is exhausted\n\n"
		}
		*bodyContent += fmt.Sprintf("Count good\n\n``` promql\n%s\n```\n\n", slo.FilterGood)
		*bodyContent += fmt.Sprintf("Count good+bad\n\n``` promql\n%s\n```\n\n", slo.FilterValid)
		if len(slo.Description) > 0 {
			lines := strings.Split(slo.Description, "\n")
			var description string
			for _, line := range lines {
				line = strings.TrimRight(line, "\n")
				line = strings.TrimRight(line, " ")
				line = line + "\n"
				description += line
			}
			*bodyContent += fmt.Sprintf("### Rationals\n%s\n", removeFirstLine(description))
		}
		var body string
		body = *bodyContent
		if strings.HasSuffix(body, "\n\n") {
			body = body[:len(body)-1]
		}
		*bodyContent = body

		switch strings.ToLower(slo.Platform) {
		case "prometheus":
			err := configureRules(team, user, goal, task, slo, rulesConfig, addExtTags, makePromAR)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
