// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSplitFilePath(t *testing.T) {
	tests := []struct {
		name               string
		filePath           string
		expectedFileName   string
		expectedParentPath string
	}{
		{
			name:               "path with a file name",
			filePath:           "/path/to/file.txt",
			expectedFileName:   "file.txt",
			expectedParentPath: "/path/to",
		},
		{
			name:               "path without a file name",
			filePath:           "/path/to/",
			expectedFileName:   "",
			expectedParentPath: "/path/to",
		},
		{
			name:               "empty path",
			filePath:           "",
			expectedFileName:   "",
			expectedParentPath: ".",
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			fileName, parentPath := splitFilePath(test.filePath)
			assert.Equal(t, test.expectedFileName, fileName)
			assert.Equal(t, test.expectedParentPath, parentPath)
		})
	}
}
