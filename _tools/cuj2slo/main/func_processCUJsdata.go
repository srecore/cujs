// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import "fmt"

func processCUJsData(sloURL string, CUJs []CriticalUserJourney,
	team, errorBudgetPolicy, tocContent, bodyContent *string,
	userNumber, goalCount, taskCount, sloCount *int, rulesConfig *RulesConfig, addExtTags bool, makePromAR bool, IDs *string) {
	for _, cuj := range CUJs {
		*userNumber += 1
		*tocContent += fmt.Sprintf("%d. %s\n", *userNumber, cuj.User)
		processGoalsData(sloURL,
			cuj.Goals,
			team,
			&cuj.User,
			errorBudgetPolicy,
			tocContent,
			bodyContent,
			userNumber,
			goalCount,
			taskCount,
			sloCount,
			rulesConfig,
			addExtTags,
			makePromAR,
			IDs)
	}

}
