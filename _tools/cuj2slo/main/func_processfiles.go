// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"log"
)

func processFiles(basePath string, makePromRR bool, makePromAR bool, addExtTags bool) (err error) {
	if basePath == "" {
		flag.StringVar(&basePath, "path", ".", "path to the parent folder containing CUJs YAML specification files")
		flag.BoolVar(&makePromRR, "promrr", false, "generate Prometheus recording rules configuration files")
		flag.BoolVar(&makePromAR, "promar", false, "generate Google Managed Prometheus alerting rules configuration files")
		flag.BoolVar(&addExtTags, "exttags", false, "add extended metric tags like team user goal task app type")
	}
	flag.Parse()

	yamlFilePaths, err := walkFolderTreeForCUJsYamlFiles(basePath)
	if err != nil {
		return err
	}
	var IDs string

	for _, yamlFilePath := range yamlFilePaths {
		msg, err := generateArtifacts(yamlFilePath, makePromRR, makePromAR, addExtTags, &IDs)
		if err != nil {
			log.Printf("%s", err)
		} else {
			log.Printf("%s\n", msg)
		}
	}
	return nil
}
