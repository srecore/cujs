// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
)

func TestWriteArtifact(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name         string
		yamlFilePath string
		artifactType string
		content      string
		wantErr      bool
		wantFileName string
	}{
		{
			name:         "readme ok",
			yamlFilePath: "testdata/testwalk1/bu1/d11/t111/bu1_d11_t111_cujs.yml",
			artifactType: "readme",
			content:      "This is a test.",
			wantErr:      false,
			wantFileName: "testdata/testwalk1/bu1/d11/t111/README.md",
		},
		{
			name:         "wrong path",
			yamlFilePath: "blabla/blabla_cujs.yml",
			artifactType: "readme",
			content:      "This is a test.",
			wantErr:      true,
		},
		{
			name:         "promrr ok",
			yamlFilePath: "testdata/testwalk1/bu1/d11/t111/bu1_d11_t111_cujs.yml",
			artifactType: "promrr",
			content:      "apiVersion: monitoring.googleapis.com/v1",
			wantErr:      false,
			wantFileName: "testdata/testwalk1/bu1/d11/t111/bu1_d11_t111_rules_k8s.yml",
		},
		{
			name:         "unsupported artifact type",
			artifactType: "blabla",
			wantErr:      true,
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			gotFileName, gotErr := writeArtifact(test.yamlFilePath, []byte(test.content), test.artifactType)

			if test.wantErr {
				assert.NotNil(t, gotErr)
			} else {
				assert.Nil(t, gotErr)
				assert.Equal(t, test.wantFileName, gotFileName)
				b, err := os.ReadFile(gotFileName)
				if err != nil {
					t.Errorf("os.ReadFile(%q) = %v, want %v", gotFileName, err, nil)
				}
				if diff := cmp.Diff(string(b), test.content); diff != "" {
					t.Errorf("writeArtifact(%q, %q) = %v, %v, want %v, %v\n%s", test.yamlFilePath, test.content, gotFileName, gotErr, test.wantFileName, test.wantErr, diff)
				}
			}
		})
	}
}
