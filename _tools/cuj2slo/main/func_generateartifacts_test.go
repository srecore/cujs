// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"strings"
	"testing"
)

func TestGenerateArtifacts(t *testing.T) {
	tests := []struct {
		name         string
		yamlFilePath string
		wantMessage  string
		wantErr      bool
		wantErrMsg   string
	}{
		{
			name:         "one valid yaml file",
			yamlFilePath: "testdata/testwalk1/bu1/d11/t111/bu1_d11_t111_cujs.yml",
			wantErr:      false,
			wantMessage:  "\n\ttestdata/testwalk1/bu1/d11/t111/README.md done",
		},
		{
			name:         "invalid suffix",
			yamlFilePath: "testdata/testwalk2/tic/toc/dong",
			wantErr:      false,
			wantMessage:  "file testdata/testwalk2/tic/toc/dong as not the CUJ suffix _cujs.yml",
		},
		{
			name:         "file not found",
			yamlFilePath: "testdata/testwalk100/bu1/d11/t111/bu1_d11_t111_cujs.yml",
			wantErr:      true,
			wantErrMsg:   "no such file or directory",
		},
		{
			name:         "invalid yaml content",
			yamlFilePath: "testdata/testwalk3/bu1/d11/t112/bu1_d11_t112_cujs.yml",
			wantErr:      true,
			wantErrMsg:   "could not find expected",
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			var IDs string
			gotMessage, err := generateArtifacts(test.yamlFilePath, false, false, false, &IDs)
			if test.wantErr {
				if err != nil {
					if !strings.Contains(err.Error(), test.wantErrMsg) {
						t.Errorf("generateArtifacts(%q) error = %v, want error message containing %v", test.yamlFilePath, err, test.wantErr)
					}
				} else {
					t.Errorf("generateArtifacts(%q) want an error and got %v", test.yamlFilePath, err)
				}
			} else {
				if gotMessage != test.wantMessage {
					t.Errorf("generateArtifacts(%q) = %q, want %q", test.yamlFilePath, gotMessage, test.wantMessage)
				}
			}
		})
	}
}
