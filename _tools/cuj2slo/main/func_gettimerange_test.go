// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"testing"
)

func TestGetStringsInBetween2String(t *testing.T) {
	tests := []struct {
		name          string
		str           string
		wantTimeRange string
		wantError     string
	}{
		{
			name:          "oneTimeRange",
			str:           "sum (increase(app01_request_total{}[14d]))",
			wantTimeRange: "14d",
		},
		{
			name:          "multipleIdenticalTimeRange",
			str:           "sum (increase(app01_request_total{}[28d])) - sum(increase(app01_error_total{type=~\"invalid\"}[28d])) - sum (increase(app01_error_total{type=~\"bad\"}[28d]))",
			wantTimeRange: "28d",
		},
		{
			name:      "noTimeRange",
			str:       "sum (increase(app01_request_total{}))",
			wantError: "no time range",
		},
		{
			name:      "multipleDifferentTimeRange",
			str:       "sum (increase(app01_request_total{}[28d])) - sum(increase(app01_error_total{type=~\"invalid\"}[7d])) - sum (increase(app01_error_total{type=~\"bad\"}[28d]))",
			wantError: "multiple different time range",
		},
		{
			name:          "squareBracketsInRegex",
			str:           "sum (increase(app01_request_total{uri=~\"/offers/v[0-9][^/]*$|/offers/v[0-9]/[^s].*$|/offers/v[0-9]/sellerIds.*$\"}[14d]))",
			wantTimeRange: "14d",
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			timeRange, err := getTimeRange(test.str, "}[", "])")
			if err != nil {
				if test.wantError == "" {
					t.Errorf("want no error and got %v", err)
				} else {
					if err.Error() != test.wantError {
						t.Errorf("want error %s and got error %s", test.wantError, err.Error())
					}
				}
			} else {
				if test.wantError != "" {
					t.Errorf("want error %s and got none", test.wantError)
				} else {
					if timeRange != test.wantTimeRange {
						t.Errorf("want time range %s and got %s", test.wantTimeRange, timeRange)
					}
				}
			}
		})
	}
}
