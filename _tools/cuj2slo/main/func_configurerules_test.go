// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfigureRules(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name           string
		team           string
		user           string
		goal           string
		task           string
		slo            SLO
		addExtTags     bool
		makePromAR     bool
		wantErr        bool
		wantGroupCount int
		wantErrMsg     string
	}{
		{
			name: "ok",
			team: "team1",
			user: "User1",
			goal: "Goal1",
			task: "Task1",
			slo: SLO{
				ID:                             "0b7874a8-3656-4632-b144-ce27c76158b7",
				Interval:                       "8h",
				FilterGood:                     "sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource=\"global\", data_product_name=\"appA\",interval_duration_sec=\"86400\",table_name=\"tableA\",good_points=~\"5|8|9\"}[28d]))",
				FilterValid:                    "sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource=\"global\", data_product_name=\"appA\",interval_duration_sec=\"86400\",table_name=\"tableA\"}[28d]))",
				Window:                         "28d",
				FreezeWhenErrorBudgetExhausted: true,
				App:                            "App1",
				Type:                           "availability",
				Target:                         99.9,
				Description:                    "Description1",
				BurnrateAlerts: []BurnrateAlert{
					{
						Type:      "low",
						Window:    "48h",
						Interval:  "1h",
						Threshold: 3,
						Message:   "@team1",
						Active:    true,
					},
				},
			},
			makePromAR:     true,
			addExtTags:     true,
			wantErr:        false,
			wantGroupCount: 2,
		},
		{
			name: "ok freeze false exttags true promar true",
			team: "team1",
			user: "User1",
			goal: "Goal1",
			task: "Task1",
			slo: SLO{
				ID:                             "0b7874a8-3656-4632-b144-ce27c76158b7",
				Interval:                       "8h",
				FilterGood:                     "sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource=\"global\", data_product_name=\"appA\",interval_duration_sec=\"86400\",table_name=\"tableA\",good_points=~\"5|8|9\"}[28d]))",
				FilterValid:                    "sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource=\"global\", data_product_name=\"appA\",interval_duration_sec=\"86400\",table_name=\"tableA\"}[28d]))",
				Window:                         "28d",
				FreezeWhenErrorBudgetExhausted: false,
				App:                            "App1",
				Type:                           "availability",
				Target:                         99.9,
				Description:                    "Description1",
				BurnrateAlerts: []BurnrateAlert{
					{
						Type:      "low",
						Window:    "48h",
						Interval:  "1h",
						Threshold: 3,
						Message:   "@team1",
					},
				},
			},
			addExtTags:     true,
			makePromAR:     true,
			wantErr:        false,
			wantGroupCount: 2,
		},
		{
			name: "filter good err",
			team: "team1",
			user: "User1",
			goal: "Goal1",
			task: "Task1",
			slo: SLO{
				ID:                             "0b7874a8-3656-4632-b144-ce27c76158b7",
				Interval:                       "8h",
				FilterGood:                     "sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource=\"global\", data_product_name=\"appA\",interval_duration_sec=\"86400\",table_name=\"tableA\",good_points=~\"5|8|9\"}))",
				FilterValid:                    "sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource=\"global\", data_product_name=\"appA\",interval_duration_sec=\"86400\",table_name=\"tableA\"}[28d]))",
				Window:                         "28d",
				FreezeWhenErrorBudgetExhausted: true,
				App:                            "App1",
				Type:                           "availability",
				Target:                         99.9,
				Description:                    "Description1",
				BurnrateAlerts:                 []BurnrateAlert{},
			},
			wantErr:    true,
			wantErrMsg: "filter good error no time range",
		},
		{
			name: "filter valid err",
			team: "team1",
			user: "User1",
			goal: "Goal1",
			task: "Task1",
			slo: SLO{
				ID:                             "0b7874a8-3656-4632-b144-ce27c76158b7",
				Interval:                       "8h",
				FilterGood:                     "sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource=\"global\", data_product_name=\"appA\",interval_duration_sec=\"86400\",table_name=\"tableA\",good_points=~\"5|8|9\"}[28d]))",
				FilterValid:                    "sum(increase(custom_googleapis_com:dataproducts_interval_count{monitored_resource=\"global\", data_product_name=\"appA\",interval_duration_sec=\"86400\",table_name=\"tableA\"}))",
				Window:                         "28d",
				FreezeWhenErrorBudgetExhausted: true,
				App:                            "App1",
				Type:                           "availability",
				Target:                         99.9,
				Description:                    "Description1",
				BurnrateAlerts:                 []BurnrateAlert{},
			},
			wantErr:    true,
			wantErrMsg: "filter valid error no time range",
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			var rulesConfig RulesConfig

			gotErr := configureRules(&test.team, &test.user, &test.goal, &test.task,
				test.slo, &rulesConfig, test.addExtTags, test.makePromAR)

			if test.wantErr {
				assert.NotNil(t, gotErr)
				assert.Contains(t, gotErr.Error(), test.wantErrMsg)
			} else {
				assert.Nil(t, gotErr)
				assert.Equal(t, len(rulesConfig.Spec.Groups), test.wantGroupCount)
			}
		})
	}
}
