// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestComputeShortWindowDuration(t *testing.T) {
	tests := []struct {
		name       string
		window     string
		want       string
		wantErr    bool
		wantErrMsg string
	}{
		{
			name:   "valid 28 days window",
			window: "28d",
			want:   "3360m",
		},
		{
			name:   "valid 14 days window",
			window: "14d",
			want:   "1680m",
		},
		{
			name:   "valid 7 days window",
			window: "7d",
			want:   "840m",
		},
		{
			name:   "valid 1 day window",
			window: "1d",
			want:   "120m",
		},
		{
			name:       "invalid window",
			window:     "blabla",
			wantErr:    true,
			wantErrMsg: "invalid duration string parsing regex: blabla",
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			got, err := computeShortWindowDuration(test.window)
			if test.wantErr {
				if err != nil {
					assert.Equal(t, test.wantErrMsg, err.Error())
				} else {
					t.Errorf("computeShortWindowDuration(%q) = %v, %v, want error %v", test.window, got, err, test.wantErr)
				}
			} else {
				assert.Equal(t, test.want, got)
			}
		})
	}
}
