// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"log"
)

func processTasksData(sloURL string, tasks []Task,
	team, user, goal, errorBudgetPolicy, tocContent, bodyContent *string,
	userNumber, goalNumber, taskCount, sloCount *int, rulesConfig *RulesConfig, addExtTags bool, makePromAR bool, IDs *string) {
	var taskNumber int
	for _, task := range tasks {
		taskNumber += 1
		*taskCount += 1
		*tocContent += fmt.Sprintf("\t\t%d. %s\n", taskNumber, task.Task)
		if task.Comment != "" {
			*tocContent += fmt.Sprintf("\t\t\t- _%s_\n", task.Comment)
		}
		err := processSLOsData(sloURL,
			task.SLOs,
			team,
			errorBudgetPolicy,
			user,
			goal,
			&task.Task,
			tocContent,
			bodyContent,
			userNumber,
			goalNumber,
			&taskNumber,
			sloCount,
			rulesConfig,
			addExtTags,
			makePromAR,
			IDs)
		if err != nil {
			log.Printf("%v", err)
		}
	}
}
