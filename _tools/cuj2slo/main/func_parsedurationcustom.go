// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"regexp"
	"strconv"
	"time"
)

// This func assumes a day has always 24 hours (no leap second), a week 7 days, a year 365 days (no leap day)
// time.DurationParse does not and will not support day unit https://github.com/golang/go/issues/11473
func parseDurationCustom(durationStr string) (time.Duration, error) {
	duration, err := time.ParseDuration(durationStr)
	if err == nil {
		return duration, nil
	}

	// When standard parsing fails, try our custom parsing
	durationRegex := regexp.MustCompile(`^(\d+)([ywd])$`)
	matches := durationRegex.FindStringSubmatch(durationStr)
	// with "28d" for example then the regex matches and:
	// [0] is "28d"
	// [1] is "28"
	// [2] is "d"
	if len(matches) != 3 {
		return 0, fmt.Errorf("invalid duration string parsing regex: %s", durationStr)
	}

	durationValue, err := strconv.Atoi(matches[1])
	if err != nil {
		return 0, fmt.Errorf("invalid duration value ASCII to integer conversion: %s", matches[1])
	}

	switch matches[2] {
	case "y":
		duration = time.Duration(durationValue) * 365 * 24 * time.Hour // Assuming 365 days in a year
	case "w":
		duration = time.Duration(durationValue) * 7 * 24 * time.Hour
	case "d":
		duration = time.Duration(durationValue) * 24 * time.Hour
	}

	return duration, nil
}
