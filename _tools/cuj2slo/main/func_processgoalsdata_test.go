// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestProcessGoalsData(t *testing.T) {
	tests := []struct {
		name              string
		goals             []Goal
		team              string
		user              string
		errorBudgetPolicy string
		userNumber        int
		wantToc           string
		wantBody          string
	}{
		{
			name:     "empty goals",
			goals:    []Goal{},
			wantToc:  "",
			wantBody: "",
		},
		{
			name: "single goal",
			goals: []Goal{
				{
					Goal: "goal1",
				},
			},
			wantToc:  "\t1. goal1\n",
			wantBody: "",
		},
		{
			name: "two goals",
			goals: []Goal{
				{
					Goal: "goal1",
				},
				{
					Goal: "goal2",
				},
			},
			wantToc:  "\t1. goal1\n\t2. goal2\n",
			wantBody: "",
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			var tocContent, bodyContent string
			var goalCount, taskCount, sloCount int
			var rulesConfig RulesConfig
			var IDs string
			processGoalsData(sloBaseURL, test.goals,
				&test.team, &test.user, &test.errorBudgetPolicy, &tocContent, &bodyContent,
				&test.userNumber, &goalCount, &taskCount, &sloCount, &rulesConfig, false, false, &IDs)
			if diff := cmp.Diff(test.wantToc, tocContent); diff != "" {
				t.Errorf("tocContent mismatch (-want +got):\n%s", diff)
			}
			if diff := cmp.Diff(test.wantBody, bodyContent); diff != "" {
				t.Errorf("bodyContent mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
