// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParseDurationCustom(t *testing.T) {
	tests := []struct {
		name        string
		durationStr string
		want        time.Duration
		wantErr     bool
		wantErrMsg  string
	}{
		{
			name:        "valid go duration string",
			durationStr: "24h",
			want:        24 * time.Hour,
			wantErr:     false,
		},
		{
			name:        "valid year string",
			durationStr: "2y",
			want:        2 * 365 * 24 * time.Hour,
			wantErr:     false,
		},
		{
			name:        "valid week string",
			durationStr: "3w",
			want:        3 * 7 * 24 * time.Hour,
			wantErr:     false,
		},
		{
			name:        "valid day string",
			durationStr: "28d",
			want:        28 * 24 * time.Hour,
			wantErr:     false,
		},
		{
			name:        "invalid duration string parsing regex",
			durationStr: "28",
			wantErr:     true,
			wantErrMsg:  "invalid duration string parsing regex: 28",
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			got, err := parseDurationCustom(test.durationStr)
			if test.wantErr {
				if err != nil {
					assert.Equal(t, test.wantErrMsg, err.Error())
				} else {
					t.Errorf("parseDurationCustom(%q) = %v, %v, want error %v", test.durationStr, got, err, test.wantErr)
				}
			} else {
				assert.Equal(t, test.want, got)
			}
		})
	}
}
