// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestProcessTasksData(t *testing.T) {
	tests := []struct {
		name              string
		tasks             []Task
		team              string
		user              string
		goal              string
		errorBudgetPolicy string
		sloBaseURL        string
		userNumber        int
		goalNumber        int
		wantErr           bool
		wantToc           string
		wantBody          string
		wantMsg           string
		wantTaskCount     int
		wantSLOCount      int
	}{
		{
			name:          "empty tasks",
			tasks:         []Task{},
			wantErr:       false,
			wantToc:       "",
			wantBody:      "",
			wantTaskCount: 0,
			wantSLOCount:  0,
		},
		{
			name: "one task",
			tasks: []Task{
				{
					Task: "Task 1",
				},
			},
			wantErr:       false,
			wantToc:       "\t\t1. Task 1\n",
			wantBody:      "",
			wantTaskCount: 1,
			wantSLOCount:  0,
		},
		{
			name: "two tasks",
			tasks: []Task{
				{
					Task: "Task 1",
				},
				{
					Task: "Task 2",
				},
			},
			wantErr:       false,
			wantToc:       "\t\t1. Task 1\n\t\t2. Task 2\n",
			wantBody:      "",
			wantTaskCount: 2,
			wantSLOCount:  0,
		},
		{
			name:              "task with comment",
			team:              "team1",
			user:              "user1",
			goal:              "goal1",
			userNumber:        9,
			goalNumber:        6,
			errorBudgetPolicy: "e1",
			tasks: []Task{
				{
					Task:    "Task 1",
					Comment: "This is a comment.",
				},
			},
			wantToc:       "\t\t1. Task 1\n\t\t\t- _This is a comment._\n",
			wantBody:      "",
			wantErr:       false,
			wantTaskCount: 1,
			wantSLOCount:  0,
		},
		{
			name: "dealing with slo err",
			tasks: []Task{
				{
					Task: "Task 1",
					SLOs: []SLO{
						{
							ID:                             "9705d679-0ca3-491a-92f4-ca1f6a408f36",
							App:                            "App1",
							Topic:                          "Topic1",
							Type:                           "Type1",
							Target:                         99.99,
							FreezeWhenErrorBudgetExhausted: true,
							FilterGood:                     "FilterGood1",
							FilterValid:                    "FilterValid1",
							Description:                    "Description1",
						},
					},
				},
			},
			sloBaseURL: "::\\\\?@*",
			wantErr:    true,
			wantMsg:    "missing protocol scheme",
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			var tocContent, bodyContent string
			var taskCount, sloCount int
			var buffer bytes.Buffer
			var rulesConfig RulesConfig
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			var IDs string
			processTasksData(test.sloBaseURL,
				test.tasks,
				&test.team,
				&test.user,
				&test.goal,
				&test.errorBudgetPolicy,
				&tocContent,
				&bodyContent,
				&test.userNumber,
				&test.goalNumber,
				&taskCount,
				&sloCount,
				&rulesConfig,
				false,
				false,
				&IDs)
			msgString := buffer.String()
			t.Logf("%v", msgString)
			if !test.wantErr {
				if diff := cmp.Diff(test.wantToc, tocContent); diff != "" {
					t.Errorf("processTasksData(%v) tocContent mismatch (-want +got):\n%s", test.tasks, diff)
				}
				if diff := cmp.Diff(test.wantBody, bodyContent); diff != "" {
					t.Errorf("processTasksData(%v) bodyContent mismatch (-want +got):\n%s", test.tasks, diff)
				}
				if taskCount != test.wantTaskCount {
					t.Errorf("processTasksData(%v) taskCount mismatch: want %d, got %d", test.tasks, test.wantTaskCount, taskCount)
				}
				if sloCount != test.wantSLOCount {
					t.Errorf("processTasksData(%v) sloCount mismatch: want %d, got %d", test.tasks, test.wantSLOCount, sloCount)
				}
			} else {
				if !strings.Contains(msgString, test.wantMsg) {
					t.Errorf("Want msg contains %s got %s", test.wantMsg, msgString)
				}
			}
		})
	}
}
