// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"strings"
)

func getTimeRange(str string, startS string, endS string) (timeRange string, err error) {
	var i int
	var multipleDifferentTimeRange bool
	for {
		i++
		s := strings.Index(str, startS)
		if s == -1 {
			break
		}
		newS := str[s+len(startS):]
		e := strings.Index(newS, endS)
		if e == -1 {
			break
		}
		if i > 1 {
			if newS[:e] != timeRange {
				multipleDifferentTimeRange = true
				break
			}
		}
		timeRange = newS[:e]
		str = newS[e+len(endS):]
	}
	if timeRange == "" {
		return "", fmt.Errorf("no time range")
	}
	if multipleDifferentTimeRange {
		return "", fmt.Errorf("multiple different time range")
	}
	return timeRange, nil
}
