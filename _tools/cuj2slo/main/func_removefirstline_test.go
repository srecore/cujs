// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRemoveFirstLine(t *testing.T) {
	tests := []struct {
		name string
		s    string
		want string
	}{
		{
			name: "With more than one line",
			s:    "This is the first line.\nThis is the second line.",
			want: "This is the second line.",
		},
		{
			name: "With only one line",
			s:    "This is the only line.",
			want: "",
		},
		{
			name: "With empty string",
			s:    "",
			want: "",
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			got := removeFirstLine(test.s)
			assert.Equal(t, test.want, got)
		})
	}
}
