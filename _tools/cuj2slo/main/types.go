package main

const sloBaseURL = "https://tbd"
const cujFileSuffix = "_cujs.yml"
const defaultAlertSeverity = "warning"

type CriticalUserJourneys struct {
	Team                 string                `yaml:"team"`
	TeamName             string                `yaml:"teamName"`
	ErrorBudgetPolicy    string                `yaml:"errorBudgetPolicy"`
	APIVersion           string                `yaml:"apiVersion"`
	Kind                 string                `yaml:"kind"`
	Metadata             Metadata              `yaml:"metadata"`
	CriticalUserJourneys []CriticalUserJourney `yaml:"criticalUserJourneys"`
}

type Metadata struct {
	Name      string            `yaml:"name,omitempty"`
	Namespace string            `yaml:"namespace,omitempty"`
	Labels    map[string]string `yaml:"labels,omitempty"`
}

type CriticalUserJourney struct {
	User  string `yaml:"user"`
	Goals []Goal `yaml:"goals"`
}

type Goal struct {
	Goal  string `yaml:"goal"`
	Tasks []Task `yaml:"tasks"`
}

type Task struct {
	Task    string `yaml:"task"`
	Comment string `yaml:"comment"`
	SLOs    []SLO  `yaml:"SLOs"`
}

type SLO struct {
	ID                             string          `yaml:"id"`
	Type                           string          `yaml:"type"`
	FreezeWhenErrorBudgetExhausted bool            `yaml:"freezeWhenErrorBudgetExhausted"`
	App                            string          `yaml:"app"`
	Topic                          string          `yaml:"topic"` // To be removed ?
	Target                         float64         `yaml:"target"`
	Description                    string          `yaml:"description"`
	Platform                       string          `yaml:"platform"`
	Window                         string          `yaml:"window"`
	Interval                       string          `yaml:"interval"`
	FilterGood                     string          `yaml:"filter_good"`
	FilterValid                    string          `yaml:"filter_valid"`
	BurnrateAlerts                 []BurnrateAlert `yaml:"burnrate_alerts"`
}

type BurnrateAlert struct {
	Type        string  `yaml:"type"`
	Window      string  `yaml:"window"`
	WindowShort string  `yaml:"window_short"`
	Interval    string  `yaml:"interval"`
	Threshold   float32 `yaml:"threshold"`
	Message     string  `yaml:"message"`
	Severity    string  `yaml:"severity,omitempty"`
	Active      bool    `yaml:"active,omitempty"`
}

type RulesConfig struct {
	APIVersion string   `yaml:"apiVersion"`
	Kind       string   `yaml:"kind"`
	Metadata   Metadata `yaml:"metadata"`
	Spec       struct {
		Groups []RuleGroup `yaml:"groups"`
	} `yaml:"spec"`
}

type RuleGroup struct {
	Name     string `yaml:"name"`
	Interval string `yaml:"interval"`
	Rules    []Rule `yaml:"rules"`
}

type Rule struct {
	Alert       string            `yaml:"alert,omitempty"`
	Annotations map[string]string `yaml:"annotations,omitempty"`
	Expr        string            `yaml:"expr"`
	For         string            `yaml:"for,omitempty"`
	Labels      map[string]string `yaml:"labels,omitempty"`
	Record      string            `yaml:"record,omitempty"`
}
