// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"os"
	"path/filepath"
	"strings"
)

func walkFolderTreeForCUJsYamlFiles(basePath string) (CUJsYAMLFilePaths []string, err1 error) {
	err1 = filepath.Walk(basePath, func(path string, info os.FileInfo, err2 error) error {
		if err2 != nil {
			return err2
		}
		p := filepath.Clean(path) // prevent G304
		if !info.IsDir() && (strings.HasSuffix(p, "_cujs.yml") || strings.HasSuffix(p, "_cujs.yaml")) && !strings.Contains(p, ".git/") {
			CUJsYAMLFilePaths = append(CUJsYAMLFilePaths, p)
		}
		return nil
	})
	return CUJsYAMLFilePaths, err1
}
