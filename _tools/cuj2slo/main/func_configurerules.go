// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"strings"
)

func configureRules(team, user, goal, task *string, slo SLO, rulesConfig *RulesConfig, addExtTags bool, makePromAR bool) (err error) {
	var ruleGroup RuleGroup
	ruleGroup.Name = fmt.Sprintf("%s-%s", slo.ID, slo.Interval)
	ruleGroup.Interval = slo.Interval

	currentTimeDuration, err := getTimeRange(slo.FilterGood, "}[", "])")
	if err != nil {
		return fmt.Errorf("filter good error %v", err)
	}
	filterGood := strings.ReplaceAll(slo.FilterGood,
		fmt.Sprintf("[%s]", currentTimeDuration),
		fmt.Sprintf("[%s]", slo.Window))

	currentTimeDuration, err = getTimeRange(slo.FilterValid, "}[", "])")
	if err != nil {
		return fmt.Errorf("filter valid error %v", err)
	}
	filterValid := strings.ReplaceAll(slo.FilterValid,
		fmt.Sprintf("[%s]", currentTimeDuration),
		fmt.Sprintf("[%s]", slo.Window))

	var freezeWhenErrorBudgetExhausted string
	if slo.FreezeWhenErrorBudgetExhausted {
		freezeWhenErrorBudgetExhausted = "true"
	} else {
		freezeWhenErrorBudgetExhausted = "false"
	}

	extTags := map[string]string{
		"team": *team,
		"app":  slo.App,
		"user": *user,
		"goal": *goal,
		"task": *task,
		"type": slo.Type,
	}

	var rule Rule

	// count of good
	rule.Record = "slo:events:sum"
	rule.Labels = map[string]string{
		"id":                             slo.ID,
		"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
		"window":                         slo.Window,
		"status":                         "good",
	}
	if addExtTags {
		for k, v := range extTags {
			rule.Labels[k] = v
		}
	}
	rule.Expr = filterGood
	ruleGroup.Rules = append(ruleGroup.Rules, rule)

	// count of valid
	rule.Labels = map[string]string{
		"id":                             slo.ID,
		"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
		"window":                         slo.Window,
		"status":                         "valid",
	}
	if addExtTags {
		for k, v := range extTags {
			rule.Labels[k] = v
		}
	}
	rule.Expr = filterValid
	ruleGroup.Rules = append(ruleGroup.Rules, rule)

	// count of bad
	rule.Labels = map[string]string{
		"id":                             slo.ID,
		"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
		"window":                         slo.Window,
		"status":                         "bad",
	}
	if addExtTags {
		for k, v := range extTags {
			rule.Labels[k] = v
		}
	}
	rule.Expr = fmt.Sprintf("%s - %s", filterValid, filterGood)
	ruleGroup.Rules = append(ruleGroup.Rules, rule)

	// service level objective
	rule.Record = "slo:ratio:percent"
	rule.Labels = map[string]string{
		"id":                             slo.ID,
		"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
		"window":                         slo.Window,
		"ratio":                          "slo",
	}
	if addExtTags {
		for k, v := range extTags {
			rule.Labels[k] = v
		}
	}
	rule.Expr = fmt.Sprintf("vector(%f)", slo.Target)
	ruleGroup.Rules = append(ruleGroup.Rules, rule)

	// error budget objective
	rule.Labels = map[string]string{
		"id":                             slo.ID,
		"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
		"window":                         slo.Window,
		"ratio":                          "ebo",
	}
	if addExtTags {
		for k, v := range extTags {
			rule.Labels[k] = v
		}
	}
	rule.Expr = fmt.Sprintf("vector(100-%f)", slo.Target)
	ruleGroup.Rules = append(ruleGroup.Rules, rule)

	// service level indicator
	rule.Labels = map[string]string{
		"id":                             slo.ID,
		"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
		"window":                         slo.Window,
		"ratio":                          "sli",
	}
	if addExtTags {
		for k, v := range extTags {
			rule.Labels[k] = v
		}
	}
	rule.Expr = fmt.Sprintf("100 * %s / %s", filterGood, filterValid)
	ruleGroup.Rules = append(ruleGroup.Rules, rule)

	// error budget indicator
	rule.Labels = map[string]string{
		"id":                             slo.ID,
		"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
		"window":                         slo.Window,
		"ratio":                          "ebi",
	}
	if addExtTags {
		for k, v := range extTags {
			rule.Labels[k] = v
		}
	}
	rule.Expr = fmt.Sprintf("100 - (100 * %s / %s)", filterGood, filterValid)
	ruleGroup.Rules = append(ruleGroup.Rules, rule)

	// burn rate indicator
	rule.Record = "slo:ratio:fraction"
	rule.Labels = map[string]string{
		"id":                             slo.ID,
		"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
		"window":                         slo.Window,
		"ratio":                          "bri",
	}
	if addExtTags {
		for k, v := range extTags {
			rule.Labels[k] = v
		}
	}
	rule.Expr = fmt.Sprintf("(100 - (100 * %s / %s)) / (vector(100-%f))", filterGood, filterValid, slo.Target)
	ruleGroup.Rules = append(ruleGroup.Rules, rule)

	rulesConfig.Spec.Groups = append(rulesConfig.Spec.Groups, ruleGroup)

	for _, burnrateAlert := range slo.BurnrateAlerts {
		var ruleGroup RuleGroup
		ruleGroup.Name = fmt.Sprintf("%s-%s", slo.ID, burnrateAlert.Interval)
		ruleGroup.Interval = burnrateAlert.Interval

		alertFilterGood := strings.Replace(filterGood,
			fmt.Sprintf("[%s]", slo.Window),
			fmt.Sprintf("[%s]", burnrateAlert.Window),
			1)
		alertFilterValid := strings.Replace(filterValid,
			fmt.Sprintf("[%s]", slo.Window),
			fmt.Sprintf("[%s]", burnrateAlert.Window),
			1)

		// burn rate objective
		rule.Alert = ""
		rule.Record = "slo:ratio:fraction"
		rule.Labels = map[string]string{
			"id":                             slo.ID,
			"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
			"window":                         slo.Window,
			"ratio":                          "bro",
			"burnratetype":                   burnrateAlert.Type,
		}
		if addExtTags {
			for k, v := range extTags {
				rule.Labels[k] = v
			}
		}
		rule.Expr = fmt.Sprintf("vector(%f)", burnrateAlert.Threshold)
		rule.Annotations = make(map[string]string)
		ruleGroup.Rules = append(ruleGroup.Rules, rule)

		// burn rate indicator
		rule.Alert = ""
		rule.Record = "slo:ratio:fraction"
		rule.Labels = map[string]string{
			"id":                             slo.ID,
			"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
			"window":                         burnrateAlert.Window,
			"ratio":                          "bri",
			"burnratetype":                   burnrateAlert.Type,
		}
		if addExtTags {
			for k, v := range extTags {
				rule.Labels[k] = v
			}
		}
		rule.Expr = fmt.Sprintf("(100 - (100 * %s / %s)) / (vector(100-%f))", alertFilterGood, alertFilterValid, slo.Target)
		rule.Annotations = make(map[string]string)
		ruleGroup.Rules = append(ruleGroup.Rules, rule)

		// burn rate indicator short window
		var windowShort string
		if burnrateAlert.WindowShort != "" {
			windowShort = burnrateAlert.WindowShort
		} else {
			windowShort, err = computeShortWindowDuration(burnrateAlert.Window)
			if err != nil {
				return err
			}
		}

		var alertShortFilterGood, alertShortFilterValid string
		if windowShort != "" {
			alertShortFilterGood = strings.Replace(filterGood,
				fmt.Sprintf("[%s]", slo.Window),
				fmt.Sprintf("[%s]", windowShort),
				1)
			alertShortFilterValid = strings.Replace(filterValid,
				fmt.Sprintf("[%s]", slo.Window),
				fmt.Sprintf("[%s]", windowShort),
				1)
			rule.Alert = ""
			rule.Record = "slo:ratio:fraction"
			rule.Labels = map[string]string{
				"id":                             slo.ID,
				"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
				"window":                         windowShort,
				"ratio":                          "bri",
				"burnratetype":                   burnrateAlert.Type,
			}
			if addExtTags {
				for k, v := range extTags {
					rule.Labels[k] = v
				}
			}
			rule.Expr = fmt.Sprintf("(100 - (100 * %s / %s)) / (vector(100-%f))", alertShortFilterGood, alertShortFilterValid, slo.Target)
			rule.Annotations = make(map[string]string)
			ruleGroup.Rules = append(ruleGroup.Rules, rule)
		}

		// burn rate alert
		if makePromAR && burnrateAlert.Active {
			rule.Alert = fmt.Sprintf("%s-%s", slo.ID, burnrateAlert.Type)
			rule.Record = ""
			severity := burnrateAlert.Severity
			if severity == "" {
				severity = defaultAlertSeverity
			}
			rule.Labels = map[string]string{
				"window":                         burnrateAlert.Window,
				"freezeWhenErrorBudgetExhausted": freezeWhenErrorBudgetExhausted,
				"burnratetype":                   burnrateAlert.Type,
				"severity":                       severity,
			}
			if addExtTags {
				for k, v := range extTags {
					rule.Labels[k] = v
				}
			}
			if windowShort != "" {
				rule.Expr = fmt.Sprintf("(((100 - (100 * %s / %s)) / (vector(100-%f)) > vector(%f)) and ((100 - (100 * %s / %s)) / (vector(100-%f)) > vector(%f)))",
					alertFilterGood, alertFilterValid, slo.Target, burnrateAlert.Threshold,
					alertShortFilterGood, alertShortFilterValid, slo.Target, burnrateAlert.Threshold)
			} else {
				rule.Expr = fmt.Sprintf("(100 - (100 * %s / %s)) / (vector(100-%f)) > vector(%f)", alertFilterGood, alertFilterValid, slo.Target, burnrateAlert.Threshold)
			}
			var t string
			if burnrateAlert.Threshold == float32(int(burnrateAlert.Threshold)) {
				t = fmt.Sprintf("%d", int(burnrateAlert.Threshold))
			} else {
				t = fmt.Sprintf("%f", burnrateAlert.Threshold)
			}
			var s string
			if addExtTags {
				s = fmt.Sprintf("%s %s %s %s during %s the error budget burn rate was > %s times the target for SLO %s", *team, slo.App, *task, slo.Type, burnrateAlert.Window, t, slo.ID)
			} else {
				s = fmt.Sprintf("During %s the error budget burn rate was > %s times the target for SLO %s", burnrateAlert.Window, t, slo.ID)
			}
			var d string
			if burnrateAlert.Message != "" {
				d = burnrateAlert.Message
			} else {
				if addExtTags {
					d = fmt.Sprintf("Team: %s App: %s User: %s Goal: %s Task: %s Type: %s", *team, slo.App, *user, *goal, *task, slo.Type)
				}
			}
			rule.Annotations = map[string]string{
				"summary":     s,
				"description": d,
			}
			ruleGroup.Rules = append(ruleGroup.Rules, rule)
		}
		rulesConfig.Spec.Groups = append(rulesConfig.Spec.Groups, ruleGroup)
	}

	return nil
}
