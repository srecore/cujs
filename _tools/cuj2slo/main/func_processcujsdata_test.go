// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestProcessCUJsData(t *testing.T) {
	tests := []struct {
		name              string
		CUJs              []CriticalUserJourney
		team              *string
		errorBudgetPolicy *string
		wantToc           string
		wantBody          string
	}{
		{
			name:              "empty CUJs",
			CUJs:              []CriticalUserJourney{},
			team:              nil,
			errorBudgetPolicy: nil,
			wantToc:           "",
			wantBody:          "",
		},
		{
			name: "one user",
			CUJs: []CriticalUserJourney{
				{User: "user1"},
			},
			team:              nil,
			errorBudgetPolicy: nil,
			wantToc:           "1. user1\n",
			wantBody:          "",
		},
		{
			name: "two users",
			CUJs: []CriticalUserJourney{
				{User: "user1"},
				{User: "user2"},
			},
			team:              nil,
			errorBudgetPolicy: nil,
			wantToc:           "1. user1\n2. user2\n",
			wantBody:          "",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var tocContent, bodyContent string
			var userNumber, goalCount, taskCount, sloCount int
			var rulesConfig RulesConfig
			var IDs string
			processCUJsData(sloBaseURL,
				test.CUJs,
				test.team,
				test.errorBudgetPolicy, &tocContent, &bodyContent,
				&userNumber, &goalCount, &taskCount, &sloCount, &rulesConfig, false, false, &IDs)
			if diff := cmp.Diff(test.wantToc, tocContent); diff != "" {
				t.Errorf("tocContent mismatch (-want +got):\n%s", diff)
			}
			if diff := cmp.Diff(test.wantBody, bodyContent); diff != "" {
				t.Errorf("bodyContent mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
