// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"
)

func TestProcessFiles(t *testing.T) {
	tests := []struct {
		name         string
		basePath     string
		wantErr      bool
		wantMessages []string
	}{
		{
			name:         "not existing folder",
			basePath:     "testdata/empty",
			wantErr:      true,
			wantMessages: []string{""},
		},
		{
			name: "valid folder tree ",
			// basePath:    "testdata/testwalk1",
			basePath: ".",
			wantErr:  false,
			wantMessages: []string{
				"testdata/testwalk1/bu1/d11/t111/README.md done",
				"testdata/testwalk1/bu1/d11/t112/README.md done",
				"testdata/testwalk1/bu2/d21/t211/README.md done",
				"testdata/testwalk1/bu2/d21/t212/README.md done",
			},
		},
	}
	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err := processFiles(test.basePath, false, false, false)
			msgString := buffer.String()
			// t.Logf("\n%v", msgString)
			if (err != nil) != test.wantErr {
				t.Errorf("processFiles() error = %v, wantErr %v", err, test.wantErr)
			} else {
				for _, wantMsg := range test.wantMessages {
					if !strings.Contains(msgString, wantMsg) {
						t.Errorf("processFiles() = %v, want %v", msgString, wantMsg)
					}
				}
			}
		})
	}
}
