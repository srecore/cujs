// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWalkFolderTreeForCUJsYamlFiles(t *testing.T) {
	tests := []struct {
		name                      string
		basePath                  string
		wantErr                   bool
		expectedCUJsYAMLFilePaths []string
	}{
		{
			name:                      "regular",
			basePath:                  "./testdata/testwalk1",
			wantErr:                   false,
			expectedCUJsYAMLFilePaths: []string{"testdata/testwalk1/bu1/d11/t111/bu1_d11_t111_cujs.yml", "testdata/testwalk1/bu1/d11/t112/bu1_d11_t112_cujs.yml", "testdata/testwalk1/bu2/d21/t211/bu2_d21_t211_cujs.yml", "testdata/testwalk1/bu2/d21/t212/bu2_d21_t212_cujs.yml"},
		},
		{
			name:                      "no found files",
			basePath:                  "./testdata/testwalk2",
			wantErr:                   false,
			expectedCUJsYAMLFilePaths: []string(nil),
		},
		{
			name:     "invalid path",
			basePath: "./blabla",
			wantErr:  true,
		},
	}

	for _, test := range tests {
		test := test
		CUJsYAMLFilePaths, err := walkFolderTreeForCUJsYamlFiles(test.basePath)
		if test.wantErr {
			assert.NotNil(t, err)
		} else {
			assert.Nil(t, err)
			assert.Equal(t, test.expectedCUJsYAMLFilePaths, CUJsYAMLFilePaths)
		}
	}
}
