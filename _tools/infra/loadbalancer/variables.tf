/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "GCP project id "
  type        = string
}

variable "region" {
  description = "gcp region"
  type        = string
}

variable "region_north" {
  description = "gcp region for production north"
  type        = string
}

variable "region_south" {
  description = "gcp region for production south"
  type        = string
}

variable "dns_name" {
  description = "The DNS name used to expose e.g. myapp.example.com"
  type        = string
}

variable "directory_customer_id" {
  description = "Cloud Identity directory customer id"
  type        = string 
}

variable "directory_dns_name" {
  description = "Cloud Identity directory dns name"
  type        = string 
}

variable "static_public_bucket_name_suffix" {
  description = "suffix to the bucket name hosting public static content"
  default     = "-staticpublicweb"
}

variable "static_site_codelab_sre_core_gcm_bucket_name_suffix" {
  description = "suffix to the bucket name hosting public static content"
  default     = "-codelab-srecoregcm"
}