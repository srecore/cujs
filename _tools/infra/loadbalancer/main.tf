/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

data "google_project" "project" {
  project_id = var.project_id
}

data "google_secret_manager_secret_version_access" "sre_iap_client_id" {
  project = var.project_id
  secret  = "sre-iap-client-id"
}

data "google_secret_manager_secret_version_access" "sre_iap_client_secret" {
  project = var.project_id
  secret  = "sre-iap-client-secret"
}

resource "google_compute_security_policy" "cloud_armor_edge" {
  project = var.project_id
  name    = "cloud-armor-edge"
  type    = "CLOUD_ARMOR_EDGE"
}

resource "google_compute_security_policy" "cloud_armor" {
  project = var.project_id
  name    = "cloud-armor"
  type    = "CLOUD_ARMOR"
}

# Default backend bucket
resource "google_storage_bucket" "static_public_web" {
  project       = var.project_id
  name          = "${var.project_id}${var.static_public_bucket_name_suffix}"
  location      = var.region
  storage_class = "COLDLINE"
  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}

resource "google_storage_bucket_object" "indexpage" {
  name         = "index.html"
  content      = "<html><body>Welcome to Site Reliability Engineering, use <a href=\"https://${var.dns_name}/dashboards\">/dashboards</a> to get the Critical User Journey Error budgets</body></html>"
  content_type = "text/html"
  bucket       = google_storage_bucket.static_public_web.id
}

resource "google_storage_bucket_object" "errorpage" {
  name         = "404.html"
  content      = "<html><body>404! Woops, the page your are trying to reach does not exist or you do not have access</body></html>"
  content_type = "text/html"
  bucket       = google_storage_bucket.static_public_web.id
}

resource "google_storage_bucket_object" "favicon" {
  name   = "favicon.ico"
  source = "${path.module}/favicon.ico"
  bucket = google_storage_bucket.static_public_web.id
}

resource "google_compute_backend_bucket" "static_public_content" {
  project              = var.project_id
  name                 = "static-public"
  description          = "Static public content, at least 404 not found page"
  bucket_name          = google_storage_bucket.static_public_web.name
  edge_security_policy = google_compute_security_policy.cloud_armor_edge.id
  enable_cdn           = false
}

resource "google_storage_bucket_iam_member" "public_rule" {
  bucket = google_storage_bucket.static_public_web.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}

# Codelab backend buckets development environment
resource "google_storage_bucket" "static_site_codelab_sre_core_gcm_dev" {
  project       = var.project_id
  name          = "${var.project_id}${var.static_site_codelab_sre_core_gcm_bucket_name_suffix}-dev"
  location      = var.region
  storage_class = "STANDARD"
  website {
    main_page_suffix = "index.html"
  }
}

resource "google_storage_bucket_iam_member" "codelab_sre_core_gcm_obj_viewer_dev" {
  bucket = google_storage_bucket.static_site_codelab_sre_core_gcm_dev.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}

resource "google_compute_backend_bucket" "codelab_sre_core_gcm_dev" {
  project              = var.project_id
  name                 = "codelab-sre-core-gcm-dev"
  description          = "Training codelab SRE core development environment"
  bucket_name          = google_storage_bucket.static_site_codelab_sre_core_gcm_dev.name
  edge_security_policy = google_compute_security_policy.cloud_armor_edge.id
  enable_cdn           = false
}

# Codelab backend buckets test environment
resource "google_storage_bucket" "static_site_codelab_sre_core_gcm_tst" {
  project       = var.project_id
  name          = "${var.project_id}${var.static_site_codelab_sre_core_gcm_bucket_name_suffix}-tst"
  location      = var.region
  storage_class = "STANDARD"
  website {
    main_page_suffix = "index.html"
  }
}

resource "google_storage_bucket_iam_member" "codelab_sre_core_gcm_obj_viewer_tst" {
  bucket = google_storage_bucket.static_site_codelab_sre_core_gcm_tst.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}

resource "google_compute_backend_bucket" "codelab_sre_core_gcm_tst" {
  project              = var.project_id
  name                 = "codelab-sre-core-gcm-tst"
  description          = "Training codelab SRE core test environment"
  bucket_name          = google_storage_bucket.static_site_codelab_sre_core_gcm_tst.name
  edge_security_policy = google_compute_security_policy.cloud_armor_edge.id
  enable_cdn           = false
}

# Codelab backend buckets production environment
resource "google_storage_bucket" "static_site_codelab_sre_core_gcm" {
  project       = var.project_id
  name          = "${var.project_id}${var.static_site_codelab_sre_core_gcm_bucket_name_suffix}"
  location      = var.region
  storage_class = "STANDARD"
  website {
    main_page_suffix = "index.html"
  }
}

resource "google_storage_bucket_iam_member" "codelab_sre_core_gcm_obj_viewer" {
  bucket = google_storage_bucket.static_site_codelab_sre_core_gcm.name
  role   = "roles/storage.objectViewer"
  member = "allUsers"
}

resource "google_compute_backend_bucket" "codelab_sre_core_gcm" {
  project              = var.project_id
  name                 = "codelab-sre-core-gcm"
  description          = "Training codelab SRE core production environment"
  bucket_name          = google_storage_bucket.static_site_codelab_sre_core_gcm.name
  edge_security_policy = google_compute_security_policy.cloud_armor_edge.id
  enable_cdn           = false
}

# Cloud Run Serverless endpoints
resource "google_compute_region_network_endpoint_group" "neg_region_default" {
  project               = var.project_id
  name                  = "neg-region-default"
  network_endpoint_type = "SERVERLESS"
  region                = var.region
  cloud_run {
    url_mask = "/<service>"
  }
}

resource "google_compute_region_network_endpoint_group" "neg_region_north" {
  project               = var.project_id
  name                  = "neg-region-north"
  network_endpoint_type = "SERVERLESS"
  region                = var.region_north
  cloud_run {
    url_mask = "/<service>"
  }
}

resource "google_compute_region_network_endpoint_group" "neg_region_south" {
  project               = var.project_id
  name                  = "neg-region-south"
  network_endpoint_type = "SERVERLESS"
  region                = var.region_south
  cloud_run {
    url_mask = "/<service>"
  }
}

resource "google_compute_backend_service" "dashboards" {
  project = var.project_id
  name    = "dashboards"
  log_config {
    enable      = true
    sample_rate = 1.0
  }
  backend {
    group = google_compute_region_network_endpoint_group.neg_region_default.id
  }
  iap {
    enabled = true
    oauth2_client_id     = data.google_secret_manager_secret_version_access.sre_iap_client_id.secret_data
    oauth2_client_secret = data.google_secret_manager_secret_version_access.sre_iap_client_secret.secret_data
  }
  security_policy = google_compute_security_policy.cloud_armor.id
}

resource "google_compute_backend_service" "codelab-dev" {
  project = var.project_id
  name    = "codelab-dev"
  log_config {
    enable      = true
    sample_rate = 1.0
  }
  backend {
    group = google_compute_region_network_endpoint_group.neg_region_default.id
  }
  iap {
    enabled = true
    oauth2_client_id     = data.google_secret_manager_secret_version_access.sre_iap_client_id.secret_data
    oauth2_client_secret = data.google_secret_manager_secret_version_access.sre_iap_client_secret.secret_data
  }
  security_policy = google_compute_security_policy.cloud_armor.id
}

resource "google_compute_backend_service" "codelab" {
  project = var.project_id
  name    = "codelab"
  log_config {
    enable      = true
    sample_rate = 1.0
  }
  backend {
    group = google_compute_region_network_endpoint_group.neg_region_north.id
  }
  backend {
    group = google_compute_region_network_endpoint_group.neg_region_south.id
  }
  iap {
    enabled = true
    oauth2_client_id     = data.google_secret_manager_secret_version_access.sre_iap_client_id.secret_data
    oauth2_client_secret = data.google_secret_manager_secret_version_access.sre_iap_client_secret.secret_data
  }
  security_policy = google_compute_security_policy.cloud_armor.id
}

resource "google_compute_backend_service" "reports" {
  project = var.project_id
  name    = "reports"
  log_config {
    enable      = true
    sample_rate = 1.0
  }
  backend {
    group = google_compute_region_network_endpoint_group.neg_region_default.id
  }
  iap {
    enabled = true
  }
  security_policy = google_compute_security_policy.cloud_armor.id
  lifecycle {
    ignore_changes = [ 
      iap[0].oauth2_client_id
     ]
  }
}

resource "google_compute_backend_service" "authtest" {
  project = var.project_id
  name    = "authtest"
  log_config {
    enable      = true
    sample_rate = 1.0
  }
  backend {
    group = google_compute_region_network_endpoint_group.neg_region_default.id
  }
  iap {
    enabled = true
  }
  security_policy = google_compute_security_policy.cloud_armor.id
  lifecycle {
    ignore_changes = [ 
      iap[0].oauth2_client_id
     ]
  }
}

resource "google_compute_url_map" "sre_urlmap" {
  project         = var.project_id
  name            = "sre-urlmap"
  description     = "SRE URL map"
  default_service = google_compute_backend_bucket.static_public_content.id
  host_rule {
    hosts        = [var.dns_name]
    path_matcher = "sre"
  }
  path_matcher {
    name            = "sre"
    default_service = google_compute_backend_bucket.static_public_content.id
    path_rule {
      paths   = ["/dashboards", "/dashboards/*"]
      service = google_compute_backend_service.dashboards.id
    }
    path_rule {
      paths   = ["/reports", "/reports/*"]
      service = google_compute_backend_service.reports.id
    }
    path_rule {
      paths   = ["/authtest", "/authtest/*"]
      service = google_compute_backend_service.authtest.id
    }
    path_rule {
      paths   = ["/lab-dev", "/lab-dev/*"]
      service = google_compute_backend_bucket.codelab_sre_core_gcm_dev.id
    }
    path_rule {
      paths   = ["/lab-test", "/lab-test/*"]
      service = google_compute_backend_bucket.codelab_sre_core_gcm_tst.id
    }
    path_rule {
      paths   = ["/lab", "/lab/*"]
      service = google_compute_backend_bucket.codelab_sre_core_gcm.id
    }
    path_rule {
      paths   = ["/codelab-dev/*"]
      service = google_compute_backend_service.codelab-dev.id
    }
    path_rule {
      paths   = ["/codelab/*"]
      service = google_compute_backend_service.codelab.id
    }
  }
}

resource "google_compute_managed_ssl_certificate" "sre_ssl_certif" {
  project = var.project_id
  name    = "sre-ssl-certif"
  managed {
    domains = ["${var.dns_name}"]
  }
}

resource "google_compute_global_address" "sre_ext_ip" {
  project = var.project_id
  name    = "sre-ext-ip"
}

resource "google_compute_target_https_proxy" "sre_https_proxy" {
  project = var.project_id
  name    = "sre-https-proxy"
  url_map = google_compute_url_map.sre_urlmap.id
  ssl_certificates = [
    google_compute_managed_ssl_certificate.sre_ssl_certif.id
  ]
}

resource "google_compute_global_forwarding_rule" "sre_fwd_rule" {
  project    = var.project_id
  name       = "sre-fwd-rule"
  ip_address = google_compute_global_address.sre_ext_ip.address
  target     = google_compute_target_https_proxy.sre_https_proxy.id
  port_range = "443"
}

# resource "google_cloud_identity_group" "group_codelab_sre_core_gcm" {
#   display_name         = "codelab-sre-core-gcm"
#   initial_group_config = "WITH_INITIAL_OWNER"

#   parent = "customers/${var.directory_customer_id}"

#   group_key {
#       id = "codelab-sre-core-gcm@${var.directory_dns_name}"
#   }

#   labels = {
#     "cloudidentity.googleapis.com/groups.discussion_forum" = ""
#   }
# }