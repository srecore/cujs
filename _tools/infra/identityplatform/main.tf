/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

data "google_secret_manager_secret_version_access" "sre_idp_google_client_id" {
  project = var.project_id
  secret  = "sre-idp-google-client-id"
}

data "google_secret_manager_secret_version_access" "sre_idp_google_client_secret" {
  project = var.project_id
  secret  = "sre-idp-google-client-secret"
}

resource "google_storage_bucket" "user_app_role" {
  project  = var.project_id
  name     = "${var.project_id}-user-app-role"
  location = var.region
}

resource "google_storage_bucket_object" "user_app_role_default" {
  name   = "user_app_role.yml"
  source = "${path.module}/user_app_role.yml"
  bucket = google_storage_bucket.user_app_role.name
  lifecycle {
    ignore_changes = all
  }
}

resource "google_storage_bucket" "func_code" {
  project  = var.project_id
  name     = "${var.project_id}-func-code"
  location = var.region
}

data "archive_file" "before_sign_in_zip" {
  type        = "zip"
  output_path = "before_sign_in.zip"
  source_dir  = "./identityplatform/blockingfunc/beforesignin"
}

resource "google_storage_bucket_object" "before_sign_in_archive" {
  name   = "before_sign_in_${data.archive_file.before_sign_in_zip.output_sha256}.zip"
  bucket = google_storage_bucket.func_code.name
  source = data.archive_file.before_sign_in_zip.output_path
}

resource "google_service_account" "before_sign_in_sa" {
  project      = var.project_id
  account_id   = "beforesignin"
  display_name = "IDP before sign-in"
  description  = "Use by the before sign-in blocking function of Cloud Identity Platform"
}

resource "google_project_iam_member" "before_sign_in_project_logging_logwriter" {
  project = var.project_id
  role    = "roles/logging.logWriter"
  member = "serviceAccount:${google_service_account.before_sign_in_sa.email}"
}

resource "google_storage_bucket_iam_member" "before_sign_in_user_app_role_repo_reader" {
  bucket = google_storage_bucket.user_app_role.name
  role   = "roles/storage.objectViewer"
  member = "serviceAccount:${google_service_account.before_sign_in_sa.email}"
}

# Identity platform before sign in function MUST be 1st generation
resource "google_cloudfunctions_function" "before_sign_in" {
  project               = var.project_id
  region                = var.region
  name                  = "beforeSignIn"
  description           = "Before sign-in blocking function for Cloud Identity Platform"
  runtime               = "nodejs20"
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.func_code.name
  source_archive_object = google_storage_bucket_object.before_sign_in_archive.name
  trigger_http          = true
  entry_point           = "beforeSignIn"
  service_account_email = google_service_account.before_sign_in_sa.email
  timeout               = 7 # blocking function must respond in less than 7 seconds
  environment_variables = {
    USER_APP_ROLE_BUCKET_NAME = google_storage_bucket.user_app_role.name
    USER_APP_ROLE_FILE_NAME = "user_app_role.yml"
  }
}

resource "google_cloudfunctions_function_iam_member" "before_sign_in_invoker" {
  project        = google_cloudfunctions_function.before_sign_in.project
  region         = google_cloudfunctions_function.before_sign_in.region
  cloud_function = google_cloudfunctions_function.before_sign_in.name
  role           = "roles/cloudfunctions.invoker"
  member         = "allUsers"
}

data "archive_file" "before_create_zip" {
  type        = "zip"
  output_path = "before_create.zip"
  source_dir  = "./identityplatform/blockingfunc/beforecreate"
}

resource "google_storage_bucket_object" "before_create_archive" {
  name   = "before_create_${data.archive_file.before_create_zip.output_sha256}.zip"
  bucket = google_storage_bucket.func_code.name
  source = data.archive_file.before_create_zip.output_path
}

resource "google_service_account" "before_create_sa" {
  project      = var.project_id
  account_id   = "beforecreate"
  display_name = "IDP before create"
  description  = "Use by the before create blocking function of Cloud Identity Platform"
}

resource "google_project_iam_member" "before_create_project_logging_logwriter" {
  project = var.project_id
  role    = "roles/logging.logWriter"
  member = "serviceAccount:${google_service_account.before_create_sa.email}"
}

resource "google_storage_bucket_iam_member" "before_create_user_app_role_repo_reader" {
  bucket = google_storage_bucket.user_app_role.name
  role   = "roles/storage.objectViewer"
  member = "serviceAccount:${google_service_account.before_create_sa.email}"
}

# Identity platform before sign in function MUST be 1st generation
resource "google_cloudfunctions_function" "before_create" {
  project               = var.project_id
  region                = var.region
  name                  = "beforeCreate"
  description           = "Before create blocking function for Cloud Identity Platform"
  runtime               = "nodejs20"
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.func_code.name
  source_archive_object = google_storage_bucket_object.before_create_archive.name
  trigger_http          = true
  entry_point           = "beforeCreate"
  service_account_email = google_service_account.before_create_sa.email
  timeout               = 7 # blocking function must respond in less than 7 seconds
  environment_variables = {
    USER_APP_ROLE_BUCKET_NAME = google_storage_bucket.user_app_role.name
    USER_APP_ROLE_FILE_NAME = "user_app_role.yml"
  }
}

resource "google_cloudfunctions_function_iam_member" "before_create_invoker" {
  project        = google_cloudfunctions_function.before_create.project
  region         = google_cloudfunctions_function.before_create.region
  cloud_function = google_cloudfunctions_function.before_create.name
  role           = "roles/cloudfunctions.invoker"
  member         = "allUsers"
}

resource "google_identity_platform_config" "default" {
  project = var.project_id
  sign_in {
    allow_duplicate_emails = false
  }
  client {
    permissions {
      disabled_user_signup   = false
      disabled_user_deletion = false
    }
  }
  blocking_functions {
    triggers {
      event_type   = "beforeCreate"
      function_uri = google_cloudfunctions_function.before_create.https_trigger_url
    }
    triggers {
      event_type   = "beforeSignIn"
      function_uri = google_cloudfunctions_function.before_sign_in.https_trigger_url
    }
    # forward_inbound_credentials {
    #   refresh_token = true
    #   access_token = true
    #   id_token = true
    # }
  }
  monitoring {
    request_logging {
      enabled = true
    }
  }
}

resource "google_identity_platform_default_supported_idp_config" "google" {
  project       = var.project_id
  enabled       = true
  idp_id        = "google.com"
  client_id     = data.google_secret_manager_secret_version_access.sre_idp_google_client_id.secret_data
  client_secret = data.google_secret_manager_secret_version_access.sre_idp_google_client_secret.secret_data

}
