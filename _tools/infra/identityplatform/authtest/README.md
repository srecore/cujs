# Auth test app

## cRun

```shell
gcloud run deploy authtest --source=. --region=europe-west1 --project=$PROJECT_ID
```

## GAE

```shell
sudo apt-get install google-cloud-cli-app-engine-go
gcloud app deploy --project=$PROJECT_ID
```

- Console / IAP / select the GAE app / enable
- Activates external identities / create cloud run auth page
