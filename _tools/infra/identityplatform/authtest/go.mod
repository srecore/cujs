module eeexample.com/authtest

go 1.22.0

toolchain go1.23.1

require (
	github.com/golang-jwt/jwt/v5 v5.2.1
	golang.org/x/exp v0.0.0-20241009180824-f66d83c29e7c
)
