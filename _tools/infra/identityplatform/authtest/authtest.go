// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/exp/slog"
)

func main() {
	logger := setStructuredLogger()
	slog.SetDefault(logger)
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/ready", readinessHandler)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		slog.Info("using default port", "description", fmt.Sprintf("Defaulting to port %s", port))
	}

	slog.Info("listening on port", "description", fmt.Sprintf("listening on port %s", port))

	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Fatal(err)
	}
}

func readinessHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	iapJWT := r.Header.Get("x-goog-iap-jwt-assertion")
	if iapJWT == "" {
		http.Error(w, "Unauthorized x-goog-iap-jwt-assertion header is missing", http.StatusUnauthorized)
		return
	}
	slog.Info("jwt", "description", iapJWT)

	token, _, err := new(jwt.Parser).ParseUnverified(iapJWT, jwt.MapClaims{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		b, err := json.MarshalIndent(claims, "", "  ")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		slog.Info("jwt payload", "description", string(b))
		fmt.Fprintf(w, "JWT payload\n%s", string(b))
	} else {
		http.Error(w, "Error accessing claims", http.StatusInternalServerError)
	}

	// var payload *idtoken.Payload
	// payload, err = idtoken.Validate(context.Background(), iapJWT, "")
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusUnauthorized)
	// 	return
	// }
	// b, err := json.MarshalIndent(payload, "", "  ")
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusInternalServerError)
	// 	return
	// }
	// slog.Info("jwt payload", "description", string(b))
	// fmt.Fprintf(w, "JWT payload\n%s", string(b))
}

func setStructuredLogger() (logger *slog.Logger) {
	// adapt log entry format and content to structured logs expected by GCP
	// https://cloud.google.com/logging/docs/reference/v2/rest/v2/LogEntry
	jsonHandler := slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		AddSource: false,
		Level:     slog.LevelInfo,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.MessageKey {
				return slog.String("message", a.Value.String())
			}
			if a.Key == slog.LevelKey {
				if a.Value.String() == "WARN" {
					return slog.String("severity", "WARNING")
				}
				return slog.String("severity", a.Value.String())
			}
			if a.Key == slog.TimeKey {
				return slog.String("timestamp", a.Value.Time().Format(time.RFC3339))
			}
			return a
		},
	})
	return slog.New(jsonHandler)
}
