/*!
 * Copyright 2024 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as gcipCloudFunctions from 'gcip-cloud-functions';
import { Storage } from '@google-cloud/storage';
import * as yaml from 'js-yaml';
import bunyan from 'bunyan';
import { LoggingBunyan } from '@google-cloud/logging-bunyan';

const authClient = new gcipCloudFunctions.Auth();
const userAppRoleBucketName = process.env.USER_APP_ROLE_BUCKET_NAME;
const userAppRoleFileName = process.env.USER_APP_ROLE_FILE_NAME;
const storage = new Storage();
const loggingBunyan = new LoggingBunyan();

const logger = bunyan.createLogger({
  name: 'beforeSignInFunction',
  streams: [
    loggingBunyan.stream(),
    {
      stream: process.stdout,
      level: 'debug',
    },
  ],
});

async function getUserAppRoleData() {
  const bucket = storage.bucket(userAppRoleBucketName);
  const file = bucket.file(userAppRoleFileName);

  try {
    const [contents] = await file.download();
    const userAppRoleData = yaml.load(contents.toString());
    return userAppRoleData
  } catch (error) {
    logger.error({
      message: 'unable to get or parse user app role data',
      severity: 'ERROR',
      labels: {
        userAppRoleBucketName: userAppRoleBucketName,
        userAppRoleFileName: userAppRoleBucketName
      },
    }, '');
    throw new gcipCloudFunctions.https.HttpsError(
      'permission-denied',
      'unable to get or parse user app role data');
  }
}

export const beforeSignIn = authClient.functions().beforeSignInHandler(async (user, context) => {
  const userAppRoleData = await getUserAppRoleData();

  if (!userAppRoleData[user.email]) {
    logger.info({
      message: 'user not found authorized user list',
      severity: 'INFO',
      labels: {
        user: user.email,
      },
    }, '');
    throw new gcipCloudFunctions.https.HttpsError(
      'permission-denied',
      'User not authorized to access this application.'
    );
  } else {
    const sessionClaims = {};
    Object.keys(userAppRoleData[user.email]).forEach(key => {
      sessionClaims[key] = userAppRoleData[user.email][key];
    });
    return {
      sessionClaims: sessionClaims,
    };
  }
});