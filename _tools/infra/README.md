# SRE tools infra

__WARNING__ there are manual steps to perform before deploying resources using this terraform module.

Review the documentation [Google Managed Prometheus Query using Grafana](https://cloud.google.com/stackdriver/docs/managed-prometheus/query), and specifically the sections:

- [Grafana](https://cloud.google.com/stackdriver/docs/managed-prometheus/query#ui-grafana)
- [Configure and authenticate the Grafana data source](https://cloud.google.com/stackdriver/docs/managed-prometheus/query#grafana-oauth)

This terraform module implement the resources described in the above documentation page as:

- The Grafana app, hosted in Cloud Run with a MySQL database in Cloud SQL
- Exposed with an external Load balancer under the path `/dashboards` using Identity Aware Proxy IAP to authenticated the users and secure the access to the Grafana app
- Implementing the Google Managed Prometheus GMP data source syncer so that Grafana can query GMP, as GMP requires OAuth and current Grafana does not support it. The syncer is deployed as a Cloud Run Job.

## IAP brand and IAP client for the Load balancer

Type: GEHTTPSLBC Global External HTTPS Load Balancer Classic with IAP. IAP uses the an [IAP Brand](https://cloud.google.com/iap/docs/reference/rest/v1/projects.brands).

IAP Brand [can be created programmatically](https://cloud.google.com/iap/docs/programmatic-oauth-clients#known_limitations) even using the Terraform resource [google_iap_brand](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/iap_brand.html).

But, there is only [ONE IAP brand per project](https://cloud.google.com/config-connector/docs/reference/resource-docs/iap/iapbrand), meaning

1. it cannot be deleted
2. If it already exist there is not point to add it to this terraform module.

So, do the following actions:

- Create the IAP service account using `gcloud beta services identity create --service=iap.googleapis.com --project=[PROJECT_ID]`
- Create or identify existing IAP Brand, on the project hosting this Load balancer
  - For example using GCP console / API / OAuth.
  - Can be named 'Site Reliability Engineering'
  - The account used to create the IAP brand must be owner of the group mentioned in the filed support_email
- Create and IAP client on that Brand.
  - For example using GCP console / API / Credentials / Create OAuth client ID / Web App
  - Can be named 'sre-iap-client'
- Store the IAP id and secret provided by the console at the previous step in two secrets secret manager as:
  - `sre-iap-client-id`
  - `sre-iap-client-secret`
  - Use exactly these names as they are referenced as is in the terraform module to set IAP on the two backend services.
- Bind the following IAM policy entry:
  1. Role 'Secret Manager Secret Accessor' `roles/secretmanager.secretAccessor`
  2. to the service account used to deploy the terraform module
  3. on the two above secrets.
- Add the `A` record to your DNS system to map the dns name you chose like `sre.yourdnsdomain.com` to the public IP address provisioned with the loadbalancer. It can be found in terraform output or in the console.
- At the first connection to `sre.yourdnsdomain.com/dashboards` IAP will through an error 400 [redirect_uri_mismatch](https://developers.google.com/identity/protocols/oauth2/web-server#authorization-errors-redirect-uri-mismatch), click on error detail and copy the redirection URL to be added to console / credentials / your client ID web app for IAP / Authorized redirect URIs / add URI
- Once the load balancer provisioned, go to console / IAP / check the box in front of the dashboard backend service to display the Info panel with the granted permission. Add the adhoc group with role `IAP-secure Web App User`
- By default the OAuth Consent screen defines the `User type` as internal, meaning only user part of the same Cloud Identity directory (aka Google Workspace) can login. Click `Make external / In production` to open it to other organizations. The `Publication status` is then `In Production`. Of course, it is still required to grant to the users from other organizations the IAM permission `IAP-secured Web App User`

## Grafana and Grafana data source sync

The following manual steps must be performed:

- Store the Cloud SQL MySQL grafana database passwords as a secrets in secret Manager as:
  - `grafana-mysql-root-password`
  - `grafana-mysql-grafana-user-password`
- Bind the following IAM policy entries:
  1. Role 'Secret Manager Secret Accessor' `roles/secretmanager.secretAccessor`
  2. to the service account used to deploy the terraform module
  3. on the above secrets.
- On first login to grafana use `username:password admin:admin` and update to strong password as proposed by grafana
- In grafana: connections / data source / add data source / prometheus
  - Give the data source a name, set the URL field to `http://localhost:9090`, then select Save & Test. You can ignore any errors saying that the data source is not configured correctly.
  - Set the terraform variable `data_source_uids` with this above data source uid that is the last chunk of the URL. Example: `https://sre.yourdnsdomain.com/dashboards/connections/datasources/edit/qwzr86g00rawwd`, here the uid is `qwzr86g00rawwd`
- Set up a Grafana service account by creating the service account and generating a token for the account to use
  - In the Grafana navigation sidebar, click Administration / Users and Access / Service Accounts / Add service account
    - Display name: `gmpsa` or another name
    - Role: `admin`
  - Add service account token / generate
  - Create a secret named `grafana_service_account_token` with the token string as value
  - Bind the following IAM policy entries:
  1. Role 'Secret Manager Secret Accessor' `roles/secretmanager.secretAccessor`
  2. to the service account used to deploy the terraform module
  3. on the above secret.

## Grafana authenticate and authorize role using Identity Aware Proxy

Review the [document Grafana authenticate and authorize role using Identity Aware Proxy](../../_docs/grafana_auth_iap.md) to set the JMESPath expression in a secret manager secret so that you Google account can get GrafanaAdmin role to perform the actions below about the syncer tool.

## Data source syncer authentication to Cloud Run and to Grafana API

- [Data source syncer tool code on github](https://github.com/GoogleCloudPlatform/prometheus-engine/blob/main/cmd/datasource-syncer/main.go)
- How authentication works?
  - the HTTP client uses TLS or no authentication
  - The Grafana client is built on the HTTP client, and uses the provided Grafana service account token to access the Grafana Data source API.
  - Our Grafana is hosted on Cloud Run that does not support TLS but Oauth2, while the data source syncer tool does not support Oauth2
  - Workaround:
    - The Grafana cloud run service is set the no authentication (meaning allUser / Invoker)
    - As it is exposed behind the load balancer with IAP this is an acceptable tradeoff: This was actually the only way to make Cloud Run and IAP work for a long time before 2023-04-07 sees Cloud run [release notes](https://cloud.google.com/run/docs/release-notes#April_07_2023)
    - The cloud ingress policy accept Internal traffic and load balancer traffic, so it works for both:
      - human users coming from secure IAP - Load balancer
      - the data source syncer tool hosted in the cloud run job that routes all its egress traffic using [direct VPC egress with a dedicated VPC network and subnet](https://cloud.google.com/run/docs/configuring/vpc-direct-vpc). The data source syncer dialog with the grafana data source API is secure by the service account token, not by cloud run.
- Potential improvements
  - Even if the current solution is secure it will be more secure by implementing security in depth using multiple layers of protection.
  - This could be achieved by updating the data source syncer tool http client to managed Oauth authentication and not only TLS . This will enable to restrict the invoker role on the Cloud run service to the IAP service account and the Cloud Job service account.

## Authenticating using Identity Platform - IAP with external identities

- Documentations:
  - [IAP Authenticate users with external identities](https://cloud.google.com/iap/docs/authenticate-users-external-identities)
  - [IAP - Hosting a sign-in page with Cloud Run](https://cloud.google.com/iap/docs/cloud-run-sign-in?_gl=1*xeumod*_ga*MTYwNzAwODk3NS4xNjkxNDc3MDQ5*_ga_WH2QY8WWF5*MTcyOTUxNzkxMi4zODIuMS4xNzI5NTE4MjAxLjIwLjAuMA..)
- Console / IAP / reports backed
  - select the `reports` backend
  - Info panel `Use external identities for authorization`
    - Select `Google` in the list of identity provider
    - Choose IAP to create an authentication page for you using cloud run, use the same region as for other Cloud Run services
    - one the Cloud Run service provided, it returns a LOGIN_URL like `https://iap-gcip-hosted-ui-reports-<yourserviceid>>-ew.a.run.app`
    - Add this LOGIN_URL/__/auth/handler as an authorized redirect URL in your provider configuration, which in our case is Google: console / API/ Credential `sre-idp-google` / Authorized redirect URIs.
    - the `authUI` app has been deployed in Cloud Run using the default compute service account `PROJECT_NUMBER-compute@developer.gserviceaccount.com`. Grant the required IAM role to this service account:
      - `roles/identitytoolkit.viewer`
      - `roles/iap.settingsAdmin`
      - `roles/compute.networkViewer`

## Project level IAM roles for the server account used to deploy the terraform module

- Cloud Function Admin
- Cloud Run Admin
- Cloud Scheduler Admin
- Cloud SQL Admin
- Compute Instance Admin v1
- Compute Load Balancer Admin
- Compute Network Admin
- Compute Security Admin
- IAP Policy Admin
- Identity Platform Admin
- OAuth Config Editor
- Project IAM Admin
- Secret Accessor
- Security Admin
- Service Account Admin
- Service Account User
- Service Usage Consumer
- Storage Admin

## API to activate

- `artifactregistry.googleapis.com`
- `cloudfunctions.googleapis.com`
- `cloudscheduler.googleapis.com`
- `cloudresourcemanager.googleapis.com`
- `cloudresourcemanager.googleapis.com`
- `compute.googleapis.com`
- `iam.googleapis.com`
- `iap.googleapis.com`
- `identitytoolkit.googleapis.com`
- `logging.googleapis.com`
- `monitoring.googleapis.com`
- `run.googleapis.com`
- `secretmanager.googleapis.com`
- `sqladmin.googleapis.com`
