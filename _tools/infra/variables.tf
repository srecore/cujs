/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "GCP project id "
  type        = string
}

variable "dns_name" {
  description = "The DNS name used to expose e.g. myapp.example.com"
  type        = string
}

variable "directory_customer_id" {
  description = "Cloud Identity directory customer id"
  type        = string 
}

variable "directory_dns_name" {
  description = "Cloud Identity directory dns name"
  type        = string 
}

variable "data_source_uids" {
  description = "datasource-uids is a comma separated list of data source UIDs to update."
  type        = string
}

variable "region" {
  description = "gcp region"
  type        = string
  default     = "europe-west1"
}

variable "region_north" {
  description = "gcp region for production north"
  type        = string
  default     = "europe-north1"
}

variable "region_south" {
  description = "gcp region for production south"
  type        = string
  default     = "europe-southwest1"
}

variable "database_version" {
  description = "MySQL version"
  type        = string
  default     = "MYSQL_8_0"
}

variable "grafana_version" {
  description = "Grafana version"
  type        = string
  default     = "11.1.7"
}

variable "data_syncer_version" {
  description = "version of the data syncer"
  type        = string
  default     = "v0.12.0-gke.5"
}

variable "schedule" {
  description = "cron schedule. The token expires in 60 min, and CloudRun stop the instance after 15min, 30 seams good tradeoff"
  type        = string
  default = "*/30 * * * *"
}
