
/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

module "loadbalancer" {
  source     = "./loadbalancer"
  project_id = var.project_id
  region     = var.region
  region_north = var.region_north
  region_south = var.region_south
  dns_name   = var.dns_name
  directory_customer_id = var.directory_customer_id
  directory_dns_name = var.directory_dns_name
}

module "dashbords" {
  source              = "./dashboards"
  project_id          = var.project_id
  region              = var.region
  database_version    = var.database_version
  grafana_version     = var.grafana_version
  data_syncer_version = var.data_syncer_version
  data_source_uids    = var.data_source_uids
  schedule            = var.schedule
  audience_dashboards = module.loadbalancer.audience_dashboards
}

module "reports" {
  source                        = "./reports"
  project_id                    = var.project_id
  region                        = var.region
  grafana_version               = var.grafana_version
  mysql_grafana_connection_name = module.dashbords.mysql_grafana_connection_name
  mysql_grafana_db_name         = module.dashbords.mysql_grafana_db_name
  audience_reports = module.loadbalancer.audience_reports
}

module "identityplatform" {
  source     = "./identityplatform"
  project_id = var.project_id
  region     = var.region
}
