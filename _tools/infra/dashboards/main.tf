/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  service_name = "dashboards"
}

data "google_project" "project" {
  project_id = var.project_id
}

data "google_secret_manager_secret_version_access" "grafana_mysql_root_password" {
  project = var.project_id
  secret  = "grafana-mysql-root-password"
}

data "google_secret_manager_secret_version_access" "grafana_mysql_grafana_user_password" {
  project = var.project_id
  secret  = "grafana-mysql-grafana-user-password"
}

resource "google_service_account" "microservice_sa" {
  project      = var.project_id
  account_id   = local.service_name
  display_name = "SRE ${local.service_name}"
  description  = "Solution: Site Reliability Engineering, microservice: ${local.service_name}"
}

resource "google_project_iam_member" "project_secretmanager_secretaccessor" {
  project = var.project_id
  role    = "roles/secretmanager.secretAccessor"
  member  = "serviceAccount:${google_service_account.microservice_sa.email}"
}

resource "google_project_iam_member" "project_cloudsql_client" {
  project = var.project_id
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${google_service_account.microservice_sa.email}"
}

// Grafana

resource "google_sql_database_instance" "mysql_grafana" {
  project          = var.project_id
  name             = "grafana"
  region           = var.region
  database_version = var.database_version
  root_password    = data.google_secret_manager_secret_version_access.grafana_mysql_root_password.secret_data
  settings {
    tier = var.database_tier
    password_validation_policy {
      min_length                  = 6
      complexity                  = "COMPLEXITY_DEFAULT"
      reuse_interval              = 2
      disallow_username_substring = true
      enable_password_policy      = true
    }
  }
  deletion_protection = true
  lifecycle {
    ignore_changes = [settings[0].database_flags]
  }
}

resource "google_sql_user" "grafana_user" {
  project  = var.project_id
  name     = "grafana"
  instance = google_sql_database_instance.mysql_grafana.name
  password = data.google_secret_manager_secret_version_access.grafana_mysql_grafana_user_password.secret_data
}

resource "google_sql_database" "grafana_db" {
  project  = var.project_id
  name     = "grafana"
  instance = google_sql_database_instance.mysql_grafana.name
}

resource "google_cloud_run_v2_service" "crun_svc" {
  project             = var.project_id
  name                = local.service_name
  location            = var.region
  deletion_protection = false
  client              = "terraform"
  ingress             = "INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER"
  traffic {
    type    = "TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST"
    percent = 100
  }
  template {
    execution_environment = "EXECUTION_ENVIRONMENT_GEN1"
    volumes {
      name = "cloudsql"
      cloud_sql_instance {
        instances = [google_sql_database_instance.mysql_grafana.connection_name]
      }
    }
    containers {
      image = "grafana/grafana:${var.grafana_version}"
      ports {
        container_port = 3000
      }
      resources {
        cpu_idle = true
        limits = {
          cpu    = "${var.crun_cpu}"
          memory = "${var.crun_memory}"
        }
      }
      volume_mounts {
        name       = "cloudsql"
        mount_path = "/cloudsql"
      }
      env {
        name  = "GF_DATABASE_TYPE"
        value = "mysql"
      }
      env {
        name  = "GF_DATABASE_HOST"
        value = "/cloudsql/${google_sql_database_instance.mysql_grafana.connection_name}" # unix socket path
      }
      env {
        name  = "GF_DATABASE_NAME"
        value = google_sql_database.grafana_db.name
      }
      env {
        name  = "GF_DATABASE_USER"
        value = "grafana"
      }
      env {
        name = "GF_DATABASE_PASSWORD"
        value_source {
          secret_key_ref {
            secret  = "projects/${var.project_id}/secrets/grafana-mysql-grafana-user-password"
            version = "latest"
          }
        }
      }
      env {
        name  = "GF_SERVER_ROOT_URL"
        value = "/dashboards"
      }

      env {
        name  = "GF_SERVER_SERVE_FROM_SUB_PATH"
        value = "true"
      }
      env {
        name  = "GF_AUTH_JWT_ENABLED"
        value = "true"
      }
      env {
        name  = "GF_AUTH_JWT_HEADER_NAME"
        value = "x-goog-iap-jwt-assertion"
      }
      env {
        name  = "GF_AUTH_JWT_USERNAME_CLAIM"
        value = "sub"
      }
      env {
        name  = "GF_AUTH_JWT_EMAIL_CLAIM"
        value = "email"
      }
      env {
        name  = "GF_AUTH_JWT_AUTO_SIGN_UP"
        value = "true"
      }
      env {
        name  = "GF_AUTH_JWT_JWK_SET_URL"
        value = "https://www.gstatic.com/iap/verify/public_key-jwk"
      }
      env {
        name  = "GF_AUTH_JWT_CACHE_TTL"
        value = "60m"
      }
      env {
        name  = "GF_AUTH_JWT_EXPECT_CLAIMS"
        value = "{\"iss\": \"https://cloud.google.com/iap\",\"aud\": \"${var.audience_dashboards}\"}"
      }
      env {
        name = "GF_AUTH_JWT_ROLE_ATTRIBUTE_PATH"
        value_source {
          secret_key_ref {
            secret  = "projects/${var.project_id}/secrets/grafana-jwt-role-attribute-path"
            version = "latest"
          }
        }
      }
      env {
        name  = "GF_AUTH_JWT_ALLOW_ASSIGN_GRAFANA_ADMIN"
        value = "true"
      }
    }
    max_instance_request_concurrency = var.crun_concurrency
    timeout                          = var.crun_timeout
    service_account                  = google_service_account.microservice_sa.email
    scaling {
      max_instance_count = var.crun_max_instances
    }
  }
  depends_on = [
    google_sql_user.grafana_user,
    google_sql_database.grafana_db,
    google_project_iam_member.project_cloudsql_client,
  ]
}

data "google_iam_policy" "binding" {
  binding {
    role = "roles/run.invoker"
    members = [
      # "serviceAccount:service-${data.google_project.project.number}@gcp-sa-iap.iam.gserviceaccount.com",
      # "serviceAccount:${google_service_account.gmp_ds_syncer_sa.email}",
      "allUsers", # For now the data source syncer http client does not implement Oauth2, only TLS, which does not enable secure auth to Cloud Run. External users comes form LB / IAP in a secured manner, making this setting acceptable until the data syncer has been improved
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location = google_cloud_run_v2_service.crun_svc.location
  project  = google_cloud_run_v2_service.crun_svc.project
  service  = google_cloud_run_v2_service.crun_svc.name

  policy_data = data.google_iam_policy.binding.policy_data
}

# Data source syncer

data "google_secret_manager_secret_version_access" "grafana_service_account_token" {
  project = var.project_id
  secret  = "grafana-service-account-token"
}

resource "google_service_account" "gmp_ds_syncer_sa" {
  project      = var.project_id
  account_id   = "gmp-ds-syncer-sa"
  display_name = "GMP Datasource Syncer Service Account"
  description  = "Google Managed Prometheus data source syncer service account"
}

resource "google_project_iam_member" "project_syncersa_secretmanager_secretaccessor" {
  project = var.project_id
  role    = "roles/secretmanager.secretAccessor"
  member  = "serviceAccount:${google_service_account.gmp_ds_syncer_sa.email}"
}

resource "google_project_iam_member" "monitoring_viewer" {
  project = var.project_id
  role    = "roles/monitoring.viewer"
  member  = "serviceAccount:${google_service_account.gmp_ds_syncer_sa.email}"
}

resource "google_project_iam_member" "service_account_token_creator" {
  project = var.project_id
  role    = "roles/iam.serviceAccountTokenCreator"
  member  = "serviceAccount:${google_service_account.gmp_ds_syncer_sa.email}"
}

resource "google_compute_network" "crun_vpc" {
  project                 = var.project_id
  name                    = "crun-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "crun_subnet" {
  project                  = var.project_id
  name                     = "crun-subnet"
  ip_cidr_range            = "10.128.0.0/20"
  region                   = var.region
  network                  = google_compute_network.crun_vpc.name
  private_ip_google_access = true
}

resource "google_cloud_run_v2_job" "datasource_syncer_job" {
  project             = var.project_id
  name                = "datasource-syncer-job"
  location            = var.region
  deletion_protection = false
  template {
    task_count = 1
    template {
      containers {
        image = "gcr.io/gke-release/prometheus-engine/datasource-syncer:${var.data_syncer_version}"
        args = [
          "--datasource-uids=${var.data_source_uids}",
          "--grafana-api-endpoint=${google_cloud_run_v2_service.crun_svc.uri}",
          "--grafana-api-token=${data.google_secret_manager_secret_version_access.grafana_service_account_token.secret_data}",
          "--project-id=${var.project_id}",
        ]
      }
      service_account = google_service_account.gmp_ds_syncer_sa.email
      vpc_access {
        egress = "ALL_TRAFFIC"
        network_interfaces {
          subnetwork = google_compute_subnetwork.crun_subnet.id
        }
      }
    }
  }
}

resource "google_service_account" "gmp_ds_syncer_scheduler_sa" {
  project      = var.project_id
  account_id   = "gmp-ds-syncer-scheduler-sa"
  display_name = "GMP Datasource Syncer Scheduler Service Account"
  description  = "Google Managed Prometheus data source syncer scheduler service account"
}

data "google_iam_policy" "binding_scheduler_invoker" {
  binding {
    role = "roles/run.invoker"
    members = [
      "serviceAccount:${google_service_account.gmp_ds_syncer_scheduler_sa.email}",
    ]
  }
}

resource "google_cloud_run_v2_job_iam_policy" "job_iam_policy" {
  location    = google_cloud_run_v2_service.crun_svc.location
  project     = google_cloud_run_v2_service.crun_svc.project
  name        = google_cloud_run_v2_job.datasource_syncer_job.name
  policy_data = data.google_iam_policy.binding_scheduler_invoker.policy_data
}

resource "google_cloud_scheduler_job" "datasource_syncer_scheduler" {
  project   = var.project_id
  name      = "datasource-syncer"
  region    = var.region
  schedule  = var.schedule
  time_zone = "UTC"
  http_target {
    http_method = "POST"
    uri         = "https://${var.region}-run.googleapis.com/apis/run.googleapis.com/v1/namespaces/${var.project_id}/jobs/datasource-syncer-job:run"
    oauth_token {
      service_account_email = google_service_account.gmp_ds_syncer_scheduler_sa.email
    }
  }
}
