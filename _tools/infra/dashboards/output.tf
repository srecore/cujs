/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

output "mysql_grafana_connection_name" {
  description = "mysql connection name like <projectID>:<region>:grafana"
  value       = google_sql_database_instance.mysql_grafana.connection_name
}

output "mysql_grafana_db_name" {
  description = "grafan database name"
  value       = google_sql_database.grafana_db.name
}

output "service_account_email" {
  description = "Service account email used to run this microservice"
  value       = google_service_account.microservice_sa.email
}

output "gmp_ds_syncer_service_account_email" {
  description = "Google Managed Prometheus data source syncer service account email"
  value       = google_service_account.gmp_ds_syncer_sa.email
}


output "crun_service_id" {
  description = "cloud run service id"
  value       = google_cloud_run_v2_service.crun_svc.id
}
output "crun_service_url" {
  description = "cloud run service url"
  value       = google_cloud_run_v2_service.crun_svc.uri
}

output "crun_service_deterministic_url" {
  description = "cloud run service deterministic url"
  value       = "https://${google_cloud_run_v2_service.crun_svc.name}-${data.google_project.project.number}.${var.region}.run.app"
}