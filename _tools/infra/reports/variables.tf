/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "GCP project id "
  type        = string
}

variable "region" {
  description = "gcp region"
  type        = string
}

variable "grafana_version" {
  description = "Grafana version"
  type        = string
}

variable "crun_cpu" {
  description = "Number of cpu in k8s quantity 1000m means 1000 millicpu aka 1"
  type        = string
  default     = "1000m"
}

variable "crun_memory" {
  description = "Memory allocation in k8s quantity "
  type        = string
  default     = "235Mi"
}

variable "crun_concurrency" {
  description = "Number of requests a container could received at the same time"
  type        = number
  default     = 80
}

variable "crun_timeout" {
  description = "Max duration for an instance for responding to a request"
  type        = string
  default     = "60s"
}

variable "crun_max_instances" {
  description = "Max number of instances"
  type        = number
  default     = 2
}

variable "mysql_grafana_connection_name" {
  description = "MySQL connection name"
  type        = string
}

variable "mysql_grafana_db_name" {
  description = "grafana database name"
  type        = string
}

variable "audience_reports" {
  description = "the back end id is used by IAP as the audience string enabling to validate claims"
}

