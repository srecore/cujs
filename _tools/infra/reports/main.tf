/**
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

locals {
  service_name = "reports"
}

data "google_project" "project" {
  project_id = var.project_id
}

data "google_secret_manager_secret_version_access" "grafana_mysql_root_password" {
  project = var.project_id
  secret  = "grafana-mysql-root-password"
}

data "google_secret_manager_secret_version_access" "grafana_mysql_grafana_user_password" {
  project = var.project_id
  secret  = "grafana-mysql-grafana-user-password"
}

resource "google_service_account" "microservice_sa" {
  project      = var.project_id
  account_id   = local.service_name
  display_name = "SRE ${local.service_name}"
  description  = "Solution: Site Reliability Engineering, microservice: ${local.service_name}"
}

resource "google_project_iam_member" "project_secretmanager_secretaccessor" {
  project = var.project_id
  role    = "roles/secretmanager.secretAccessor"
  member  = "serviceAccount:${google_service_account.microservice_sa.email}"
}

resource "google_project_iam_member" "project_cloudsql_client" {
  project = var.project_id
  role    = "roles/cloudsql.client"
  member  = "serviceAccount:${google_service_account.microservice_sa.email}"
}

// Grafana

resource "google_cloud_run_v2_service" "crun_svc" {
  project             = var.project_id
  name                = local.service_name
  location            = var.region
  deletion_protection = false
  client              = "terraform"
  ingress             = "INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER"
  traffic {
    type    = "TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST"
    percent = 100
  }
  template {
    execution_environment = "EXECUTION_ENVIRONMENT_GEN1"
    volumes {
      name = "cloudsql"
      cloud_sql_instance {
        instances = [var.mysql_grafana_connection_name]
      }
    }
    containers {
      image = "grafana/grafana:${var.grafana_version}"
      ports {
        container_port = 3000
      }
      resources {
        cpu_idle = true
        limits = {
          cpu    = "${var.crun_cpu}"
          memory = "${var.crun_memory}"
        }
      }
      volume_mounts {
        name       = "cloudsql"
        mount_path = "/cloudsql"
      }
      env {
        name  = "GF_DATABASE_TYPE"
        value = "mysql"
      }
      env {
        name  = "GF_DATABASE_HOST"
        value = "/cloudsql/${var.mysql_grafana_connection_name}" # unix socket path
      }
      env {
        name  = "GF_DATABASE_NAME"
        value = var.mysql_grafana_db_name
      }
      env {
        name  = "GF_DATABASE_USER"
        value = "grafana"
      }
      env {
        name = "GF_DATABASE_PASSWORD"
        value_source {
          secret_key_ref {
            secret  = "projects/${var.project_id}/secrets/grafana-mysql-grafana-user-password"
            version = "latest"
          }
        }
      }
      env {
        name  = "GF_SERVER_ROOT_URL"
        value = "/reports"
      }

      env {
        name  = "GF_SERVER_SERVE_FROM_SUB_PATH"
        value = "true"
      }
      env {
        name  = "GF_AUTH_JWT_ENABLED"
        value = "true"
      }
      env {
        name  = "GF_AUTH_JWT_HEADER_NAME"
        value = "x-goog-iap-jwt-assertion"
      }
      env {
        name  = "GF_AUTH_JWT_USERNAME_ATTRIBUTE_PATH"
        value = "gcip.user_id"
      }
      env {
        name  = "GF_AUTH_JWT_EMAIL_ATTRIBUTE_PATH"
        value = "gcip.email"
      }
      env {
        name  = "GF_AUTH_JWT_AUTO_SIGN_UP"
        value = "true"
      }
      env {
        name  = "GF_AUTH_JWT_JWK_SET_URL"
        value = "https://www.gstatic.com/iap/verify/public_key-jwk"
      }
      env {
        name  = "GF_AUTH_JWT_CACHE_TTL"
        value = "60m"
      }
      env {
        name  = "GF_AUTH_JWT_EXPECT_CLAIMS"
        value = "{\"iss\": \"https://cloud.google.com/iap\",\"aud\": \"${var.audience_reports}\"}"
      }
      env {
        name = "GF_AUTH_JWT_ROLE_ATTRIBUTE_PATH"
        value = "gcip.grafanarole"
      }
      env {
        name  = "GF_AUTH_JWT_ALLOW_ASSIGN_GRAFANA_ADMIN"
        value = "true"
      }
    }
    max_instance_request_concurrency = var.crun_concurrency
    timeout                          = var.crun_timeout
    service_account                  = google_service_account.microservice_sa.email
    scaling {
      max_instance_count = var.crun_max_instances
    }
  }
  depends_on = [
    google_project_iam_member.project_cloudsql_client,
  ]
}

data "google_iam_policy" "binding" {
  binding {
    role = "roles/run.invoker"
    members = [
      # "serviceAccount:service-${data.google_project.project.number}@gcp-sa-iap.iam.gserviceaccount.com",
      # "serviceAccount:${google_service_account.gmp_ds_syncer_sa.email}",
      "allUsers", # For now the data source syncer http client does not implement Oauth2, only TLS, which does not enable secure auth to Cloud Run. External users comes form LB / IAP in a secured manner, making this setting acceptable until the data syncer has been improved
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location = google_cloud_run_v2_service.crun_svc.location
  project  = google_cloud_run_v2_service.crun_svc.project
  service  = google_cloud_run_v2_service.crun_svc.name

  policy_data = data.google_iam_policy.binding.policy_data
}
