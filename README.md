<!-- markdownlint-disable MD010 MD024 MD033 MD038 -->
# SLOs spec by critical user journeys

1 users 1 goals 1 tasks 0 SLOs

_This doc is auto generated from file_ template_gcm_cujs.yml on 2025-01-06 09:15:36

1. As an online consumer
	1. I want to find items that suit me
		1. I get the homepage

## To provision SLOs, automatically @ scale using PromQL queries and Prometheus recording rules see the following folders

- `teams` folder contains CUJs YAML configuration examples with ProQL queries examples
- `_tools` folder contains the code samples to provision the resources. This this folder README.md for details

## To provision SLOs, automatically @ scale using monitoring filters on Cloud Monitoring see the following folders

- `teams_w_gcm` folder contains CUJs YAML configuration examples with monitoring filters examples
- `_tools` folder contains the code samples to provision the resources. This this folder README.md for details
